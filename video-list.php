<?php
/**
 * This file displays a list of all video files in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listvideoq = "SELECT * FROM video ORDER BY video_name ASC";
$listvideoquery = mysqli_query($dbconn,$listvideoq);

$page_name = "All video files";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START VIDEO-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/video-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listvideoquery) > 0) {
    while ($listvideoopt = mysqli_fetch_assoc($listvideoquery)) {
        $videoid      = $listvideoopt['video_id'];
        $videoname    = $listvideoopt['video_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"video.php?videoid=".$videoid."\">".$videoname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"admin/video-edit.php?videoid=".$videoid."\">Edit</a> | <a href=\"admin/video-delete.php?videoid=".$videoid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listvideoquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no videos in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END VIDEO-LIST.PHP -->
<?php require 'footer.php'; ?>
