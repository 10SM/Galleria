<?php
/**
 * This file displays an organization, and all files associated with them.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';


/** get the ID for this organization */
if (isset($_GET["orgid"])) {
    $get_id = $_GET["orgid"];
} else {
    $get_id = "";
}

/** If we have an ID, lets get the data from the 'organization' table */
if ($get_id != '') {

    /** let's create the query */
    $organizationpageq = "SELECT * FROM organization WHERE organization_id=".$get_id."";
    $organizationpagequery = mysqli_query($dbconn,$organizationpageq);

    while ($organizationpageopt = mysqli_fetch_assoc($organizationpagequery)) {
        $organizationpageid         = $organizationpageopt['organization_id'];
        $organizationpagename       = $organizationpageopt['organization_name'];
        $organizationpagealiases    = $organizationpageopt['organization_aliases'];
        $organizationpagestart      = $organizationpageopt['organization_start_date'];
        $organizationpageend        = $organizationpageopt['organization_end_date'];
        $organizationpagedesc       = $organizationpageopt['organization_description'];
        $organizationpagenato       = $organizationpageopt['organization_place'];
        $organizationpageurl        = $organizationpageopt['organization_url'];
        $organizationpageemail      = $organizationpageopt['organization_email'];
        $organizationpagelogo       = $organizationpageopt['organization_logo'];
        $organizationpagethumb      = $organizationpageopt['organization_thumbnail'];
        $organizationpagehero       = $organizationpageopt['organization_hero_image'];
        $organizationpagetags       = $organizationpageopt['organization_tags'];
        $organizationpagecats       = $organizationpageopt['organization_categories'];
    }
}
/** Does this organization have a logo, thumbnail, or hero image? */
if ($organizationpagelogo !== '') {
    /** Let's get the logo for this organization */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$organizationpagelogo."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
} else if ($organizationpagethumb !== '') {
    /** Let's get the alternate thumbnail for this organization */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$organizationpagethumb."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
} else if ($organizationpagehero !== '') {
    /** Let's get the main thumbnail for this organization */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$organizationpagehero."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
}

$page_name = $organizationpagename;
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START organization.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $organizationpagename; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
                    <div class="vert-block">                <!-- picture-of-the-day: a function randomly chooses a picture to display each day -->
                        <figure class="potd">

<?php
/** if $thumbid is empty, show generic organization image */
if ($thumbid !== NULL or "") {
    echo "\t\t\t\t\t\t\t<img src=\"thumb.php?imageid=".$thumbid."\" alt=\"".$thumbdesc."\" title=\"".$thumbname."\">\n";
    echo "\t\t\t\t\t\t\t<figcaption>".$thumbname."</figcaption>\n";
} else {
    echo "\t\t\t\t\t\t\t<img src=\"includes/generic-organization.png\" alt=\"".$organizationpagename."\" title=\"".$organizationpagename."\">\n";
    echo "\t\t\t\t\t\t\t<figcaption>".$organizationpagename."</figcaption>\n";
}
?>
                        </figure>
                    </div> <!-- end div .vert-block -->
                    <div class="vert-block">                <!-- statistics showing numbers of images, videos, audios, texts, people, companies, awards, etc -->
                        <h2 class="block-title">Demographics</h2>
                        <table>

<?php
                if ($organizationpagedob !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Start date</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$organizationpagestart."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($organizationpagedod !== "0000-00-00") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>End date</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$organizationpageend."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($organizationpageurl !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Website</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td class=\"long-text\"><a href=\"".$organizationpageurl."\">".$organizationpageurl."</a></td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($organizationpageemail !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Email</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td><a href=\"mailto:".$organizationpageemail."\"".$organizationpageemail."</a></td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }
?>
                        </table>
                        <p><a href="https://gottahavacuppamocha.com">Galleria</a> <?php echo $version; ?></p>
                    </div> <!-- end div .vert-block -->
                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                    <h1><?php echo $organizationpagename; ?></h1>
<?php
                if ($organizationpagedesc !== "") {
                    echo "\t\t\t\t\t\t<div class=\"organization-bio\">".$organizationpagedesc."</div>\n";
                } else {
                    echo "\t\t\t\t\t\t<p>We do not have a biography or description for this organization.</p>\n";
                }
?>
                    </div>

<?php
/**
 * Find out if there are any recently added images for this organization in the DB
 */
$orgimgq = "SELECT * FROM image WHERE image_organizations='".$organizationpageid."'";
$orgimgquery = mysqli_query($dbconn,$orgimgq);
if (mysqli_num_rows($orgimgquery) > 0) {
    echo "\t\t\t\t\t<div class=\"horiz-block\">\n";
    echo "\t\t\t\t\t\t<h2 class=\"block-title\">Recent images</h2>\n";
    /** get the image information */
    while ($orgimgopt = mysqli_fetch_assoc($orgimgquery)) {
        $orgimgid      = $orgimgopt['image_id'];
        $orgimgname    = $orgimgopt['image_name'];
        $orgimgdesc    = $orgimgopt['image_description'];
        $orgimgpath    = $orgimgopt['image_path'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($orgimgpath !== '') {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$orgimgid."\" title=\"".$orgimgdesc."\"><img src=\"thumb.php?imageid=".$orgimgid."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$orgimgid."\" title=\"".$orgimgdesc."\"><img src=\"includes/generic-image.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
    echo "\t\t\t\t\t</div>\n";
}
?>
<!-- There will be other sections here in the future -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END organization.PHP -->
<?php require 'footer.php'; ?>
