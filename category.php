<?php
/**
 * This file displays a list of all items in a certain category
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';


/** get the ID for this category */
if (isset($_GET["catid"])) {
    $get_id = $_GET["catid"];
} else {
    $get_id = "";
}

/** If we have an ID, lets get the data from the 'category' table */
if ($get_id != '') {

    /** let's create the query */
    $catpageq = "SELECT * FROM category WHERE category_id=".$get_id."";
    $catpagequery = mysqli_query($dbconn,$catpageq);

    while ($catpageopt = mysqli_fetch_assoc($catpagequery)) {
        $catpageid      = $catpageopt['category_id'];
        $catpagename    = $catpageopt['category_name'];
        $catpageparent  = $catpageopt['category_parent'];
        $catpagedesc    = $catpageopt['category_description'];
        $catpagecolor   = $catpageopt['category_color'];
    }
}


$page_name = $catpagename;
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START CATEGORY.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>
                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><span style="height:30px;width:30px;border-radius:50%;background-color:#<?php echo $catpagecolor; ?>;display:inline-block;"></span>&nbsp;<?php echo $catpagename; ?></h1>

				            <p><?php echo $catpagedesc; ?></p>
				            <p>Future versions of this page will include this category's parent, as well as any child categories.</p>
				            <p>Future versions will include sections or tabs for people, organizations, files, collections, etc in this category.</p>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END CATEGORY.PHP -->
<?php require 'footer.php'; ?>
