# Galleria CHANGELOG

## v20230614

+ Fixed issue where `person-add.php` was trying to send arrays using the `nicetext()` function.

+ Fixed issue where dates and select in forms didn't have any css. 

## v20230227

+ Fixed `img-dir-add.php` so that it actually works. It's not perfect since it will add any file, not just images, but it's a step in the right direction.

+ Trying to get rid of some duplication by moving files for adding, editing, or deleting stuff into the admin folder.

## v20230224

+ Started work on updating admin side of things.

## v20220625

+ Updated `themes/dark.css` to change the colors of input fields.

+ Updated tag* files to new look.

+ Updated person* files to new look.

+ Updated `category-add.php` so it actually adds the color to the DB.

+ Updated `category-list.php` to include color and description.

## v20220624

+ Added `stats-queries.php`, `sidebar-stats.php`, and `sidebar-random-image.php`, and modified `index.php` and `image-list.php`. If I plan on showing the sidebar stuff on multiple pges, it makes sense to treat those parts like a header or footer.

+ Updated *-list.php files to include new looks, including sidebar blocks.

## v20220623

+ Updated `person.php` to make it more useful. It's not done yet, but it's a significant start.

+ Updated `person-add.php` and added `person-edit.php`. Both files probably need more work in the future.

+ On main page, changed Pic of the Day to Random image.

+ Added Statistics sidebar to `image-list.php`. May try to do something else later. I like the look of the sidebar, but not totally sold on it showing statistics.

## v20220621

+ Page titles now come from `$page_title` instead of just showing the version number.

+ Working on `person.php` to make it more useful.

## v20220616

+ Created generic image and generic person icons to use when one isn't available.

+ Created thumb.php to use for making thumbnails for the index page. This should allow us to use images that are outside the web directory. There may be a better way of doing it, but this will do for now.

+ Updated statistics on index page, plus added new queries so we only get five items in the image and people sections. Other sections will be done soon.

## v20220615

+ Stats section on index page can show actual stats.

+ Index page sections won't show tiles unless there are items to show. If there are no items to show, it displays a message.

+ Added queries to index page to get items from the DB.

## v20220609

+ Added TAGS and CATEGORIES to `gadmin-header.php` to make it more consistent with `header.php`.

+ In `dark.css` the padding for `<main>` has been changed. The front page is now full width again, and the other webpages no longer need extra space for a `<nav>` that doesn't exist.

+ In `tag-list.php`, `category-list.php`, and `person-list.php` added notifications if there are no results from the database.

## v20220608

+ Moving most CRUD stuff to main `galleria` folder from the `admin` folder. In the future, the `admin` folder will be more for things like themes, plugins, and language packs. It will be more about the backend of *Galleria* rather than the content in its catalog.

+ Created `person.php`, `person-add.php`, `person-delete.php`, `person-edit.php`, `person-list.php`

## v20220607

+ Created `tag-edit.php` and `tag-delete.php`.

+ Updated `tag-list.php` to add links for editing and deleting, as well as better show off the tag's color.

+ Updated `tag.php` to better show off the tag's color.

+ Updated `gadmin-dark.css` to account for new form inputs.

+ Updated `category-add.php` to make it functional.

+ Created `category.php`, `category-list.php`, `category-edit.php`, and `category-delete.php`

+ Created `person.php`, `person-add.php`, `person-delete.php` and `person-list.php`.

## v20220606

+ It turns out that using hyphens '-' in table and column names isn't a good idea in MySQL/MariaDB. They've been changed to underscores '_'.

+ Finished pretty much everything to do with tags for the moment. More will need to be done to `tag.php` file in the future.

+ Began updating `category-add.php`

## v20220602

+ Fixing comments and indentations for better consistency.

## v20220601

+ Created favicons and added to main and admin headers.

+ Began work on the form in `tag-add.php`. This page will serve as a basis for creating more complex forms elsewhere in the software.

+ Cleaned up `gadmin-dark.css`

## v20220525

+ Created repository on Codeberg (minus the `test/` folder).

+ Began work on admin interface, including its own header, footer, nav and css files.

+ Starting `tag-add.php` and `category-add.php`. The purposes of these pages may be moved to other PHP files in the near future.

## v20220519

+ Galleria mockup in `test/` is more or less done. Starting to create PHP files. DB has been created on test PCs.

## v20220404

+ Created `galleria/` project folder on my laptop

+ Created `admin/`, `includes/`, `themes/`, and `plugins/,` folders inside `galleria/`folder.

  + `admin/` folder will contain elements of the admin interface which will be separate from the regular front end interface.

  + `includes/` folder will contain files for doing specific tasks, such as determining a person's age.

  + `themes/` folder will contain all elements of the front end interface, including stylesheets, image files, localization files, and theme specific functions.

  + `plugins/` folder will be used to add functionality to Galleria, such as scraping an author's page on GoodReads.
