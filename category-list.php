<?php
/**
 * This file displays a list of all categories in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listcatq = "SELECT * FROM category ORDER BY category_name ASC";
$listcatquery = mysqli_query($dbconn,$listcatq);

$page_name = "All categories";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START CATEGORY-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/category-add.php">Add new</a></p>
                        <ul class="item-table">
<?php

if(mysqli_num_rows($listcatquery) > 0) {
    while ($listcatopt = mysqli_fetch_assoc($listcatquery)) {
        $categoryid     = $listcatopt['category_id'];
        $categoryname   = $listcatopt['category_name'];
        $categoryparen	= $listcatopt['category_parent'];
        $categorydesc	= $listcatopt['category_description'];
        $categorycolor	= $listcatopt['category_color'];

        echo "\t\t\t\t\t\t\t<li>\n";
        echo "\t\t\t\t\t\t\t\t<span style=\"height:16px;width:16px;border-radius:50%;background-color:#".$categorycolor.";display:inline-block;\"></span>&nbsp;<a href=\"category.php?catid=".$categoryid."\">".$categoryname."</a></span>\n";
        echo "\t\t\t\t\t\t\t\t<span>".$categorydesc."</span>\n";
        echo "\t\t\t\t\t\t\t\t<span><a href=\"admin/category-edit.php?catid=".$categoryid."\">Edit</a> | <a href=\"admin/category-delete.php?catid=".$categoryid."\">Delete</a></span>\n";
        echo "\t\t\t\t\t\t\t</li>\n";
    }
} else if(mysqli_num_rows($listcategoryquery) == 0) {
    echo "\t\t\t\t\t\t\t<li>There are no categories in the database</li>\n";
}


?>
                        </ul>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END ADMIN/CATEGORY-LIST.PHP -->
<?php require 'footer.php'; ?>
