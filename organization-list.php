<?php
/**
 * This file displays a list of all organizations in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listorganizationq = "SELECT * FROM organization ORDER BY organization_added_timestamp DESC";
$listorganizationquery = mysqli_query($dbconn,$listorganizationq);

$page_name = "All organizations";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START ORGANIZATION-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/organization-add.php">Add new</a></p>
<?php

if(mysqli_num_rows($listorganizationquery) > 0) {
    while ($listorganizationopt = mysqli_fetch_assoc($listorganizationquery)) {
        $organizationid      = $listorganizationopt['organization_id'];
        $organizationadded      = $listorganizationopt['organization_added_timestamp'];
        $organizationname    = $listorganizationopt['organization_name'];
        $organizationlogo       = $listorganizationopt['organization_logo'];
        $organizationthumb      = $listorganizationopt['organization_thumbnail'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";

        if ($organizationlogo != '') {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"thumb.php?imageid=".$organizationlogo."\" class=\"horiz-block-img\"></a>\n";
        } else if ($organizationthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"thumb.php?imageid=".$organizationthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"includes/generic-organization.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
} else if(mysqli_num_rows($listorganizationquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no organizations in the database</td></tr>\n";
}


?>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END ORGANIZATION-LIST.PHP -->
<?php require 'footer.php'; ?>
