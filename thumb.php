<?php
/**
 * This file should let us read images outside of the web directory
 */

/** Files required to go further */
#require_once 'includes/galleria-metadata.php';
require 'includes/functions.php';


/** get the ID for this person */
if (isset($_GET["imageid"])) {
    $get_id = $_GET["imageid"];
} else {
    $get_id = "";
}

/** If we have an ID, lets get the data from the 'image' table */
if ($get_id != '') {

    /** let's create the query */
    $thumbq = "SELECT * FROM image WHERE image_id=".$get_id."";
    $thumbquery = mysqli_query($dbconn,$thumbq);

    while ($thumbopt = mysqli_fetch_assoc($thumbquery)) {
        $thumbpath      = $thumbopt['image_path'];
    }

/** see https://bytes.com/topic/php/answers/886538-retrieve-images-located-outside-web-root-using-php */
/** see also https://stackoverflow.com/questions/21869016/php-scandir-on-external-hard-drive-stops-at-root-directory */
$thumbext = pathinfo($thumbpath,PATHINFO_EXTENSION);

header('Content-type: image/'.$thumbext);
header('Content-length' . filesize($thumbpath));

readfile($thumbpath);
}
?>
