<?php
/**
 * This file displays a list of all persons in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listpersonq = "SELECT * FROM person ORDER BY person_added_timestamp DESC";
$listpersonquery = mysqli_query($dbconn,$listpersonq);

$page_name = "All people";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START PERSON-LIST.PHP -->
        <main>
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/person-add.php">Add new</a></p>
<?php

if(mysqli_num_rows($listpersonquery) > 0) {
    while ($listpersonopt = mysqli_fetch_assoc($listpersonquery)) {
        $personid      = $listpersonopt['person_id'];
        $personadded    = $listpersonopt['person_added_timestamp'];
        $personname    = $listpersonopt['person_name'];
        $personthumb    = $listpersonopt['person_thumbnail'];
        $personaltthumb = $listpersonopt['person_alternate_thumbnail'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";

        if ($personthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"thumb.php?imageid=".$personthumb."\" class=\"horiz-block-img\"></a>\n";
        } else if ($personaltthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"thumb.php?imageid=".$personaltthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"includes/generic-person.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
} else if(mysqli_num_rows($listpersonquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no people in the database</td></tr>\n";
}


?>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END PERSON-LIST.PHP -->
<?php require 'footer.php'; ?>
