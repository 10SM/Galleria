<?php
/**
 * This file displays a list of all image files in Galleria.
 */

/** Files required to go further */
require_once 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our main query */
$listimageq = "SELECT * FROM image ORDER BY image_added_timestamp DESC";
$listimagequery = mysqli_query($dbconn,$listimageq);

$page_name = "All image files";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START IMAGE-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/image-add.php">Upload an image</a></p>
                        <p class="add-new-span"><a href="admin/image-dir-add.php">Scan a directory</a></p>
<?php

if(mysqli_num_rows($listimagequery) > 0) {
    while ($listimageopt = mysqli_fetch_assoc($listimagequery)) {
        $imageid        = $listimageopt['image_id'];
        $imageadded     = $listimageopt['image_added_timestamp'];
        $imagename      = $listimageopt['image_name'];
        $imagepath      = $listimageopt['image_path'];


        echo "\t\t\t\t\t\t<figure class=\"horiz-block-column\">\n";
        if ($imagepath != '') {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"thumb.php?imageid=".$imageid."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"includes/generic-image.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</figure>\n";
    }
} else if(mysqli_num_rows($listimagequery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no image files in the database</td></tr>\n";
}


?>

                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END IMAGE-LIST.PHP -->
<?php require 'footer.php'; ?>
