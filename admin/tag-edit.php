<?php
/**
 * This file is for editing tags in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this tag */
if (isset($_GET["tagid"])) {
    $get_id = $_GET["tagid"];
} else {
    $get_id = "";
}

/**
 * Get information on this tag to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $gettagq = "SELECT * FROM tag WHERE tag_id=".$get_id."";
    $gettagquery = mysqli_query($dbconn,$gettagq);

    while ($gettagopt = mysqli_fetch_assoc($gettagquery)) {
        $gettagid       = $gettagopt['tag_id'];
        $gettagname     = $gettagopt['tag_name'];
        $gettagslug     = $gettagopt['tag_slug'];
        $gettagdesc     = $gettagopt['tag_description'];
        $gettagcolor    = $gettagopt['tag_color'];
    }
}

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['tag-submit'])) {
    $tag_id     = $_POST['tag-id'];
    $tag_name   = nicetext($_POST['tag-name']);
    $tag_slug   = makeslug($_POST['tag-slug']);
    $tag_desc   = nicetext($_POST['tag-desc']);
    $tag_color  = iscolor($_POST['tag-color']);

    /** Here is our query */
    $edittagq  = "UPDATE tag SET tag_name='".$tag_name."', tag_slug='".$tag_slug."', tag_description='".$tag_desc."', tag_color='".$tag_color."' WHERE tag_id='".$tag_id."'";
    $edittagquery = mysqli_query($dbconn,$edittagq);
    redirect($website_url."/tag-list.php");
}


$page_name = "Edit ".$gettagname;
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $edittagq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START TAG-EDIT.PHP -->
        <main>
	        <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="tag-edit.php">
				            <input type="hidden" name="tag-id" id="tag-id" value="<?php echo $gettagid; ?>">
				                <table>
				                    <tr>
				                        <td><label for="tag-name">Tag name</label></td>
				                        <td><input type="text" name="tag-name" id="tag-name" class="form-input-text" value="<?php echo $gettagname; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="tag-name">Tag slug</label></td>
				                        <td><input type="text" name="tag-slug" id="tag-slug" class="form-input-text" value="<?php echo $gettagslug; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="tag-desc">Tag description</label></td>
				                        <td><textarea name="tag-desc" id ="tag-desc" class="form-textarea" rows="12"><?php echo $gettagdesc; ?></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="tag-color">Tag color</label></td>
				                        <td><input type="color" name="tag-color" id="tag-color" class="form-input-color" value="#<?php echo $gettagcolor; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="tag-submit" id="tag-submit" class="form-input-submit" value="<?php echo _('UPDATE TAG'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TAG-EDIT.PHP -->
<?php require 'gadmin-footer.php'; ?>
