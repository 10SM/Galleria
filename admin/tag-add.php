<?php
/**
 * This file is for creating new tags in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['tag-submit'])) {
    $tag_name   = nicetext($_POST['tag-name']);
    $tag_slug   = makeslug($_POST['tag-name']);
    $tag_desc   = nicetext($_POST['tag-desc']);
    $tag_color  = iscolor($_POST['tag-color']);

    /** Here is our query */
    $addtagq  = "INSERT INTO tag (tag_name, tag_slug, tag_description, tag_color) VALUES ('".$tag_name."', '".$tag_slug."', '".$tag_desc."', '".$tag_color."')";
    $addtagquery = mysqli_query($dbconn,$addtagq);
    redirect($website_url."/tag-list.php");
}


$page_name = "Add a tag";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $addtagq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START TAG-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="tag-add.php">
				                <table>
				                    <tr>
				                        <td><label for="tag-name">Tag name</label></td>
				                        <td><input type="text" name="tag-name" id="tag-name" class="form-input-text" placeholder="<?php echo _('Add tag name'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="tag-desc">Tag description</label></td>
				                        <td><textarea name="tag-desc" id ="tag-desc" class="form-textarea" rows="12"></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="tag-color">Tag color</label></td>
				                        <td><input type="color" name="tag-color" id="tag-color" class="form-input-color" placeholder="ffffff"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="tag-submit" id="tag-submit" class="form-input-submit" value="<?php echo _('ADD TAG'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TAG-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
