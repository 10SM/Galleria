<?php
/**
 * This file is for deleting tags in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this tag */
if (isset($_GET["tagid"])) {
    $get_id = $_GET["tagid"];
} else {
    $get_id = "";
}

/**
 * Get information on this tag to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $gettagq = "SELECT * FROM tag WHERE tag_id=".$get_id."";
    $gettagquery = mysqli_query($dbconn,$gettagq);

    while ($gettagopt = mysqli_fetch_assoc($gettagquery)) {
        $gettagid       = $gettagopt['tag_id'];
        $gettagname     = $gettagopt['tag_name'];
    }
}

/**
 * Delete the tag from the DB or go back to the list
 */
if (isset($_POST['tag-delete'])) {
    $tag_id     = $_POST['tag-id'];

    /** Here is our query */
    $deletetagq  = "DELETE from tag WHERE tag_id='".$tag_id."'";
    $deletetagquery = mysqli_query($dbconn,$deletetagq);
    redirect($website_url."/tag-list.php");
} else if (isset($_POST['tag-cancel'])) {
    redirect($website_url."/tag-list.php");
}


$page_name = "Delete ".$gettagname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deletetagq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START TAG-DELETE.PHP -->
        <main>
	        <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="tag-delete.php">
				            <input type="hidden" name="tag-id" id="tag-id" value="<?php echo $gettagid; ?>">
				                <table>
				                    <tr>
				                        <td><input type="submit" name="tag-cancel" id="tag-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
				                        <td><input type="submit" name="tag-delete" id="tag-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
				                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TAG-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
