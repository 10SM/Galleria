<?php
/**
 * This file is for deleting categories in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this category */
if (isset($_GET["catid"])) {
    $get_id = $_GET["catid"];
} else {
    $get_id = "";
}

/**
 * Get information on this category to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getcatq = "SELECT * FROM category WHERE category_id=".$get_id."";
    $getcatquery = mysqli_query($dbconn,$getcatq);

    while ($getcatopt = mysqli_fetch_assoc($getcatquery)) {
        $getcatid       = $getcatopt['category_id'];
        $getcatname     = $getcatopt['category_name'];
    }
}

/**
 * Delete the category from the DB or go back to the list
 */
if (isset($_POST['cat-delete'])) {
    $cat_id     = $_POST['cat-id'];

    /** Here is our query */
    $deletecatq  = "DELETE from category WHERE category_id='".$cat_id."'";
    $deletecatquery = mysqli_query($dbconn,$deletecatq);
    redirect($website_url."/category-list.php");
} else if (isset($_POST['category-cancel'])) {
    redirect($website_url."/category-list.php");
}


$page_name = "Delete ".$getcatname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deletecatq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START CATEGORY-DELETE.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="category-delete.php">
				            <input type="hidden" name="cat-id" id="cat-id" value="<?php echo $getcatid; ?>">
				                <table>
				                    <tr>
				                        <td><input type="submit" name="cat-cancel" id="cat-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
				                        <td><input type="submit" name="cat-delete" id="cat-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
				                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END CATEGORY-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
