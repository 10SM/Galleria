<?php
/**
 * This file is for adding a single text/document file to Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['text-submit'])) {
    /** borrowing heavily from https://www.w3schools.com/php/php_file_upload.asp */
    $textdir       = "../media/texts/";
    $textfile      = basename($_FILES["text-file"]["name"]);
    $textcheck     = 1;

    /** Get the text file type */
    $texttype      = strtolower(pathinfo($textfile,PATHINFO_EXTENSION));
    if ($texttype != "pdf" && $texttype != "cbr" && $texttype != "cbz" && $texttype != "epub" && $texttype != "cb7" && $texttype != "cba" && $texttype != "cbt" && $texttype != "md" && $texttype != "txt" && $texttype != "csv" && $texttype != "rtf" && $texttype != "md" && $texttype != "ris") {
        echo "<span>File is not a text or document file type</span><br>\n";
        $textcheck = 0;
    }

    /** If the text file passes the checks above, let's get ready to upload it */
    if ($textcheck !== 0) {

        /** Get today's date and create a folder for it in media/audios if it doesn't already exist */
        $thisyear       = date("Y");
        $thismonth      = date("m");
        $thisday        = date("d");

        if (!is_dir($textdir.$thisyear)) {
            mkdir($textdir.$thisyear,0777,true);
            echo "<span>".$textdir.$thisyear." directory was created.</span><br>\n";
        } else {
            echo "<span>".$textdir.$thisyear." is a directory</span><br>\n";
        }

        if (!is_dir($textdir.$thisyear."/".$thismonth)) {
            mkdir($textdir.$thisyear."/".$thismonth,0777,true);
        }

        if (!is_dir($textdir.$thisyear."/".$thismonth."/".$thisday)) {
            mkdir($textdir.$thisyear."/".$thismonth."/".$thisday,0777,true);
        }

        /** Based on the folder created above, determine the file path of the text file */
        $textpath = $textdir.$thisyear."/".$thismonth."/".$thisday."/".$textfile;

        /** let's upload the text file and enter it in the DB */
        if (move_uploaded_file($_FILES["text-file"]["tmp_name"], $textpath)) {

            $addtextq = "INSERT INTO text (text_name, text_path) VALUES ('".$textfile."', '".$textpath."')";
            $addtextquery = mysqli_query($dbconn,$addtextq);
            redirect($website_url."/text-list.php");

        } else {

            echo "<span>There was a problem uploading the file.</span><br>\n";

        }


    } else {
        echo "<span>The file was unable to be uploaded</span><br>\n";
    }


}


$page_name = "Upload a text file";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START TEXT-ADD.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $page_name; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
		            <h1><?php echo $page_name; ?></h1>
			            <p class="add-new-span"><a href="text-dir-add.php">Scan a directory</a></p>
                        <p class="add-new-span"><a href="text-list.php">Return to the text list</a></p>
			            <form method="post" action="text-add.php" enctype="multipart/form-data">
			                <table>
			                    <tr>
			                        <td><input type="file" name="text-file" id="text-file" class="form-input-submit"></td>
			                    </tr>
			                    <tr>
			                        <td><input type="submit" value="Upload text file" name="text-submit" class="form-input-submit"></td>
                                </tr>
			                </table>
			            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TEXT-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
