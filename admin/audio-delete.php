<?php
/**
 * This file deletes an audio file from the Galleria database and removes it from the hard drive.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';


/** get the ID for this image */
if (isset($_GET["audioid"])) {
    $get_id = $_GET["audioid"];
} else {
    $get_id = "";
}

/**
 * Get information on this audio file to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getaudioq = "SELECT * FROM audio WHERE audio_id='".$get_id."'";
    $getaudioquery = mysqli_query($dbconn,$getaudioq);

    while ($getaudioopt = mysqli_fetch_assoc($getaudioquery)) {
        $getaudioid         = $getaudioopt['audio_id'];
        $getaudioname       = $getaudioopt['audio_name'];
        $getaudiopath       = $getaudioopt['audio_path'];
    }
}

/**
 * Delete the audio file from the DB and dir or go back to the list
 */
if (isset($_POST['audio-delete'])) {
    $audio_id       = $_POST['audio-id'];
    $audio_path     = $_POST['audio-path'];

    /** Here is our query to delete it from the DB */
    $deleteaudioq       = "DELETE from audio WHERE audio_id='".$audio_id."'";
    $deleteaudioquery   = mysqli_query($dbconn,$deleteaudioq);

    /** This should remove it from the hard drive */
    unlink($audio_path);

    /** The actions are completed so we go back to the audio list*/
    redirect($website_url."/audio-list.php");

} else if (isset($_POST['audio-cancel'])) {

    /** The action is canceled so we go back to the audio list */
    redirect($website_url."/audio-list.php");

}


$page_name = "Delete ".$getaudioname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deleteaudioq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START AUDIO-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				</div> <!-- end div .column-one -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="audio-delete.php">
                    <input type="hidden" name="audio-id" id="audio-id" value="<?php echo $getaudioid; ?>">
                    <input type="hidden" name="audio-path" id="audio-path" value="<?php echo $getaudiopath; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="audio-cancel" id="audio-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="audio-delete" id="audio-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
    <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END AUDIO-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
