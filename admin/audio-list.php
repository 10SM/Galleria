<?php
/**
 * This file displays a list of all audio files in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** Here is our query */
$listaudioq = "SELECT * FROM audio ORDER BY audio_name ASC";
$listaudioquery = mysqli_query($dbconn,$listaudioq);

$page_name = "All audio files";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START AUDIO-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="audio-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listaudioquery) > 0) {
    while ($listaudioopt = mysqli_fetch_assoc($listaudioquery)) {
        $audioid      = $listaudioopt['audio_id'];
        $audioname    = $listaudioopt['audio_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"audio.php?audioid=".$audioid."\">".$audioname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"audio-edit.php?audioid=".$audioid."\">Edit</a> | <a href=\"audio-delete.php?audioid=".$audioid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listaudioquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no audio files in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END AUDIO-LIST.PHP -->
<?php require 'gadmin-footer.php'; ?>
