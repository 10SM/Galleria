<?php
/**
 * This file is for deleting an organization in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this tag */
if (isset($_GET["orgid"])) {
    $get_id = $_GET["orgid"];
} else {
    $get_id = "";
}

/**
 * Get information on this organization to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getorganizationq = "SELECT * FROM organization WHERE organization_id=".$get_id."";
    $getorganizationquery = mysqli_query($dbconn,$getorganizationq);

    while ($getorganizationopt = mysqli_fetch_assoc($getorganizationquery)) {
        $getorganizationid       = $getorganizationopt['organization_id'];
        $getorganizationname     = $getorganizationopt['organization_name'];
    }
}

/**
 * Delete the organization from the DB or go back to the list
 */
if (isset($_POST['organization-delete'])) {
    $organization_id     = $_POST['organization-id'];

    /** Here is our query */
    $deleteorganizationq  = "DELETE from organization WHERE organization_id='".$organization_id."'";
    $deleteorganizationquery = mysqli_query($dbconn,$deleteorganizationq);
    redirect($website_url."/organization-list.php");
} else if (isset($_POST['organization-cancel'])) {
    redirect($website_url."/organization-list.php");
}


$page_name = "Delete ".$getorganizationname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deleteorganizationq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START organization-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="organization-delete.php">
                    <input type="hidden" name="organization-id" id="organization-id" value="<?php echo $gettagid; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="organization-cancel" id="organization-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="tag-delete" id="organization-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
		<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END organization-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
