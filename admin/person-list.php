<?php
/**
 * This file displays a list of all persons in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** Here is our query */
$listpersonq = "SELECT * FROM person ORDER BY person_sort_name ASC";
$listpersonquery = mysqli_query($dbconn,$listpersonq);

$page_name = "All people";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START PERSON-LIST.PHP -->
        <main>
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="person-add.php">Add new</a></p>


<?php

if(mysqli_num_rows($listpersonquery) > 0) {

    echo "\t\t\t\t\t\t<!-- A table where each row is a new person -->\n";
    echo "\t\t\t\t\t\t<table class=\"admin-list-table\">\n";
    echo "\t\t\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t\t<th>Thumbnail</th>\n";
    echo "\t\t\t\t\t\t\t<th>Name</th>\n";
    echo "\t\t\t\t\t\t\t<th>Added</th>\n";
    echo "\t\t\t\t\t\t\t<th>Meta</th>\n";
    echo "\t\t\t\t\t\t</tr>\n";

    while ($listpersonopt = mysqli_fetch_assoc($listpersonquery)) {
        $personid      = $listpersonopt['person_id'];
        $personadded    = $listpersonopt['person_added_timestamp'];
        $personname    = $listpersonopt['person_name'];
        $personthumb    = $listpersonopt['person_thumbnail'];
        $personaltthumb = $listpersonopt['person_alternate_thumbnail'];

        echo "\t\t\t\t\t\t\t\t<tr class=\"admin-list-record\">\n";

        if ($personthumb != '') {
            echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"../thumb.php?imageid=".$personthumb."\" alt=\"Image of ".$personname."\" class=\"admin-list-img\"></a></td>\n";
        } else if ($personaltthumb != '') {
            echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"../thumb.php?imageid=".$personaltthumb."\" alt=\"Image of ".$personname."\" class=\"admin-list-img\"></a></td>\n";
        } else {
            echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"../includes/generic-person.png\" alt=\"We have no image of ".$personname."\" class=\"admin-list-img\"></a></td>\n";
        }

        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"person.php?personid=".$personid."\">".$personname."</a></td>\n";
        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\">".$personadded."</td>\n";
        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"person-edit.php?persid=".$personid."\">EDIT</a> | <a href=\"person-delete.php?persid=".$personid."\">DELETE</a></td>\n";
        echo "\t\t\t\t\t\t\t\t</tr>\n";
    }
    echo "\t\t\t\t\t\t</table>\n";
} else if(mysqli_num_rows($listpersonquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no people in the database</td></tr>\n";
}


?>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END PERSON-LIST.PHP -->
<?php require 'footer.php'; ?>
