<?php
/**
 * This file deletes an image from the Galleria database and removes it from the hard drive.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';


/** get the ID for this image */
if (isset($_GET["imageid"])) {
    $get_id = $_GET["imageid"];
} else {
    $get_id = "";
}

/**
 * Get information on this image to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getimageq = "SELECT * FROM image WHERE image_id='".$get_id."'";
    $getimagequery = mysqli_query($dbconn,$getimageq);

    while ($getimageopt = mysqli_fetch_assoc($getimagequery)) {
        $getimageid         = $getimageopt['image_id'];
        $getimagename       = $getimageopt['image_name'];
        $getimagepath       = $getimageopt['image_path'];
    }
}

/**
 * Delete the image from the DB and dir or go back to the list
 */
if (isset($_POST['image-delete'])) {
    $image_id       = $_POST['image-id'];
    $image_path     = $_POST['image-path'];

    /** Here is our query to delete it from the DB */
    $deleteimageq       = "DELETE from image WHERE image_id='".$image_id."'";
    $deleteimagequery   = mysqli_query($dbconn,$deleteimageq);

    /** This should remove it from the hard drive */
    unlink($image_path);

    /** The actions are completed so we go back to the image list*/
    redirect($website_url."/admin/image-list.php");

} else if (isset($_POST['image-cancel'])) {

    /** The action is canceled so we go back to the image list */
    redirect($website_url."/admin/image-list.php");

}


$page_name = "Delete ".$getimagename."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deleteimageq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START IMAGE-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="image-delete.php">
                    <input type="hidden" name="image-id" id="image-id" value="<?php echo $getimageid; ?>">
                    <input type="hidden" name="image-path" id="image-path" value="<?php echo $getimagepath; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="image-cancel" id="image-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="image-delete" id="image-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
		<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END IMAGE-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
