<?php
/**
 * This file deletes a video file from the Galleria database and removes it from the hard drive.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';


/** get the ID for this image */
if (isset($_GET["videoid"])) {
    $get_id = $_GET["videoid"];
} else {
    $get_id = "";
}

/**
 * Get information on this video file to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getvideoq = "SELECT * FROM video WHERE video_id='".$get_id."'";
    $getvideoquery = mysqli_query($dbconn,$getvideoq);

    while ($getvideoopt = mysqli_fetch_assoc($getvideoquery)) {
        $getvideoid         = $getvideoopt['video_id'];
        $getvideoname       = $getvideoopt['video_name'];
        $getvideopath       = $getvideoopt['video_path'];
    }
}

/**
 * Delete the video file from the DB and dir or go back to the list
 */
if (isset($_POST['video-delete'])) {
    $video_id       = $_POST['video-id'];
    $video_path     = $_POST['video-path'];

    /** Here is our query to delete it from the DB */
    $deletevideoq       = "DELETE from video WHERE video_id='".$video_id."'";
    $deletevideoquery   = mysqli_query($dbconn,$deletevideoq);

    /** This should remove it from the hard drive */
    unlink($video_path);

    /** The actions are completed so we go back to the video list*/
    redirect($website_url."/video-list.php");

} else if (isset($_POST['video-cancel'])) {

    /** The action is canceled so we go back to the video list */
    redirect($website_url."/video-list.php");

}


$page_name = "Delete ".$getvideoname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deletevideoq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START VIDEO-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="video-delete.php">
                    <input type="hidden" name="video-id" id="video-id" value="<?php echo $getvideoid; ?>">
                    <input type="hidden" name="video-path" id="video-path" value="<?php echo $getvideopath; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="video-cancel" id="video-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="video-delete" id="video-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
		<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END VIDEO-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
