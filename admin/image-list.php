<?php
/**
 * This file displays a list of all image files in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** Here is our main query */
$listimageq = "SELECT * FROM image ORDER BY image_added_timestamp DESC";
$listimagequery = mysqli_query($dbconn,$listimageq);

$page_name = "All image files";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START IMAGE-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="image-add.php">Upload an image</a></p>
                        <p class="add-new-span"><a href="image-dir-add.php">Scan a directory</a></p>
<?php

if(mysqli_num_rows($listimagequery) > 0) {

    echo "\t\t\t\t\t\t<!-- A table where each row is a new person -->\n";
    echo "\t\t\t\t\t\t<table class=\"admin-list-table\">\n";
    echo "\t\t\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t\t<th>Thumbnail</th>\n";
    echo "\t\t\t\t\t\t\t<th>Title</th>\n";
    echo "\t\t\t\t\t\t\t<th>Added</th>\n";
    echo "\t\t\t\t\t\t\t<th>Meta</th>\n";
    echo "\t\t\t\t\t\t</tr>\n";
    while ($listimageopt = mysqli_fetch_assoc($listimagequery)) {
        $imageid        = $listimageopt['image_id'];
        $imageadded     = $listimageopt['image_added_timestamp'];
        $imagename      = $listimageopt['image_name'];
        $imagepath      = $listimageopt['image_path'];

        echo "\t\t\t\t\t\t\t\t<tr class=\"admin-list-record\">\n";

        if ($imagepath != '') {
            echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"../thumb.php?imageid=".$imageid."\" class=\"admin-list-img\"></a></td>\n";
        } else {
            echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"../includes/generic-image.png\" class=\"admin-list-img\"></a></td>\n";
        }

        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"image.php?imageid=".$imageid."\">".$imagename."</a></td>\n";
        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\">".$imageadded."</td>\n";
        echo "\t\t\t\t\t\t\t\t\t<td class=\"admin-list-record-field\"><a href=\"image-meta-edit.php?imageid=".$imageid."\">EDIT</a> | <a href=\"image-delete.php?imageid=".$imageid."\">DELETE</a></td>\n";
        echo "\t\t\t\t\t\t\t\t</tr>\n";
    }
    echo "\t\t\t\t\t\t</table>\n";
} else if(mysqli_num_rows($listimagequery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no image files in the database</td></tr>\n";
}


?>

                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END IMAGE-LIST.PHP -->
<?php require 'gadmin-footer.php'; ?>
