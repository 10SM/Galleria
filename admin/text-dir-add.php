<?php
/**
 * This file is for scanning a directory for text files and adding them to the database. It does not upload them to a separate directory in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['text-dir-submit'])) {

    $txtdir     = $_POST['text-dir-name'];

    /** Let's make sure it is a directory */
    /** Borrowed quite a bit from https://www.w3schools.com/PHP/func_directory_readdir.asp */
    if (is_dir($txtdir)) {


        if ($thisdir = opendir($txtdir)) {

            while (($file = readdir($thisdir)) !== false) {

                /**
                 * TO DO LIST
                 * Check to see if file is a text or document file
                 * Check to see if the file/path is already in the db
                 * Check to see if it is a supported file type
                 * Get text file date (?))
                 * After these steps are done, enter the files in the DB if appropriate
                 */

                /** Set the path and insert that and the text name into the db */

                $textpath = $txtdir."/".$file;
                $addtextq = "INSERT INTO text (text_name, text_path) VALUES ('".$file."', '".$textpath."')";
                $addtextquery = mysqli_query($dbconn,$addtextq);
            }

            closedir($thisdir);
            redirect($website_url."/admin/text-list.php");

        }
    /** If it's not a directory */
    } else {
        echo "<span>".$imgdir." doesn't look like a directory. Please check it and try again.</span><br>\n";
    }


}


$page_name = "Scan a directory";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $textpath."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START TEXT-DIR-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
				            <h1><?php echo $page_name; ?></h1>
				            <p class="add-new-span"><a href="text-add.php">Upload a single text file</a></p>
                            <p class="add-new-span"><a href="text-list.php">Return to the text list</a></p>
				            <form method="post" action="text-dir-add.php" enctype="multipart/form-data">
				                <table>
				                    <tr>
				                        <td><label for="text-dir">Directory</label></td>
				                        <td><input type="text" name="text-dir-name" id="text-dir-name" class="form-input-text"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" value="Scan Directory" name="text-dir-submit" class="form-input-submit"></td>
                                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TEXT-DIR-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
