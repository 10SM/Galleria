<?php
/**
 * This file is for editing person in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this person */
if (isset($_GET["personid"])) {
    $get_id = $_GET["personid"];
} else {
    $get_id = "";
}

/**
 * Get information on this tag to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getpersonq = "SELECT * FROM person WHERE person_id=".$get_id."";
    $getpersonquery = mysqli_query($dbconn,$getpersonq);

    while ($getpersonopt = mysqli_fetch_assoc($getpersonquery)) {
        $getpersonid            = $getpersonopt['person_id'];
        $getpersonname          = $getpersonopt['person_name'];
        $getpersonalias         = $getpersonopt['person_aliases'];
        $getpersonborndate      = $getpersonopt['person_birth_date'];
        $getpersonbornplace     = $getpersonopt['person_birth_place'];
        $getpersondiedate       = $getpersonopt['person_death_date'];
        $getpersondieplace      = $getpersonopt['person_death_place'];
        $getpersondiecause      = $getpersonopt['person_death_cause'];
        $getpersondesc          = $getpersonopt['person_description'];
        $getpersonnato          = $getpersonopt['person_nationalities'];
        $getpersonurl           = $getpersonopt['person_url'];
        $getpersonemail         = $getpersonopt['person_email'];
        $getpersontags          = $getpersonopt['person_tags'];
        $getpersoncats          = $getpersonopt['person_categories'];
    }
}

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['person-submit'])) {
    $personid           = $_POST['person-id'];
    $personname         = nicetext($_POST['person-name']);
    $personalias        = nicetext($_POST['person-alias']);
    $personborn         = $_POST['person-birth-date'];
    $personbornplace    = nicetext($_POST['person-birth-place']);
    $persondied         = $_POST['person-death-date'];
    $persondiedplace    = nicetext($_POST['person-death-place']);
    $persondiedcause    = nicetext($_POST['person-death-cause']);
    $persondesc         = nicetext($_POST['person-desc']);
    $personnationality  = nicetext($_POST['person-nationality']);
    $personurl          = nicetext($_POST['person-url']);
    $personemail        = nicetext($_POST['person-email']);
    $persontags         = $_POST['person-tags'];
    $personcats         = $_POST['person-cats'];

    if($personborn == '') {
        $birthdate = '0000-00-00';
    } else {
        $birthdate = $personborn;
    }

    if($persondied == '') {
        $deathdate = '0000-00-00';
    } else {
        $deathdate = $persondied;
    }
    $editpersonq = "UPDATE person SET person_name='".$personname."', person_aliases='".$personalias."', person_birth_date='".$birthdate."', person_birth_place='".$personbornplace."', person_death_date='".$deathdate."', person_death_place='".$persondiedplace."', person_death_cause='".$persondiedcause."', person_description='".$persondesc."', person_nationalities='".$personnationality."', person_url='".$personurl."', person_email='".$personemail."', person_tags='".$persontags."', person_categories='".$personcats."' WHERE person_id='".$personid."'";


    $editpersonquery = mysqli_query($dbconn,$editpersonq);
    redirect($website_url."/person-list.php");
}


$page_name = "Edit ".$getpersonname;
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $editpersonq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START PERSON-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
					            <form method="post" action="person-edit.php">
					            <input type="hidden" name="person-id" id="person-id" value="<?php echo $getpersonid; ?>">
					                <table>
					                    <tr>
					                        <td><label for="person-name">Name</label></td>
					                        <td><input type="text" name="person-name" id="person-name" class="form-input-text" value="<?php echo $getpersonname; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-alias">Aliases</label></td>
					                        <td><input type="text" name="person-alias[]" id="person-alias" class="form-input-text" value="<?php echo $getpersonalias; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-birth-date">Date of birth</label></td>
					                        <td><input type="date" name="person-birth-date" id="person-birth-date" class="form-input-date" value="<?php echo $getpersonborndate; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-birth-place">Place of birth</label></td>
					                        <td><input type="text" name="person-birth-place" id="person-birth-place" class="form-input-text" value="<?php echo $getpersonbornplace; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-death-date">Date of death</label></td>
					                        <td><input type="date" name="person-death-date" id="person-death-date" class="form-input-date" value="<?php echo $getpersondiedate; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-death-place">Place of death</label></td>
					                        <td><input type="text" name="person-death-place" id="person-death-place" class="form-input-text" value="<?php echo $getpersondieplace; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-death-cause">Cause of death</label></td>
					                        <td><input type="text" name="person-death-cause" id="person-death-cause" class="form-input-text" value="<?php echo $getpersondiecause; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-desc">Mini biography</label></td>
					                        <td><textarea name="person-desc" id ="person-desc" class="form-textarea" rows="12"><?php echo $getpersondesc; ?></textarea></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-nationality">Nationalities</label></td>
					                        <td><input type="text" name="person-nationality[]" id="person-nationality" class="form-input-text" value="<?php echo $getpersonnato; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-url">Website</label></td>
					                        <td><input type="url" name="person-url" id="person-url" class="form-input-text" value="<?php echo $getpersonurl; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-email">Email address</label></td>
					                        <td><input type="email" name="person-email" id="person-email" class="form-input-text" value="<?php echo $getpersonemail; ?>"></td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-tags">Tags</label></td>
					                        <td>
					                            <select multiple name="person-tags[]" id="person-tags" class="form-select">
					<?php
					/**
					 * Get the current tags and display them
					 */
					 $gettagsq = "SELECT * FROM tag ORDER BY tag_name ASC";
					 $gettagsquery = mysqli_query($dbconn,$gettagsq);
					 if(mysqli_num_rows($gettagsquery) > 0) {
					     while ($gettagsopt = mysqli_fetch_assoc($gettagsquery)) {
					        echo "\t\t\t\t\t\t\t\t<option value=\"".$gettagsopt['tag_id']."\">".$gettagsopt['tag_name']."</option>\n";
					     }
					 }
					?>
					                            </select>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td><label for="person-cats">Categories</label></td>
					                        <td>
					                            <select multiple name="person-cats[]" id="person-cats" class="form-select">
					<?php
					/**
					 * Get the current categories and display them
					 */
					 $getcatsq = "SELECT * FROM category ORDER BY category_name ASC";
					 $getcatsquery = mysqli_query($dbconn,$getcatsq);
					 if(mysqli_num_rows($getcatsquery) > 0) {
					     while ($getcatsopt = mysqli_fetch_assoc($getcatsquery)) {
					        echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatsopt['category_id']."\">".$getcatsopt['category_name']."</option>\n";
					     }
					 }
					?>
					                            </select>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td></td>
					                        <td><input type="submit" name="person-submit" id="person-submit" class="form-input-submit" value="<?php echo _('ADD PERSON'); ?>"></td>
					                    </tr>

					                </table>
					            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END PERSON-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
