<?php
/**
 * This file is for adding a new person in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['person-submit'])) {
    $personname         = nicetext($_POST['person-name']);
    $personsort         = nicetext($_POST['person-sort']);
    $personalias        = $_POST['person-alias'];
    $personborn         = $_POST['person-birth-date'];
    $personbornplace    = nicetext($_POST['person-birth-place']);
    $persondied         = $_POST['person-death-date'];
    $persondiedplace    = nicetext($_POST['person-death-place']);
    $persondiedcause    = nicetext($_POST['person-death-cause']);
    $persondesc         = nicetext($_POST['person-desc']);
    $personnationality  = $_POST['person-nationality'];
    $personurl          = nicetext($_POST['person-url']);
    $personemail        = nicetext($_POST['person-email']);
    $persontags         = $_POST['person-tags'];
    $personcats         = $_POST['person-cats'];

    if($personborn == '') {
        $birthdate = '0000-00-00';
    } else {
        $birthdate = $personborn;
    }

    if($persondied == '') {
        $deathdate = '0000-00-00';
    } else {
        $deathdate = $persondied;
    }
    $addpersonq = "INSERT INTO person (person_name, person_sort, person_aliases, person_birth_date, person_birth_place, person_death_date, person_death_place, person_death_cause, person_description, person_nationalities, person_url, person_email, person_tags, person_categories) VALUES ('".$personname."', '".$personsort."', '".$personalias."', '".$birthdate."', '".$personbornplace."', '".$deathdate."', '".$persondiedplace."', '".$persondiedcause."', '".$persondesc."', '".$personnationality."', '".$personurl."', '".$personemail."', '".$persontags."', '".$personcats."')";
    $addpersonquery = mysqli_query($dbconn,$addpersonq);
    redirect($website_url."/person-list.php");
}


$page_name = "Add a person";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $addpersonq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START PERSON-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="person-add.php">
				                <table>
				                    <tr>
				                        <td><label for="person-name">Name</label></td>
				                        <td><input type="text" name="person-name" id="person-name" class="form-input-text" placeholder="<?php echo _('Name'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-sort">Sort as...</label></td>
				                        <td><input type="text" name="person-sort" id="person-sort" class="form-input-text" placeholder="<?php echo _('Sort as...'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-alias">Aliases</label></td>
				                        <td><input type="text" name="person-alias[]" id="person-alias" class="form-input-text" placeholder="<?php echo _('Aliases'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-birth-date">Date of birth</label></td>
				                        <td><input type="date" name="person-birth-date" id="person-birth-date" class="form-input-date"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-birth-place">Place of birth</label></td>
				                        <td><input type="text" name="person-birth-place" id="person-birth-place" class="form-input-text" placeholder="<?php echo _('City, State, Country'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-death-date">Date of death</label></td>
				                        <td><input type="date" name="person-death-date" id="person-death-date" class="form-input-date"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-death-place">Place of death</label></td>
				                        <td><input type="text" name="person-death-place" id="person-death-place" class="form-input-text" placeholder="<?php echo _('City, State, Country'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-death-cause">Cause of death</label></td>
				                        <td><input type="text" name="person-death-cause" id="person-death-cause" class="form-input-text" placeholder="<?php echo _('If known'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-desc">Mini biography</label></td>
				                        <td><textarea name="person-desc" id ="person-desc" class="form-textarea" rows="12"></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-nationality">Nationalities</label></td>
				                        <td><input type="text" name="person-nationality[]" id="person-nationality" class="form-input-text" placeholder="<?php echo _('Can be more than one'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-url">Website</label></td>
				                        <td><input type="url" name="person-url" id="person-url" class="form-input-text" placeholder="<?php echo _('https://www.example.com'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-email">Email address</label></td>
				                        <td><input type="email" name="person-email" id="person-email" class="form-input-text" placeholder="<?php echo _('celebrity@example.com'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-tags">Tags</label></td>
				                        <td>
				                            <select multiple name="person-tags[]" id="person-tags" class="form-select">
				<?php
				/**
				 * Get the current tags and display them
				 */
				 $gettagsq = "SELECT * FROM tag ORDER BY tag_name ASC";
				 $gettagsquery = mysqli_query($dbconn,$gettagsq);
				 if(mysqli_num_rows($gettagsquery) > 0) {
				     while ($gettagsopt = mysqli_fetch_assoc($gettagsquery)) {
				        echo "\t\t\t\t\t\t\t\t<option value=\"".$gettagsopt['tag_id']."\">".$gettagsopt['tag_name']."</option>\n";
				     }
				 }
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td><label for="person-cats">Categories</label></td>
				                        <td>
				                            <select multiple name="person-cats[]" id="person-cats" class="form-select">
				<?php
				/**
				 * Get the current categories and display them
				 */
				 $getcatsq = "SELECT * FROM category ORDER BY category_name ASC";
				 $getcatsquery = mysqli_query($dbconn,$getcatsq);
				 if(mysqli_num_rows($getcatsquery) > 0) {
				     while ($getcatsopt = mysqli_fetch_assoc($getcatsquery)) {
				        echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatsopt['category_id']."\">".$getcatsopt['category_name']."</option>\n";
				     }
				 }
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="person-submit" id="person-submit" class="form-input-submit" value="<?php echo _('ADD PERSON'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END PERSON-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
