<?php
/**
 * This file is for editing categories in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this category */
if (isset($_GET["catid"])) {
    $get_id = $_GET["catid"];
} else {
    $get_id = "";
}

/**
 * Get information on this category to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getcatq = "SELECT * FROM category WHERE category_id=".$get_id."";
    $getcatquery = mysqli_query($dbconn,$getcatq);

    while ($getcatopt = mysqli_fetch_assoc($getcatquery)) {
        $getcatid       = $getcatopt['category_id'];
        $getcatname     = $getcatopt['category_name'];
        $getcatslug     = $getcatopt['category_slug'];
        $getcatparent   = $getcatopt['category_parent'];
        $getcatdesc     = $getcatopt['category_description'];
        $getcatcolor    = $getcatopt['category_color'];
    }
}

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['category-submit'])) {
    $category_id        = $_POST['category-id'];
    $category_name      = nicetext($_POST['category-name']);
    $category_slug      = makeslug($_POST['category-slug']);
    $category_parent    = $_POST['category-parent'];
    $category_desc      = nicetext($_POST['category-desc']);
    $category_color     = ltrim($_POST['category-color'],"#");

    /** Here is our query */
    $editcategoryq  = "UPDATE category SET category_name='".$category_name."', category_slug='".$category_slug."', category_parent='".$category_parent."', category_description='".$category_desc."', category_color='".$category_color."' WHERE category_id='".$category_id."'";
    $editcategoryquery = mysqli_query($dbconn,$editcategoryq);
    redirect($website_url."/category-list.php");
}


$page_name = "Edit ".$getcatname;
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $editcategoryq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START CATEGORY-EDIT.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="category-edit.php">
				            <input type="hidden" name="category-id" id="category-id" value="<?php echo $getcatid; ?>">
				                <table>
				                    <tr>
				                        <td><label for="category-name">Category name</label></td>
				                        <td><input type="text" name="category-name" id="category-name" class="form-input-text" value="<?php echo $getcatname; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-slug">Category slug</label></td>
				                        <td><input type="text" name="category-slug" id="category-slug" class="form-input-text" value="<?php echo $getcatslug; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-parent">Category parent</label></td>
				                        <td>
				                            <select name="category-parent" id="category-parent" class="form-select">
				                                <option value="0">---</option>
				<?php
				/**
				 * Get the current categories and display them
				 */
				$getcatparentq = "SELECT * FROM category ORDER BY category_name ASC";
				$getcatparentquery = mysqli_query($dbconn,$getcatparentq);
				if(mysqli_num_rows($getcatparentquery) > 0) {
				    while ($getcatparentopt = mysqli_fetch_assoc($getcatparentquery)) {
				        if ($getcatparentopt['category_id'] === $getcatparent) {
				           echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatparent."\" selected>".$getcatparentopt['category_name']."</option>\n";
				       } else {
				           echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatparentopt['category_id']."\">".$getcatparentopt['category_name']."</option>\n";
				       }
				    }
				}
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-desc">Category description</label></td>
				                        <td><textarea name="category-desc" id ="category-desc" class="form-textarea" rows="12"><?php echo $getcatdesc; ?></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-color">Category color</label></td>
				                        <td><input type="color" name="category-color" id="category-color" class="form-input-color" value="#<?php echo $getcatcolor; ?>"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="category-submit" id="category-submit" class="form-input-submit" value="<?php echo _('UPDATE CATEGORY'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END CATEGORY-EDIT.PHP -->
<?php require 'gadmin-footer.php'; ?>
