<?php
/**
 * This file is for creating new categories in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['category-submit'])) {
    $category_name      = nicetext($_POST['category-name']);
    $category_slug      = makeslug($_POST['category-name']);
    $category_parent    = $_POST['category-parent'];
    $category_desc      = nicetext($_POST['category-desc']);
    $category_color     = ltrim($_POST['category-color'],"#");

    /** Here is our query */
    $addcategoryq  = "INSERT INTO category (category_name, category_slug, category_parent, category_description, category_color) VALUES ('".$category_name."', '".$category_slug."', '".$category_parent."', '".$category_desc."', '".$category_color."')";
    $addcategoryquery = mysqli_query($dbconn,$addcategoryq);
    redirect($website_url."/category-list.php");
}


$page_name = "Add a category";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $addcategoryq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START CATEGORY-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="category-add.php">
				                <table>
				                    <tr>
				                        <td><label for="category-name">Category name</label></td>
				                        <td><input type="text" name="category-name" id="category-name" class="form-input-text" placeholder="<?php echo _('Add category name'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-parent">Category parent</label></td>
				                        <td>
				                            <select name="category-parent" id="category-parent" class="form-select">
				                                <option value="0">---</option>
				<?php
				/**
				 * Get the current categories and display them
				 */
				 $getcatsq = "SELECT * FROM category ORDER BY category_name ASC";
				 $getcatsquery = mysqli_query($dbconn,$getcatsq);
				 if(mysqli_num_rows($getcatsquery) > 0) {
				     while ($getcatsopt = mysqli_fetch_assoc($getcatsquery)) {
				        echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatsopt['category_id']."\">".$getcatsopt['category_name']."</option>\n";
				     }
				 }
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-desc">Category description</label></td>
				                        <td><textarea name="category-desc" id ="category-desc" class="form-textarea" rows="12"></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="category-color">Category color</label></td>
				                        <td><input type="color" name="category-color" id="category-color" class="form-input-color" placeholder="ffffff"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="category-submit" id="category-submit" class="form-input-submit" value="<?php echo _('ADD CATEGORY'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END CATEGORY-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
