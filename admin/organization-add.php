<?php
/**
 * This file is for adding a new organization in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['organization-submit'])) {
    $organizationname           = nicetext($_POST['organization-name']);
    $organizationalias          = nicetext($_POST['organization-alias']);
    $organizationstart          = $_POST['organization-start-date'];
    $organizationend            = $_POST['organization-end-date'];
    $organizationdesc           = nicetext($_POST['organization-desc']);
    $organizationplace          = nicetext($_POST['organization-place']);
    $organizationurl            = nicetext($_POST['organization-url']);
    $organizationemail          = nicetext($_POST['organization-email']);
    $organizationtags           = $_POST['organization-tags'];
    $organizationcats           = $_POST['organization-cats'];

    if($organizationstart == '') {
        $startdate = '0000-00-00';
    } else {
        $startdate = $organizationstart;
    }

    if($organizationend == '') {
        $enddate = '0000-00-00';
    } else {
        $enddate = $organizationend;
    }
    $addorganizationq = "INSERT INTO organization (organization_name, organization_aliases, organization_start_date, organization_end_date, organization_description, organization_place, organization_url, organization_email, organization_tags, organization_categories) VALUES ('".$organizationname."', '".$organizationalias."', '".$startdate."', '".$enddate."', '".$organizationdesc."', '".$organizationplace."', '".$organizationurl."', '".$organizationemail."', '".$organizationtags."', '".$organizationcats."')";
    $addorganizationquery = mysqli_query($dbconn,$addorganizationq);
    redirect($website_url."/organization-list.php");
}


$page_name = "Add an organization";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $addorganizationq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START organization-ADD.PHP -->
        <main>
            <div class="container">                     <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="organization-add.php">
				                <table>
				                    <tr>
				                        <td><label for="organization-name">Name</label></td>
				                        <td><input type="text" name="organization-name" id="organization-name" class="form-input-text" placeholder="<?php echo _('Name'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-alias">Aliases</label></td>
				                        <td><input type="text" name="organization-alias[]" id="organization-alias" class="form-input-text" placeholder="<?php echo _('Aliases'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-start-date">Start date</label></td>
				                        <td><input type="date" name="organization-start-date" id="organization-start-date" class="form-input-date"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-end-date">End date</label></td>
				                        <td><input type="date" name="organization-end-date" id="organization-end-date" class="form-input-date"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-desc">Description</label></td>
				                        <td><textarea name="organization-desc" id ="organization-desc" class="form-textarea" rows="12"></textarea></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-place">Places</label></td>
				                        <td><input type="text" name="organization-place[]" id="organization-place" class="form-input-text" placeholder="<?php echo _('Can be more than one'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-url">Website</label></td>
				                        <td><input type="url" name="organization-url" id="organization-url" class="form-input-text" placeholder="<?php echo _('https://www.example.com'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-email">Email address</label></td>
				                        <td><input type="email" name="organization-email" id="organization-email" class="form-input-text" placeholder="<?php echo _('celebrity@example.com'); ?>"></td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-tags">Tags</label></td>
				                        <td>
				                            <select multiple name="organization-tags[]" id="organization-tags" class="form-select">
				<?php
				/**
				 * Get the current tags and display them
				 */
				 $gettagsq = "SELECT * FROM tag ORDER BY tag_name ASC";
				 $gettagsquery = mysqli_query($dbconn,$gettagsq);
				 if(mysqli_num_rows($gettagsquery) > 0) {
				     while ($gettagsopt = mysqli_fetch_assoc($gettagsquery)) {
				        echo "\t\t\t\t\t\t\t\t<option value=\"".$gettagsopt['tag_id']."\">".$gettagsopt['tag_name']."</option>\n";
				     }
				 }
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td><label for="organization-cats">Categories</label></td>
				                        <td>
				                            <select multiple name="organization-cats[]" id="organization-cats" class="form-select">
				<?php
				/**
				 * Get the current categories and display them
				 */
				 $getcatsq = "SELECT * FROM category ORDER BY category_name ASC";
				 $getcatsquery = mysqli_query($dbconn,$getcatsq);
				 if(mysqli_num_rows($getcatsquery) > 0) {
				     while ($getcatsopt = mysqli_fetch_assoc($getcatsquery)) {
				        echo "\t\t\t\t\t\t\t\t<option value=\"".$getcatsopt['category_id']."\">".$getcatsopt['category_name']."</option>\n";
				     }
				 }
				?>
				                            </select>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" name="organization-submit" id="organization-submit" class="form-input-submit" value="<?php echo _('ADD organization'); ?>"></td>
				                    </tr>

				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END organization-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
