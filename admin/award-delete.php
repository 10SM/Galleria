<?php
/**
 * This file is for deleting awards in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this category */
if (isset($_GET["awardid"])) {
    $get_id = $_GET["awardid"];
} else {
    $get_id = "";
}

/**
 * Get information on this award to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getawardq = "SELECT * FROM award WHERE award_id=".$get_id."";
    $getawardquery = mysqli_query($dbconn,$getawardq);

    while ($getawardopt = mysqli_fetch_assoc($getawardquery)) {
        $getawardid       = $getawardopt['award_id'];
        $getawardname     = $getawardopt['award_name'];
    }
}

/**
 * Delete the award from the DB or go back to the list
 */
if (isset($_POST['award-delete'])) {
    $award_id     = $_POST['award-id'];

    /** Here is our query */
    $deleteawardq  = "DELETE from award WHERE award_id='".$award_id."'";
    $deleteawardquery = mysqli_query($dbconn,$deleteawardq);
    redirect($website_url."/award-list.php");
} else if (isset($_POST['award-cancel'])) {
    redirect($website_url."/award-list.php");
}


$page_name = "Delete ".$getawardname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deleteawardq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START AWARD-DELETE.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
				            <form method="post" action="award-delete.php">
				            <input type="hidden" name="award-id" id="award-id" value="<?php echo $getawardid; ?>">
				                <table>
				                    <tr>
				                        <td><input type="submit" name="award-cancel" id="award-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
				                        <td><input type="submit" name="award-delete" id="award-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
				                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END AWARD-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
