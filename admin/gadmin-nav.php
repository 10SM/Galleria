<!-- -------------------------------------------------------------------------- START ADMIN/GADMIN-NAV.PHP -->
        <nav>
            <button class="accordion">Audio</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="audio-list.php">Audio files</a></li>
                        <li><a href="audio-collection-list.php">Audio collections</a></li>
                        <li><a href="audio-add.php">Add audio</a></li>
                        <li><a href="audio-type-add.php">Add audio type</a></li>
                        <li><a href="audio-collection-add.php">Add audio collection</a></li>
                        <li><a href="audio-collection-type-add.php">Add audio collection type</a></li>
                    </ul>
                </div>
            <button class="accordion">Images</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="image-list.php">Images</a></li>
                        <li><a href="image-collection-list.php">Image collections</a></li>
                        <li><a href="image-add.php">Add image</a></li>
                        <li><a href="image-type-add.php">Add image type</a></li>
                        <li><a href="image-collection-add.php">Add image collection</a></li>
                        <li><a href="image-collection-type-add.php">Add image collection type</a></li>
                    </ul>
                </div>
            <button class="accordion">Texts</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="text-list.php">Texts</a></li>
                        <li><a href="text-collection-list.php">Text collections</a></li>
                        <li><a href="text-add.php">Add text</a></li>
                        <li><a href="text-type-add.php">Add text type</a></li>
                        <li><a href="text-collection-add.php">Add text collection</a></li>
                        <li><a href="text-collection-type-add.php">Add text collection type</a></li>
                    </ul>
                </div>
            <button class="accordion">Videos</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="video-list.php">Videos</a></li>
                        <li><a href="video-collection-list.php">Video collections</a></li>
                        <li><a href="video-add.php">Add video</a></li>
                        <li><a href="video-type-add.php">Add video type</a></li>
                        <li><a href="video-collection-add.php">Add video collection</a></li>
                        <li><a href="video-collection-type-add.php">Add video collection type</a></li>
                    </ul>
                </div>
            <button class="accordion">Awards</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="award-list.php">Awards</a></li>
                        <li><a href="award-collection-list.php">Award collections</a></li>
                        <li><a href="award-add.php">Add award</a></li>
                        <li><a href="award-type-add.php">Add award type</a></li>
                        <li><a href="award-collection-add.php">Add award collection</a></li>
                        <li><a href="award-collection-type-add.php">Add award collection type</a></li>
                    </ul>
                </div>
            <button class="accordion">People</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="person-list.php">All people</a></li>
                        <li><a href="person-add.php">Add person</a></li>
                    </ul>
                </div>
            <button class="accordion">Organizations</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="organization-list.php">All organizations</a></li>
                        <li><a href="organization-add.php">Add organization</a></li>
                    </ul>
                </div>
            <button class="accordion">Tags</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="tag-list.php">All tags</a></li>
                        <li><a href="tag-add.php">Add tag</a></li>
                    </ul>
                </div>
            <button class="accordion">Categories</button>
                <div class="panel">
                    <ul class="sidebar-list">
                        <li><a href="category-list.php">All categories</a></li>
                        <li><a href="category-add.php">Add category</a></li>
                    </ul>
                </div>
        </nav>
<!-- -------------------------------------------------------------------------- END ADMIN/GADMIN-NAV.PHP -->
