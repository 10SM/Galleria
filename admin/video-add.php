<?php
/**
 * This file is for adding a single video file to Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['video-submit'])) {
    /** borrowing heavily from https://www.w3schools.com/php/php_file_upload.asp */
    $videodir       = "../media/videos/";
    $videofile      = basename($_FILES["video-file"]["name"]);
    $videocheck     = 1;

    /** Get the video file type */
    $videotype      = strtolower(pathinfo($videofile,PATHINFO_EXTENSION));
    if ($videotype != "mp4" && $videotype != "webm" && $videotype != "ogg") {
        echo "<span>File is not a video file type</span><br>\n";
        $videocheck = 0;
    }

    /** If the audio file passes the checks above, let's get ready to upload it */
    if ($videocheck !== 0) {

        /** Get today's date and create a folder for it in media/videos if it doesn't already exist */
        $thisyear       = date("Y");
        $thismonth      = date("m");
        $thisday        = date("d");

        if (!is_dir($videodir.$thisyear)) {
            mkdir($videodir.$thisyear,0777,true);
            echo "<span>".$videodir.$thisyear." directory was created.</span><br>\n";
        } else {
            echo "<span>".$videodir.$thisyear." is a directory</span><br>\n";
        }

        if (!is_dir($videodir.$thisyear."/".$thismonth)) {
            mkdir($videodir.$thisyear."/".$thismonth,0777,true);
        }

        if (!is_dir($videodir.$thisyear."/".$thismonth."/".$thisday)) {
            mkdir($videodir.$thisyear."/".$thismonth."/".$thisday,0777,true);
        }

        /** Based on the folder created above, determine the file path of the video file */
        $videopath = $videodir.$thisyear."/".$thismonth."/".$thisday."/".$videofile;

        /** let's upload the video file and enter it in the DB */
        if (move_uploaded_file($_FILES["video-file"]["tmp_name"], $videopath)) {

            $addvideoq = "INSERT INTO video (video_name, video_path) VALUES ('".$videofile."', '".$videopath."')";
            $addvideoquery = mysqli_query($dbconn,$addvideoq);
            redirect($website_url."/video-list.php");

        } else {

            echo "<span>There was a problem uploading the file.</span><br>\n";

        }


    } else {
        echo "<span>The file was unable to be uploaded</span><br>\n";
    }


}


$page_name = "Upload a video file";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START VIDEO-ADD.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $page_name; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
		            <h1><?php echo $page_name; ?></h1>
			            <p class="add-new-span"><a href="video-dir-add.php">Scan a directory</a></p>
                        <p class="add-new-span"><a href="video-list.php">Return to the video list</a></p>
			            <form method="post" action="video-add.php" enctype="multipart/form-data">
			                <table>
			                    <tr>
			                        <td><input type="file" name="video-file" id="video-file" class="form-input-submit"></td>
			                    </tr>
			                    <tr>
			                        <td><input type="submit" value="Upload video file" name="video-submit" class="form-input-submit"></td>
                                </tr>
			                </table>
			            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END VIDEO-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
