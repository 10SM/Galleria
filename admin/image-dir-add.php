<?php
/**
 * This file is for scanning a directory for images and adding them to the database. It does not upload them to a separate directory in Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['image-dir-submit'])) {

    $imgdir     = $_POST['image-dir-name'];

    /** Let's make sure it is a directory */
    /** Borrowed quite a bit from https://www.w3schools.com/PHP/func_directory_readdir.asp */
    if (is_dir($imgdir)) {


        if ($thisdir = opendir($imgdir)) {

            while (($file = readdir($thisdir)) !== false) {

                /**
                 * TO DO LIST
                 * Check to see if file is an image
                 * Check to see if the file/path is already in the db
                 * Check to see if it is a supported image type
                 * Get image date (?))
                 * After these steps are done, enter the files in the DB if appropriate
                 */

                /** Set the path and insert that and the image name into the db */

                $imagepath = $imgdir."/".$file;
                $addimageq = "INSERT INTO image (image_name, image_path) VALUES ('".$file."', '".$imagepath."')";
                $addimagequery = mysqli_query($dbconn,$addimageq);
            }

            closedir($thisdir);
            redirect($website_url."/admin/image-list.php");

        }
    /** If it's not a directory */
    } else {
        echo "<span>".$imgdir." doesn't look like a directory. Please check it and try again.</span><br>\n";
    }


}


$page_name = "Scan a directory";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $imagepath."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START IMAGE-DIR-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
				            <h1><?php echo $page_name; ?></h1>
				            <p class="add-new-span"><a href="image-add.php">Upload a single image</a></p>
                            <p class="add-new-span"><a href="image-list.php">Return to the image list</a></p>
				            <form method="post" action="image-dir-add.php" enctype="multipart/form-data">
				                <table>
				                    <tr>
				                        <td><label for="image-dir">Directory</label></td>
				                        <td><input type="text" name="image-dir-name" id="image-dir-name" class="form-input-text"></td>
				                    </tr>
				                    <tr>
				                        <td></td>
				                        <td><input type="submit" value="Scan Directory" name="image-dir-submit" class="form-input-submit"></td>
                                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END IMAGE-DIR-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
