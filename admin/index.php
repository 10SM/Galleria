<?php
// SET THE NAME OF THIS PAGE
$page_name = "Galleria Dashboard";

// REQUIRE HEADER AND NAV
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START ADMIN/INDEX.PHP -->
        <main>
            <h1><?php echo $page_name; ?></h1>
            <p>This area will have dashboard-y things in the future.</p>


        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END ADMIN/INDEX.PHP -->
<?php require 'gadmin-footer.php'; ?>
