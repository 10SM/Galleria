<?php
/**
 * This file allows a user to edit information Galleria has about an image.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';


/** get the ID for this image */
if (isset($_GET["imageid"])) {
    $get_id = $_GET["imageid"];
} else {
    $get_id = "";
}

/**
 * Get information about this image to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getimageq = "SELECT * FROM image WHERE image_id='".$get_id."'";
    $getimagequery = mysqli_query($dbconn,$getimageq);

    while ($getimageopt = mysqli_fetch_assoc($getimagequery)) {
        $getimageid         = $getimageopt['image_id'];
        $getimagetype       = $getimageopt['image_type'];
        $getimagename       = $getimageopt['image_name'];
        $getimagepath       = $getimageopt['image_path'];
        $getimagedesc       = $getimageopt['image_description'];
        $getimageppl        = $getimageopt['image_people'];
        $getimageorgs       = $getimageopt['image_organizations'];
        $getimagetags       = $getimageopt['image_tags'];
        $getimagecats       = $getimageopt['image_categories'];
        $getimagedate       = $getimageopt['image_date'];
    }
}

/** Process the submitted form */
if (isset($_POST['image-submit'])) {

    /** Get the form inputs */
    $pimageid        = $_POST['image-id'];
    $pimagename      = nicetext($_POST['image-name']);
    $pimagedesc      = nicetext($_POST['image-desc']);
    $pimageppl       = $_POST['image-ppl'];
    $pimageorgs      = $_POST['image-orgs'];
    $pimagetags      = $_POST['image-tags'];
    $pimagecats      = $_POST['image-cats'];
    $pimagetype      = $_POST['image-type'];
    $pimagedate      = $_POST['image-date'];

    $editimageq = "UPDATE image SET image_type='".$pimagetype."', image_name='".$pimagename."', image_description='".$pimagedesc."' WHERE image_id=".$pimageid;
    $editimagequery = mysqli_query($dbconn,$editimageq);
    #redirect($website_url."/image-list.php");
}


$page_name = $getimagename;
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $editimageq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START IMAGE.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <img src="thumb.php?imageid=<?php echo $getimageid; ?>" alt="<?php echo $getimagedesc; ?>" title="<?php echo $getimagename; ?>" class="image-main">
                        <!-- table for more detailed metadata. we don't want the sidebar to look too busy/crowded -->
                        <form method="post" action="image-meta-edit.php">
                            <input type="hidden" name="image-id" id="image-id" value="<?php echo $getimageid; ?>">
                            <table>
                                <tr>
                                    <td><label for="image-name">Name</label></td>
                                    <td><input type="text"name="image-name" id="image-name" class="form-input-text" value="<?php echo $getimagename; ?>"></td>
                                </tr>
                                <tr>
                                    <td><label for="image-desc">Description</label></td>
                                    <td><textarea name="image-desc" id ="image-desc" class="form-textarea" rows="12"><?php echo $getimagedesc; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td><label for="image-path">Path</label></td>
                                    <td><input type="text" name="image-path" id="image-path" class="form-input-text" value="<?php echo $getimagepath; ?>" readonly></td>
                                </tr>
                                <tr>
                                    <td><label for="image-ppl">People</label></td>
                                    <td>
                                        <select multiple name="image-ppl[]" id="image-ppl" class="form-select">
<?php
/** get the list of people */
$personselectq = "SELECT * FROM person ORDER BY person_name ASC";
$personselectquery = mysqli_query($dbconn,$personselectq);
while ($personselectopt = mysqli_fetch_assoc($personselectquery)) {
    $personselectid     = $personselectopt['person_id'];
    $personselectname   = $personselectopt['person_name'];

    if ($getimageppl == $personselectid) {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$personselectid."\" selected>".$personselectname."</option>\n";
    } else {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$personselectid."\">".$personselectname."</option>\n";
    }
}
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="image-orgs">Organizations</label></td>
                                    <td>
                                        <select multiple name="image-orgs[]" id="image-orgs" class="form-select">
<?php
/** get the list of organizations */
$orgsselectq = "SELECT * FROM organization ORDER BY organization_name ASC";
$orgsselectquery = mysqli_query($dbconn,$orgsselectq);
while ($orgsselectopt = mysqli_fetch_assoc($orgsselectquery)) {
    $orgsselectid     = $orgsselectopt['organization_id'];
    $orgsselectname   = $orgsselectopt['organization_name'];

    if ($getimageorgs == $orgsselectid) {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$orgsselectid."\" selected>".$orgsselectname."</option>\n";
    } else {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$orgsselectid."\">".$orgsselectname."</option>\n";
    }
}
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="image-tags">Tags</label></td>
                                    <td>
                                        <select multiple name="image-tags[]" id="image-tags" class="form-select">
<?php
/** get the list of tags */
$tagsselectq = "SELECT * FROM tag ORDER BY tag_name ASC";
$tagsselectquery = mysqli_query($dbconn,$tagsselectq);
while ($tagsselectopt = mysqli_fetch_assoc($tagsselectquery)) {
    $tagsselectid     = $tagsselectopt['tag_id'];
    $tagsselectname   = $tagsselectopt['tag_name'];

    if ($getimagetags == $tagsselectid) {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$tagsselectid."\" selected>".$tagsselectname."</option>\n";
    } else {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$tagsselectid."\">".$tagsselectname."</option>\n";
    }
}
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="image-cats">Categories</label></td>
                                    <td>
                                        <select multiple name="image-cats[]" id="image-cats" class="form-select">
<?php
/** get the list of categories */
$catsselectq = "SELECT * FROM category ORDER BY category_name ASC";
$catsselectquery = mysqli_query($dbconn,$catsselectq);
while ($catsselectopt = mysqli_fetch_assoc($catsselectquery)) {
    $catsselectid     = $catsselectopt['category_id'];
    $catsselectname   = $catsselectopt['category_name'];

    if ($getimagecats == $catsselectid) {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$catsselectid."\" selected>".$catsselectname."</option>\n";
    } else {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$catsselectid."\">".$catsselectname."</option>\n";
    }
}
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="image-type">Type</label></td>
                                    <td>
                                        <select name="image-type" id="image-type" class="form-select">
<?php
/** gets the list of image types */
$imagetypeselectq = "SELECT * FROM image_type ORDER BY image_type_name ASC";
$imagetypeselectquery = mysqli_query($dbconn, $imagetypeselectq);
while ($imagetypeselectopt = mysqli_fetch_assoc($imagetypeselectquery)) {
    $imagetypeselectid      = $imagetypeselectopt['image_type_id'];
    $imagetypeselectname    = $imagetypeselectopt['image_type_name'];

    if ($getimagetype == $imagetypeselectid) {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$imagetypeselectid."\" selected>".$imagetypeselectname."</option>\n";
    } else {
        echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"".$imagetypeselectid."\">".$imagetypeselectname."</option>\n";
    }
}
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="image-date">Date</label></td>
                                    <td><input type="date" name="image-date" id="image-date" class="form-input-date" value="<?php echo $getimagedate; ?>"></td>
                                </tr>
                                <tr>
			                        <td></td>
			                        <td><input type="submit" name="image-submit" id="image-submit" class="form-input-submit" value="<?php echo _('Save'); ?>"></td>
			                    </tr>
                            </table>
                        </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END IMAGE.PHP -->
<?php require 'gadmin-footer.php'; ?>
