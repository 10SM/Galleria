<?php
/**
 * This file is for adding a single audio file to Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['audio-submit'])) {
    /** borrowing heavily from https://www.w3schools.com/php/php_file_upload.asp */
    $audiodir       = "../media/audios/";
    $audiofile      = basename($_FILES["audio-file"]["name"]);
    $audiocheck     = 1;

    /** Get the audio file type */
    $audiotype      = strtolower(pathinfo($audiofile,PATHINFO_EXTENSION));
    if ($audiotype != "mp3" && $audiotype != "wav" && $audiotype != "ogg") {
        echo "<span>File is not an audio file type</span><br>\n";
        $audiocheck = 0;
    }

    /** If the audio file passes the checks above, let's get ready to upload it */
    if ($audiocheck !== 0) {

        /** Get today's date and create a folder for it in media/audios if it doesn't already exist */
        $thisyear       = date("Y");
        $thismonth      = date("m");
        $thisday        = date("d");

        if (!is_dir($audiodir.$thisyear)) {
            mkdir($audiodir.$thisyear,0777,true);
            echo "<span>".$audiodir.$thisyear." directory was created.</span><br>\n";
        } else {
            echo "<span>".$audiodir.$thisyear." is a directory</span><br>\n";
        }

        if (!is_dir($audiodir.$thisyear."/".$thismonth)) {
            mkdir($audiodir.$thisyear."/".$thismonth,0777,true);
        }

        if (!is_dir($audiodir.$thisyear."/".$thismonth."/".$thisday)) {
            mkdir($audiodir.$thisyear."/".$thismonth."/".$thisday,0777,true);
        }

        /** Based on the folder created above, determine the file path of the audio file */
        $audiopath = $audiodir.$thisyear."/".$thismonth."/".$thisday."/".$audiofile;

        /** let's upload the audio file and enter it in the DB */
        if (move_uploaded_file($_FILES["audio-file"]["tmp_name"], $audiopath)) {

            $addaudioq = "INSERT INTO audio (audio_name, audio_path) VALUES ('".$audiofile."', '".$audiopath."')";
            $addaudioquery = mysqli_query($dbconn,$addaudioq);
            redirect($website_url."/audio-list.php");

        } else {

            echo "<span>There was a problem uploading the file.</span><br>\n";

        }


    } else {
        echo "<span>The file was unable to be uploaded</span><br>\n";
    }


}


$page_name = "Upload an audio file";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START AUDIO-ADD.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $page_name; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
		            <h1><?php echo $page_name; ?></h1>
			            <p class="add-new-span"><a href="audio-dir-add.php">Scan a directory</a></p>
                        <p class="add-new-span"><a href="audio-list.php">Return to the audio list</a></p>
			            <form method="post" action="audio-add.php" enctype="multipart/form-data">
			                <table>
			                    <tr>
			                        <td><input type="file" name="audio-file" id="audio-file" class="form-input-submit"></td>
			                    </tr>
			                    <tr>
			                        <td><input type="submit" value="Upload audio file" name="audio-submit" class="form-input-submit"></td>
                                </tr>
			                </table>
			            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END AUDIO-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
