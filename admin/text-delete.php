<?php
/**
 * This file deletes an text file from the Galleria database and removes it from the hard drive.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';


/** get the ID for this image */
if (isset($_GET["textid"])) {
    $get_id = $_GET["textid"];
} else {
    $get_id = "";
}

/**
 * Get information on this text file to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $gettextq = "SELECT * FROM text WHERE text_id='".$get_id."'";
    $gettextquery = mysqli_query($dbconn,$gettextq);

    while ($gettextopt = mysqli_fetch_assoc($gettextquery)) {
        $gettextid         = $gettextopt['text_id'];
        $gettextname       = $gettextopt['text_name'];
        $gettextpath       = $gettextopt['text_path'];
    }
}

/**
 * Delete the text file from the DB and dir or go back to the list
 */
if (isset($_POST['text-delete'])) {
    $text_id       = $_POST['text-id'];
    $text_path     = $_POST['text-path'];

    /** Here is our query to delete it from the DB */
    $deletetextq       = "DELETE from text WHERE text_id='".$text_id."'";
    $deletetextquery   = mysqli_query($dbconn,$deletetextq);

    /** This should remove it from the hard drive */
    unlink($text_path);

    /** The actions are completed so we go back to the text list*/
    redirect($website_url."/text-list.php");

} else if (isset($_POST['text-cancel'])) {

    /** The action is canceled so we go back to the text list */
    redirect($website_url."/text-list.php");

}


$page_name = "Delete ".$gettextname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deletetextq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START TEXT-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="text-delete.php">
                    <input type="hidden" name="text-id" id="text-id" value="<?php echo $gettextid; ?>">
                    <input type="hidden" name="text-path" id="text-path" value="<?php echo $gettextpath; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="text-cancel" id="text-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="text-delete" id="text-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
		<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TEXT-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
