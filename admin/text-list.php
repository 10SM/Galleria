<?php
/**
 * This file displays a list of all text files in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** Here is our query */
$listtextq = "SELECT * FROM text ORDER BY text_name ASC";
$listtextquery = mysqli_query($dbconn,$listtextq);

$page_name = "All text files";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<!-- -------------------------------------------------------------------------- START TEXT-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="text-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listtextquery) > 0) {
    while ($listtextopt = mysqli_fetch_assoc($listtextquery)) {
        $textid      = $listtextopt['text_id'];
        $textname    = $listtextopt['text_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"text.php?textid=".$textid."\">".$textname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"text-edit.php?textid=".$textid."\">Edit</a> | <a href=\"text-delete.php?textid=".$textid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listtextquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no text files in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END TEXT-LIST.PHP -->
<?php require 'gadmin-footer.php'; ?>
