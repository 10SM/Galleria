<?php
/**
 * This file is for deleting a person in Galleria.
 */

/** Files required to go further */
require '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/** get the ID for this tag */
if (isset($_GET["personid"])) {
    $get_id = $_GET["personid"];
} else {
    $get_id = "";
}

/**
 * Get information on this person to pre-populate form values
 */
if ($get_id != '') {

    /** let's create the query */
    $getpersonq = "SELECT * FROM person WHERE person_id=".$get_id."";
    $getpersonquery = mysqli_query($dbconn,$getpersonq);

    while ($getpersonopt = mysqli_fetch_assoc($getpersonquery)) {
        $getpersonid       = $getpersonopt['person_id'];
        $getpersonname     = $getpersonopt['person_name'];
    }
}

/**
 * Delete the person from the DB or go back to the list
 */
if (isset($_POST['person-delete'])) {
    $person_id     = $_POST['person-id'];

    /** Here is our query */
    $deletepersonq  = "DELETE from person WHERE person_id='".$person_id."'";
    $deletepersonquery = mysqli_query($dbconn,$deletepersonq);
    redirect($website_url."/person-list.php");
} else if (isset($_POST['person-cancel'])) {
    redirect($website_url."/person-list.php");
}


$page_name = "Delete ".$getpersonname."?";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $deletepersonq."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START PERSON-DELETE.PHP -->
		<main>
			<div class="container">                         <!-- covers pretty much everything between the header and the footer -->
				<div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
					<div class="list-block">
						<h1><?php echo $page_name; ?></h1>
                    <form method="post" action="person-delete.php">
                    <input type="hidden" name="person-id" id="person-id" value="<?php echo $getpersonid; ?>">
                        <table>
                            <tr>
                                <td><input type="submit" name="person-cancel" id="person-cancel" class="form-input-submit" value="<?php echo _('CANCEL'); ?>"></td>
                                <td><input type="submit" name="person-delete" id="person-delete" class="form-input-delete" value="<?php echo _('DELETE'); ?>"></td>
                            </tr>
                        </table>
                    </form>
                </div> <!-- end div .horiz-block -->
            </div> <!-- end div .column-two -->
			</div> <!-- end div .container -->
		</main>
		<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END PERSON-DELETE.PHP -->
<?php require 'gadmin-footer.php'; ?>
