<?php
/**
 * This file is for adding a single image to Galleria.
 */

/** Files required to go further */
require_once '../includes/galleria-metadata.php';
require '../includes/functions.php';
require '../stats-queries.php';

/**
 * Process the data from the form before inserting it in the DB.
 */
if (isset($_POST['image-submit'])) {
    /** borrowing heavily from https://www.w3schools.com/php/php_file_upload.asp */
    $imagedir       = "../media/images/";
    $imagefile      = basename($_FILES["image-file"]["name"]);
    $imagecheck     = 1;

    /** Make sure this is an image file. The PHP function getimagesize() will fail if the file is not an image. */
    $check          = getimagesize($_FILES["image-file"]["tmp_name"]);
    if ($check !== false) {
        $imagecheck = 1;
    } else {
        echo "<span>File is not an image</span><br>\n";
        $imagecheck = 0;
    }

    /** Get the image file type */
    $imagetype      = strtolower(pathinfo($imagefile,PATHINFO_EXTENSION));
    if ($imagetype != "jpg" && $imagetype != "jpeg" && $imagetype != "gif" && $imagetype != "png" && $imagetype != "webp" && $imagetype != "svg" && $imagetype != "ico") {
        echo "<span>File is not an image type</span><br>\n";
        $imagecheck = 0;
    }

    /** If the image passes the checks above, let's get ready to upload it */
    if ($imagecheck !== 0) {

        /** Get today's date and create a folder for it in media/images if it doesn't already exist */
        $thisyear       = date("Y");
        $thismonth      = date("m");
        $thisday        = date("d");

        if (!is_dir($imagedir.$thisyear)) {
            mkdir($imagedir.$thisyear,0777,true);
            echo "<span>".$imagedir.$thisyear." directory was created.</span><br>\n";
        } else {
            echo "<span>".$imagedir.$thisyear." is a directory</span><br>\n";
        }

        if (!is_dir($imagedir.$thisyear."/".$thismonth)) {
            mkdir($imagedir.$thisyear."/".$thismonth,0777,true);
        }

        if (!is_dir($imagedir.$thisyear."/".$thismonth."/".$thisday)) {
            mkdir($imagedir.$thisyear."/".$thismonth."/".$thisday,0777,true);
        }

        /** Based on the folder created above, determine the file path of the image */
        $imagepath = $imagedir.$thisyear."/".$thismonth."/".$thisday."/".$imagefile;

        /** let's upload the image and enter it in the DB */
        if (move_uploaded_file($_FILES["image-file"]["tmp_name"], $imagepath)) {

            $addimageq = "INSERT INTO image (image_name, image_path) VALUES ('".$imagefile."', '".$imagepath."')";
            $addimagequery = mysqli_query($dbconn,$addimageq);
            redirect($website_url."/image-list.php");

        } else {

            echo "<span>There was a problem uploading the file.</span><br>\n";

        }


    } else {
        echo "<span>The file was unable to be uploaded</span><br>\n";
    }


}


$page_name = "Upload an image";
require 'gadmin-header.php';
require 'gadmin-nav.php';
?>
<?php echo $imagepath."<br>\n"; /** for testing */ ?>
<!-- -------------------------------------------------------------------------- START IMAGE-ADD.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
				            <h1><?php echo $page_name; ?></h1>
				            <p class="add-new-span"><a href="image-dir-add.php">Scan a directory</a></p>
                            <p class="add-new-span"><a href="image-list.php">Return to the image list</a></p>
				            <form method="post" action="image-add.php" enctype="multipart/form-data">
				                <table>
				                    <tr>
				                        <td><input type="file" name="image-file" id="image-file" class="form-input-submit"></td>
				                    </tr>
				                    <tr>
				                        <td><input type="submit" value="Upload Image" name="image-submit" class="form-input-submit"></td>
                                    </tr>
				                </table>
				            </form>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
        <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>
<!-- -------------------------------------------------------------------------- END IMAGE-ADD.PHP -->
<?php require 'gadmin-footer.php'; ?>
