<?php
/**
 * This file displays a list of all audio files in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listaudioq = "SELECT * FROM audio ORDER BY audio_name ASC";
$listaudioquery = mysqli_query($dbconn,$listaudioq);

$page_name = "All audio files";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START AUDIO-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/audio-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listaudioquery) > 0) {
    while ($listaudioopt = mysqli_fetch_assoc($listaudioquery)) {
        $audioid      = $listaudioopt['audio_id'];
        $audioname    = $listaudioopt['audio_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"audio.php?audioid=".$audioid."\">".$audioname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"admin/audio-edit.php?audioid=".$audioid."\">Edit</a> | <a href=\"admin/audio-delete.php?audioid=".$audioid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listaudioquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no audio files in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END AUDIO-LIST.PHP -->
<?php require 'footer.php'; ?>
