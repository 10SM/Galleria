<?php
// files required to go further
require_once 'includes/galleria-metadata.php';
require 'includes/functions.php';

/**
 * Get the number of images in the database
 */
$imgstatsq = "SELECT * FROM image";
$imgstatsquery = mysqli_query($dbconn,$imgstatsq);
$imgstatsnum = mysqli_num_rows($imgstatsquery);

/**
 * Get the number of videos in the database
 */
$vidstatsq = "SELECT * FROM video";
$vidstatsquery = mysqli_query($dbconn,$vidstatsq);
$vidstatsnum = mysqli_num_rows($vidstatsquery);

/**
 * Get the number of audio files in the database
 */
$audstatsq = "SELECT * FROM audio";
$audstatsquery = mysqli_query($dbconn,$audstatsq);
$audstatsnum = mysqli_num_rows($audstatsquery);

/**
 * Get the number of texts in the database
 */
$txtstatsq = "SELECT * FROM text";
$txtstatsquery = mysqli_query($dbconn,$txtstatsq);
$txtstatsnum = mysqli_num_rows($txtstatsquery);

/**
 * Get the number of awards in the database
 */
$awastatsq = "SELECT * FROM award";
$awastatsquery = mysqli_query($dbconn,$awastatsq);
$awastatsnum = mysqli_num_rows($awastatsquery);

/**
 * Get the number of people in the database
 */
$pplstatsq = "SELECT * FROM person";
$pplstatsquery = mysqli_query($dbconn,$pplstatsq);
$pplstatsnum = mysqli_num_rows($pplstatsquery);

/**
 * Get the number of organizations in the database
 */
$orgstatsq = "SELECT * FROM organization";
$orgstatsquery = mysqli_query($dbconn,$orgstatsq);
$orgstatsnum = mysqli_num_rows($orgstatsquery);

/**
 * Get the number of tags in the database
 */
$tagstatsq = "SELECT * FROM tag";
$tagstatsquery = mysqli_query($dbconn,$tagstatsq);
$tagstatsnum = mysqli_num_rows($tagstatsquery);

/**
 * Get the number of categories in the database
 */
$catstatsq = "SELECT * FROM category";
$catstatsquery = mysqli_query($dbconn,$catstatsq);
$catstatsnum = mysqli_num_rows($catstatsquery);

$page_name = $site_name;
require 'header.php';

?>
<!-- -------------------------------------------------------------------------- START INDEX.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $site_name; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
                    <div class="vert-block">                <!-- picture-of-the-day: a function randomly chooses a picture to display each day -->
                        <h2 class="block-title">Random image</h2>
                        <figure class="potd">
<?php
/** Get a random image ID */
$randimgq = "SELECT * FROM image ORDER BY RAND() LIMIT 1";
$randimgquery = mysqli_query($dbconn,$randimgq);
while ($randimgopt = mysqli_fetch_assoc($randimgquery)) {
    $randimgid      = $randimgopt['image_id'];
    $randimgname    = $randimgopt['image_name'];
    $randimgdesc    = $randimgopt['image_description'];

    echo "\t\t\t\t\t\t\t\t<img src=\"thumb.php?imageid=".$randimgid."\" alt=\"".$randimgdesc."\" title=\"".$randimgname."\">\n";
    echo "\t\t\t\t\t\t\t\t<figcaption>".$randimgname."</figcaption>\n";
}
?>
                        </figure>
                    </div> <!-- end div .vert-block -->
                    <div class="vert-block">                <!-- statistics showing numbers of images, videos, audios, texts, people, companies, awards, etc -->
                        <h2 class="block-title">Statistics</h2>
                        <table>
                            <tr>
                                <td class="num"><?php echo $imgstatsnum; ?></td>
                                <td>images</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $audstatsnum; ?></td>
                                <td>audio files</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $vidstatsnum; ?></td>
                                <td>videos</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $txtstatsnum; ?></td>
                                <td>texts</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $awastatsnum; ?></td>
                                <td>awards</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $pplstatsnum; ?></td>
                                <td>people</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $orgstatsnum; ?></td>
                                <td>organizations</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $tagstatsnum; ?></td>
                                <td>tags</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $catstatsnum; ?></td>
                                <td>categories</td>
                            </tr>
                        </table>
                        <p><a href="https://gottahavacuppamocha.com">Galleria</a> <?php echo $version; ?></p>
                    </div> <!-- end div .vert-block -->
                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent images</h2>
<?php
/**
 * Display tiles only if there are images in the DB
 */
if ($imgstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no images in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent images from the database
 */
$imgfetchq = "SELECT * FROM image ORDER BY image_added_timestamp DESC LIMIT 5";
$imgfetchquery = mysqli_query($dbconn,$imgfetchq);

    while($imgfetchopt = mysqli_fetch_assoc($imgfetchquery)) {

        $imageid       = $imgfetchopt['image_id'];
        $imageadded    = $imgfetchopt['image_added_timestamp'];
        $imagename     = $imgfetchopt['image_name'];
        $imagepath     = $imgfetchopt['image_path'];


        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($imagepath != '') {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"thumb.php?imageid=".$imageid."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$imageid."\" title=\"".$imagename."\"><img src=\"includes/generic-image.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent videos</h2>
<?php
/**
 * Display tiles only if there are videos in the DB
 */
if ($vidstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no videos in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent videos from the database
 */
$vidfetchq = "SELECT * FROM video ORDER BY video_added_timestamp DESC LIMIT 5";
$vidfetchquery = mysqli_query($dbconn,$vidfetchq);

    while($vidfetchopt = mysqli_fetch_assoc($vidfetchquery)) {

        $videoid       = $vidfetchopt['video_id'];
        $videoadded    = $vidfetchopt['video_added_timestamp'];
        $videoname     = $vidfetchopt['video_name'];
        $videothumb    = $vidfetchopt['video_thumbnail'];


        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($videothumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"video.php?videoid=".$videoid."\" title=\"".$videoname."\"><img src=\"thumb.php?imageid=".$videothumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"video.php?videoid=".$videoid."\" title=\"".$videoname."\"><img src=\"includes/generic-video.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent audio</h2>
<?php
/**
 * Display tiles only if there are audio files in the DB
 */
if ($audstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no audio files in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent audio files from the database
 */
$audfetchq = "SELECT * FROM audio ORDER BY audio_added_timestamp DESC LIMIT 5";
$audfetchquery = mysqli_query($dbconn,$audfetchq);

    while($audfetchopt = mysqli_fetch_assoc($audfetchquery)) {

        $audioid       = $audfetchopt['audio_id'];
        $audioadded    = $audfetchopt['audio_added_timestamp'];
        $audioname     = $audfetchopt['audio_name'];
        $audiothumb    = $audfetchopt['audio_thumbnail'];


        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($audiothumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"audio.php?audioid=".$audioid."\" title=\"".$audioname."\"><img src=\"thumb.php?imageid=".$audiothumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"audio.php?audioid=".$audioid."\" title=\"".$audioname."\"><img src=\"includes/generic-audio.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent texts</h2>
<?php
/**
 * Display tiles only if there are texts in the DB
 */
if ($txtstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no texts in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent texts from the database
 */
$txtfetchq = "SELECT * FROM text ORDER BY text_added_timestamp DESC LIMIT 5";
$txtfetchquery = mysqli_query($dbconn,$txtfetchq);

    while($txtfetchopt = mysqli_fetch_assoc($txtfetchquery)) {

        $textid       = $txtfetchopt['text_id'];
        $textadded    = $txtfetchopt['text_added_timestamp'];
        $textname     = $txtfetchopt['text_name'];
        $textthumb    = $txtfetchopt['text_thumbnail'];


        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($txtpath != '') {
            echo "\t\t\t\t\t\t\t<a href=\"text.php?textid=".$textid."\" title=\"".$textname."\"><img src=\"thumb.php?imageid=".$textthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"text.php?textid=".$textid."\" title=\"".$textname."\"><img src=\"includes/generic-text.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent awards</h2>
<?php
/**
 * Display tiles only if there are images in the DB
 */
if ($awastatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no awards in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent awards from the database
 */
$awafetchq = "SELECT * FROM award ORDER BY award_added_timestamp DESC LIMIT 5";
$awafetchquery = mysqli_query($dbconn,$awafetchq);

    while($awafetchopt = mysqli_fetch_assoc($awafetchquery)) {

        $awardid       = $awafetchopt['award_id'];
        $awardadded    = $awafetchopt['award_added_timestamp'];
        $awardname     = $awafetchopt['award_name'];
        $awardthumb    = $awafetchopt['award_thumbnail'];


        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($awardthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"award.php?awardid=".$awardid."\" title=\"".$awardname."\"><img src=\"thumb.php?imageid=".$awardthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"award.php?awardid=".$awardid."\" title=\"".$awardname."\"><img src=\"includes/generic-award.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent people</h2>
<?php
/**
 * Display tiles only if there are people in the DB
 */
if ($pplstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no people in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent people from the database
 */
$pplfetchq = "SELECT * FROM person ORDER BY person_added_timestamp DESC LIMIT 5";
$pplfetchquery = mysqli_query($dbconn,$pplfetchq);

    while($pplfetchopt = mysqli_fetch_assoc($pplfetchquery)) {

        $personid       = $pplfetchopt['person_id'];
        $personadded    = $pplfetchopt['person_added_timestamp'];
        $personname     = $pplfetchopt['person_name'];
        $personthumb    = $pplfetchopt['person_thumbnail'];
        $personaltthumb = $pplfetchopt['person_alternate_thumbnail'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";

        if ($personthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"thumb.php?imageid=".$personthumb."\" class=\"horiz-block-img\"></a>\n";
        } else if ($personaltthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"thumb.php?imageid=".$personaltthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"person.php?personid=".$personid."\" title=\"".$personname."\"><img src=\"includes/generic-person.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                    <div class="horiz-block">
                        <h2 class="block-title">Recent organizations</h2>
<?php
/**
 * Display tiles only if there are organizations in the DB
 */
if ($pplstatsnum == 0) {
    echo "\t\t\t\t\t\t<span>There are no organizations in the database at the moment.</span>\n";
} else {

/**
 * Get the five most recent people from the database
 */
$orgfetchq = "SELECT * FROM organization ORDER BY organization_added_timestamp DESC LIMIT 5";
$orgfetchquery = mysqli_query($dbconn,$orgfetchq);

    while($orgfetchopt = mysqli_fetch_assoc($orgfetchquery)) {

        $organizationid       = $orgfetchopt['organization_id'];
        $organizationadded    = $orgfetchopt['organization_added_timestamp'];
        $organizationname     = $orgfetchopt['organization_name'];
        $organizationlogo     = $orgfetchopt['organization_logo'];
        $organizationthumb    = $orgfetchopt['organization_thumbnail'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";

        if ($organizationlogo != '') {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"thumb.php?imageid=".$organizationlogo."\" class=\"horiz-block-img\"></a>\n";
        } else if ($organizationthumb != '') {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"thumb.php?imageid=".$organizationthumb."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"organization.php?orgid=".$organizationid."\" title=\"".$organizationname."\"><img src=\"includes/generic-organization.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
}
?>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END INDEX.PHP -->
<?php require 'footer.php'; ?>
