<?php
/**
 * Gets some site statistics from the database
 */
?>
                    <div class="vert-block">                <!-- statistics showing numbers of images, videos, audios, texts, people, companies, awards, etc -->
                        <h2 class="block-title">Statistics</h2>
                        <table>
                            <tr>
                                <td class="num"><?php echo $imgstatsnum; ?></td>
                                <td>images</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $audstatsnum; ?></td>
                                <td>audio files</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $vidstatsnum; ?></td>
                                <td>videos</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $txtstatsnum; ?></td>
                                <td>texts</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $awastatsnum; ?></td>
                                <td>awards</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $pplstatsnum; ?></td>
                                <td>people</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $orgstatsnum; ?></td>
                                <td>organizations</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $tagstatsnum; ?></td>
                                <td>tags</td>
                            </tr>
                            <tr>
                                <td class="num"><?php echo $catstatsnum; ?></td>
                                <td>categories</td>
                            </tr>
                        </table>
                        <p><a href="https://codeberg.org/10SM/Galleria">Galleria</a> <?php echo $version; ?></p>
                    </div> <!-- end div .vert-block -->
