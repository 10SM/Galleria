<?php
/**
 * This file displays a person, and all files associated with them.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';


/** get the ID for this person */
if (isset($_GET["personid"])) {
    $get_id = $_GET["personid"];
} else {
    $get_id = "";
}

/** If we have an ID, lets get the data from the 'person' table */
if ($get_id != '') {

    /** let's create the query */
    $personpageq = "SELECT * FROM person WHERE person_id=".$get_id."";
    $personpagequery = mysqli_query($dbconn,$personpageq);

    while ($personpageopt = mysqli_fetch_assoc($personpagequery)) {
        $personpageid       = $personpageopt['person_id'];
        $personpagename     = $personpageopt['person_name'];
        $personpagealiases  = $personpageopt['person_aliases'];
        $personpagedob      = $personpageopt['person_birth_date'];
        $personpagepob      = $personpageopt['person_birth_place'];
        $personpagedod      = $personpageopt['person_death_date'];
        $personpagepod      = $personpageopt['person_death_place'];
        $personpagecod      = $personpageopt['person_death_cause'];
        $personpagedesc     = $personpageopt['person_description'];
        $personpagenato     = $personpageopt['person_nationalities'];
        $personpageurl      = $personpageopt['person_url'];
        $personpageemail    = $personpageopt['person_email'];
        $personpagethumb    = $personpageopt['person_thumbnail'];
        $personpagealtthumb = $personpageopt['person_alternate_thumbnail'];
        $personpagehero     = $personpageopt['person_hero_image'];
        $personpagetags     = $personpageopt['person_tags'];
        $personpagecats     = $personpageopt['person_categories'];
    }
}
/** Does this person have a thumbnail, alt thumbnail, or hero image? */
if ($personpagethumb !== '') {
    /** Let's get the main thumbnail for this person */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$personpagethumb."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
} else if ($personpagealtthumb !== '') {
    /** Let's get the alternate thumbnail for this person */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$personpagealtthumb."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
} else if ($personpagehero !== '') {
    /** Let's get the main thumbnail for this person */
    $getthumbq = "SELECT * FROM image WHERE image_id='".$personpagehero."'";
    $getthumbquery = mysqli_query($dbconn,$getthumbq);
    while ($getthumbopt = mysqli_fetch_assoc($getthumbquery)) {
        $thumbid        = $getthumbopt['image_id'];
        $thumbname      = $getthumbopt['image_name'];
        $thumbpath      = $getthumbopt['image_path'];
        $thumbdesc      = $getthumbopt['image_description'];
    }
}

$page_name = $personpagename;
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START PERSON.PHP -->
        <main>
            <!-- <h1 class="hidden"><?php echo $personpagename; ?></h1> -->
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
                    <div class="vert-block">                <!-- picture-of-the-day: a function randomly chooses a picture to display each day -->
                        <figure class="potd">

<?php
/** if $thumbid is empty, show generic person image */
if ($thumbid !== NULL or "") {
    echo "\t\t\t\t\t\t\t<img src=\"thumb.php?imageid=".$thumbid."\" alt=\"".$thumbdesc."\" title=\"".$thumbname."\">\n";
    echo "\t\t\t\t\t\t\t<figcaption>".$thumbname."</figcaption>\n";
} else {
    echo "\t\t\t\t\t\t\t<img src=\"includes/generic-person.png\" alt=\"".$personpagename."\" title=\"".$personpagename."\">\n";
    echo "\t\t\t\t\t\t\t<figcaption>".$personpagename."</figcaption>\n";
}
?>
                        </figure>
                    </div> <!-- end div .vert-block -->
                    <div class="vert-block">                <!-- statistics showing numbers of images, videos, audios, texts, people, companies, awards, etc -->
                        <h2 class="block-title">Demographics</h2>
                        <table>

<?php
                if ($personpagedob !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Date of birth</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$personpagedob."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpagepob !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Place of birth</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$personpagepob."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpagedod !== "0000-00-00") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Date of death</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$personpagedod."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpagepod !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Place of death</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$personpagepod."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpagecod !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Cause of death</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td>".$personpagecod."</td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpageurl !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Website</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td class=\"long-text\"><a href=\"".$personpageurl."\">".$personpageurl."</a></td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }

                if ($personpageemail !== "") {
                    echo "\t\t\t\t\t\t\t<tr>\n";
                    echo "\t\t\t\t\t\t\t\t<td><b>Email</b></td>\n";
                    echo "\t\t\t\t\t\t\t\t<td><a href=\"mailto:".$personpageemail."\"".$personpageemail."</a></td>\n";
                    echo "\t\t\t\t\t\t\t</tr>\n";
                }
?>
                        </table>
                        <p><a href="https://gottahavacuppamocha.com">Galleria</a> <?php echo $version; ?></p>
                    </div> <!-- end div .vert-block -->
                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="horiz-block">
                    <h1><?php echo $personpagename; ?></h1>
<?php
                if ($personpagedesc !== "") {
                    echo "\t\t\t\t\t\t<div class=\"person-bio\">".$personpagedesc."</div>\n";
                } else {
                    echo "\t\t\t\t\t\t<p>We do not have a biography or description for this person.</p>\n";
                }
?>
                    </div>

<?php
/**
 * Find out if there are any recently added images for this person in the DB
 */
$persimgq = "SELECT * FROM image WHERE image_people='".$personpageid."'";
$persimgquery = mysqli_query($dbconn,$persimgq);
if (mysqli_num_rows($persimgquery) > 0) {
    echo "\t\t\t\t\t<div class=\"horiz-block\">\n";
    echo "\t\t\t\t\t\t<h2 class=\"block-title\">Recent images</h2>\n";
    /** get the image information */
    while ($persimgopt = mysqli_fetch_assoc($persimgquery)) {
        $persimgid      = $persimgopt['image_id'];
        $persimgname    = $persimgopt['image_name'];
        $persimgdesc    = $persimgopt['image_description'];
        $persimgpath    = $persimgopt['image_path'];

        echo "\t\t\t\t\t\t<div class=\"horiz-block-column\">\n";
        if ($persimgpath !== '') {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$persimgid."\" title=\"".$persimgdesc."\"><img src=\"thumb.php?imageid=".$persimgid."\" class=\"horiz-block-img\"></a>\n";
        } else {
            echo "\t\t\t\t\t\t\t<a href=\"image.php?imageid=".$persimgid."\" title=\"".$persimgdesc."\"><img src=\"includes/generic-image.png\" class=\"horiz-block-img\"></a>\n";
        }
        echo "\t\t\t\t\t\t</div>\n";
    }
    echo "\t\t\t\t\t</div>\n";
}
?>
<!-- There will be other sections here in the future -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END PERSON.PHP -->
<?php require 'footer.php'; ?>
