-- MySQL dump 10.19  Distrib 10.3.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: galleria
-- ------------------------------------------------------
-- Server version	10.3.34-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio`
--

DROP TABLE IF EXISTS `audio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio` (
  `audio_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio_type` int(11) DEFAULT NULL COMMENT 'audio_type_id, can only be one',
  `audio_name` varchar(100) NOT NULL,
  `audio_path` varchar(255) NOT NULL COMMENT 'relative path',
  `audio_description` text DEFAULT NULL,
  `audio_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `audio_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `audio_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `audio_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `audio_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`audio_id`),
  UNIQUE KEY `audios_audio_path_IDX` (`audio_path`) USING BTREE,
  KEY `audios_audio_added_timestamp_IDX` (`audio_added_timestamp`) USING BTREE,
  KEY `audios_audio_type_IDX` (`audio_type`) USING BTREE,
  KEY `audios_audio_name_IDX` (`audio_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio`
--

LOCK TABLES `audio` WRITE;
/*!40000 ALTER TABLE `audio` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_collection`
--

DROP TABLE IF EXISTS `audio_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_collection` (
  `audio_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio_collection_type` int(11) DEFAULT NULL COMMENT 'audio_collection_type_id, can only be one',
  `audio_collection_name` varchar(100) NOT NULL,
  `audio_collection_files` text NOT NULL COMMENT 'audio_id, must be at least one, comma-separated',
  `audio_collection_description` text DEFAULT NULL,
  `audio_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `audio_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `audio_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `audio_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `audio_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `audio_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`audio_collection_id`),
  KEY `audi_collections_audio_collection_added_timestamp_IDX` (`audio_collection_added_timestamp`) USING BTREE,
  KEY `audi_collections_audio_collection_type_IDX` (`audio_collection_type`) USING BTREE,
  KEY `audi_collections_audio_collection_name_IDX` (`audio_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_collection`
--

LOCK TABLES `audio_collection` WRITE;
/*!40000 ALTER TABLE `audio_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_collection_type`
--

DROP TABLE IF EXISTS `audio_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_collection_type` (
  `audio_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio_collection_type_id`),
  FULLTEXT KEY `audio_collection_types_audio_coll_type_name_IDX` (`audio_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Audio collection types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_collection_type`
--

LOCK TABLES `audio_collection_type` WRITE;
/*!40000 ALTER TABLE `audio_collection_type` DISABLE KEYS */;
INSERT INTO `audio_collection_type` VALUES (1,'ALBUM'),(2,'AUDIO DRAMA'),(3,'AUDIOBOOK'),(4,'PODCAST');
/*!40000 ALTER TABLE `audio_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_type`
--

DROP TABLE IF EXISTS `audio_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_type` (
  `audio_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio_type_id`),
  FULLTEXT KEY `audio_types_audio_type_name_IDX` (`audio_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='Audio file types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_type`
--

LOCK TABLES `audio_type` WRITE;
/*!40000 ALTER TABLE `audio_type` DISABLE KEYS */;
INSERT INTO `audio_type` VALUES (1,'CHAPTER'),(2,'EPISODE'),(3,'SAMPLE'),(4,'SKIT'),(5,'SONG'),(6,'ADVERTISEMENT');
/*!40000 ALTER TABLE `audio_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `award`
--

DROP TABLE IF EXISTS `award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award` (
  `award_id` int(11) NOT NULL AUTO_INCREMENT,
  `award_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award_type` tinyint(1) DEFAULT NULL COMMENT 'competitive/non-competitive',
  `award_name` varchar(100) NOT NULL,
  `award_description` text DEFAULT NULL,
  `award_winner` varchar(100) NOT NULL COMMENT 'person_id, can be more than one',
  `award_nominees` varchar(100) DEFAULT NULL COMMENT 'person_id, can be more than one',
  `award_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `award_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`award_id`),
  UNIQUE KEY `awards_award_name_IDX` (`award_name`) USING BTREE,
  KEY `awards_award_added_timestamp_IDX` (`award_added_timestamp`) USING BTREE,
  KEY `awards_award_type_IDX` (`award_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award`
--

LOCK TABLES `award` WRITE;
/*!40000 ALTER TABLE `award` DISABLE KEYS */;
/*!40000 ALTER TABLE `award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `award_collection`
--

DROP TABLE IF EXISTS `award_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award_collection` (
  `award_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `award_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award_collection_formal_name` varchar(100) NOT NULL COMMENT 'e.g. The Academy Awards',
  `award_collection_nickname` varchar(100) DEFAULT NULL COMMENT 'e.g. The Oscars',
  `award_collection_description` text DEFAULT NULL,
  `award_collection_start_year` date DEFAULT NULL,
  `award_collection_end_year` date DEFAULT NULL,
  `award_collection_date` date DEFAULT NULL COMMENT 'the date of award ceremony',
  `award_collection_number` varchar(10) DEFAULT NULL COMMENT 'e.g. 8th, 50th, etc',
  `award_collection_frequency` varchar(100) DEFAULT NULL COMMENT 'not all awards are given annually',
  `award_collection_organization` int(11) NOT NULL COMMENT 'organization_id, must be included',
  `award_collection_parent` int(11) DEFAULT NULL COMMENT 'award_collection_id, can only be one',
  `award_collection_awards` text NOT NULL COMMENT 'award_id, comma separated, can be more than one',
  `award_collection_tags` text DEFAULT NULL,
  `award_collection_categories` text DEFAULT NULL,
  `award_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id',
  `award_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id',
  PRIMARY KEY (`award_collection_id`),
  UNIQUE KEY `awards_collections_award_collection_formal_name_IDX` (`award_collection_formal_name`) USING BTREE,
  UNIQUE KEY `awards_collections_award_collection_nickname_IDX` (`award_collection_nickname`) USING BTREE,
  KEY `awards_collections_award_collection_added_timestamp_IDX` (`award_collection_added_timestamp`) USING BTREE,
  KEY `awards_collections_award_collection_start_year_IDX` (`award_collection_start_year`) USING BTREE,
  KEY `awards_collections_award_collection_end_year_IDX` (`award_collection_end_year`) USING BTREE,
  KEY `awards_collections_award_collection_date_IDX` (`award_collection_date`) USING BTREE,
  KEY `awards_collections_award_collection_number_IDX` (`award_collection_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Mainly for awards ceremonies';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award_collection`
--

LOCK TABLES `award_collection` WRITE;
/*!40000 ALTER TABLE `award_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `award_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `category_name` varchar(100) NOT NULL,
  `category_slug` varchar(100) NOT NULL,
  `category_parent` int(11) DEFAULT NULL COMMENT 'category_id, can only be one',
  `category_description` text DEFAULT NULL,
  `category_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `category_color` varchar(6) DEFAULT NULL COMMENT 'RGB hex code',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `categories_category_name_IDX` (`category_name`) USING BTREE,
  UNIQUE KEY `categories_category_slug_IDX` (`category_slug`) USING BTREE,
  KEY `categories_category_added_timestamp_IDX` (`category_added_timestamp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image_type` int(11) DEFAULT NULL COMMENT 'image_type_id, can only be one',
  `image_name` varchar(100) NOT NULL,
  `image_path` varchar(255) NOT NULL COMMENT 'relative path',
  `image_description` text DEFAULT NULL,
  `image_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `image_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `image_date` date DEFAULT NULL,
  `image_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `image_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `images_image_path_IDX` (`image_path`) USING BTREE,
  KEY `images_image_added_timestamp_IDX` (`image_added_timestamp`) USING BTREE,
  KEY `images_image_type_IDX` (`image_type`) USING BTREE,
  KEY `images_image_name_IDX` (`image_name`) USING BTREE,
  KEY `images_image_date_IDX` (`image_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_collection`
--

DROP TABLE IF EXISTS `image_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_collection` (
  `image_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image_collection_type` int(11) DEFAULT NULL COMMENT 'image_collection_type_id, can only be one',
  `image_collection_name` varchar(100) NOT NULL,
  `image_collection_files` text NOT NULL COMMENT 'image_id, must be at least one, comma-separated',
  `image_collection_description` text DEFAULT NULL,
  `image_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `image_collection_organizations` varchar(300) DEFAULT NULL,
  `image_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `image_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `image_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`image_collection_id`),
  KEY `image_collections_image_collection_added_timestamp_IDX` (`image_collection_added_timestamp`) USING BTREE,
  KEY `image_collections_image_collection_type_IDX` (`image_collection_type`) USING BTREE,
  KEY `image_collections_image_collection_name_IDX` (`image_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_collection`
--

LOCK TABLES `image_collection` WRITE;
/*!40000 ALTER TABLE `image_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `image_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_collection_type`
--

DROP TABLE IF EXISTS `image_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_collection_type` (
  `image_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`image_collection_type_id`),
  UNIQUE KEY `image_collection_types_image_coll_type_name_IDX` (`image_collection_type_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='Types of image collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_collection_type`
--

LOCK TABLES `image_collection_type` WRITE;
/*!40000 ALTER TABLE `image_collection_type` DISABLE KEYS */;
INSERT INTO `image_collection_type` VALUES (2,'ATLAS'),(1,'GALLERY'),(3,'PORTFOLIO');
/*!40000 ALTER TABLE `image_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_type`
--

DROP TABLE IF EXISTS `image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_type` (
  `image_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`image_type_id`),
  FULLTEXT KEY `image_types_image_type_name_IDX` (`image_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='Types of image files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_type`
--

LOCK TABLES `image_type` WRITE;
/*!40000 ALTER TABLE `image_type` DISABLE KEYS */;
INSERT INTO `image_type` VALUES (1,'CHART'),(2,'ILLUSTRATION'),(3,'INFOGRAPHIC'),(4,'LOGO'),(5,'MAP'),(6,'PAINTING'),(7,'PHOTOGRAPH'),(8,'SKETCH'),(9,'POSTER'),(10,'ADVERTISEMENT');
/*!40000 ALTER TABLE `image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `organization_name` varchar(100) NOT NULL,
  `organization_aliases` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `organization_start_date` date DEFAULT NULL,
  `organization_end_date` date DEFAULT NULL,
  `organization_description` text DEFAULT NULL,
  `organization_place` varchar(100) DEFAULT NULL COMMENT 'city, state, country',
  `organization_url` varchar(255) DEFAULT NULL COMMENT 'Offical website or social media',
  `organization_email` varchar(255) DEFAULT NULL COMMENT 'Official emal address',
  `organization_logo` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `organization_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one.',
  `organization_hero_image` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `organization_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `organization_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  PRIMARY KEY (`organization_id`),
  UNIQUE KEY `organizations_organization_name_IDX` (`organization_name`) USING BTREE,
  KEY `organizations_organization_added_timestamp_IDX` (`organization_added_timestamp`) USING BTREE,
  KEY `organizations_organization_start_date_IDX` (`organization_start_date`) USING BTREE,
  KEY `organizations_organization_end_date_IDX` (`organization_end_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `person_name` varchar(100) NOT NULL,
  `person_aliases` varchar(100) DEFAULT NULL,
  `person_birth_date` date DEFAULT NULL,
  `person_birth_place` varchar(100) DEFAULT NULL,
  `person_death_date` date DEFAULT NULL,
  `person_death_place` varchar(100) DEFAULT NULL,
  `person_death_cause` varchar(100) DEFAULT NULL,
  `person_description` text DEFAULT NULL,
  `person_nationalities` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `person_url` varchar(255) DEFAULT NULL,
  `person_email` varchar(255) DEFAULT NULL COMMENT 'professional email address',
  `person_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_hero_image` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_tags` varchar(300) DEFAULT NULL COMMENT 'can be more than one, tag-id, comma-separated',
  `person_categories` varchar(300) DEFAULT NULL COMMENT 'can be more than one, category-id, comma-separated',
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `people_person_name_IDX` (`person_name`) USING BTREE,
  KEY `people_person_added_timestamp_IDX` (`person_added_timestamp`) USING BTREE,
  KEY `people_person_birth_date_IDX` (`person_birth_date`) USING BTREE,
  KEY `people_person_death_date_IDX` (`person_death_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tag_name` varchar(100) NOT NULL,
  `tag_slug` varchar(100) NOT NULL,
  `tag_description` text DEFAULT NULL,
  `tag_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `tag_color` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tags_tag_name_IDX` (`tag_name`) USING BTREE,
  UNIQUE KEY `tags_tag_slug_IDX` (`tag_slug`) USING BTREE,
  KEY `tags_tag_added_timestamp_IDX` (`tag_added_timestamp`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text`
--

DROP TABLE IF EXISTS `text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text` (
  `text_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text_type` int(11) DEFAULT NULL COMMENT 'text-type-id, can only be one',
  `text_name` varchar(100) NOT NULL,
  `text_path` varchar(255) NOT NULL COMMENT 'relative path',
  `text_description` text DEFAULT NULL,
  `text_people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `text_organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `text_tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `text_categories` varchar(300) DEFAULT NULL COMMENT 'category-id can be more than one, comma-separated',
  `text_thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `text_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`text_id`),
  UNIQUE KEY `texts_text_path_IDX` (`text_path`) USING BTREE,
  KEY `texts_text_added_timestamp_IDX` (`text_added_timestamp`) USING BTREE,
  KEY `texts_text_type_IDX` (`text_type`) USING BTREE,
  KEY `texts_text_name_IDX` (`text_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text`
--

LOCK TABLES `text` WRITE;
/*!40000 ALTER TABLE `text` DISABLE KEYS */;
/*!40000 ALTER TABLE `text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_collection`
--

DROP TABLE IF EXISTS `text_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_collection` (
  `text_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text_collection_type` int(11) DEFAULT NULL COMMENT 'text_collection_type_id, can only be one',
  `text_collection_name` varchar(100) NOT NULL,
  `text_collection_files` text NOT NULL COMMENT 'text_id, must be at least one, comma-separated',
  `text_collection_description` text DEFAULT NULL,
  `text_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `text_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `text_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `text_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `text_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `text_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`text_collection_id`),
  KEY `text_collections_text_collection_added_timestamp_IDX` (`text_collection_added_timestamp`) USING BTREE,
  KEY `text_collections_text_collection_type_IDX` (`text_collection_type`) USING BTREE,
  KEY `text_collections_text_collection_name_IDX` (`text_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_collection`
--

LOCK TABLES `text_collection` WRITE;
/*!40000 ALTER TABLE `text_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `text_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_collection_type`
--

DROP TABLE IF EXISTS `text_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_collection_type` (
  `text_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`text_collection_type_id`),
  FULLTEXT KEY `text_collection_types_text_coll_type_name_IDX` (`text_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='Types of text collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_collection_type`
--

LOCK TABLES `text_collection_type` WRITE;
/*!40000 ALTER TABLE `text_collection_type` DISABLE KEYS */;
INSERT INTO `text_collection_type` VALUES (1,'BOOK'),(2,'MAGAZINE'),(3,'JOURNAL'),(4,'NEWSPAPER'),(5,'BLOG'),(6,'FORUM'),(7,'PORTFOLIO');
/*!40000 ALTER TABLE `text_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_type`
--

DROP TABLE IF EXISTS `text_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_type` (
  `text_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`text_type_id`),
  FULLTEXT KEY `text_types_text_type_name_IDX` (`text_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='Types of text files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_type`
--

LOCK TABLES `text_type` WRITE;
/*!40000 ALTER TABLE `text_type` DISABLE KEYS */;
INSERT INTO `text_type` VALUES (1,'BOOK CHAPTER'),(2,'JOURNAL ARTICLE'),(3,'MAGAZINE ARTICLE'),(4,'NEWSPAPER ARTICLE'),(5,'TRANSCRIPT'),(6,'THESIS'),(7,'DISSERTATION'),(8,'ESSAY'),(9,'BLOG POST'),(10,'BLOG PAGE'),(11,'FORUM POST'),(12,'COMMENT'),(13,'CORRESPONDENCE'),(14,'ADVERTISEMENT');
/*!40000 ALTER TABLE `text_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video_type` int(11) DEFAULT NULL COMMENT 'video-type-id, can only be one',
  `video_name` varchar(100) NOT NULL,
  `video_path` varchar(255) NOT NULL COMMENT 'relative path',
  `video_description` text DEFAULT NULL,
  `video_people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `video_organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `video_tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `video_categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `video_poster` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `video_fanart` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`video_id`),
  UNIQUE KEY `videos_video_path_IDX` (`video_path`) USING BTREE,
  KEY `videos_video_added_timestamp_IDX` (`video_added_timestamp`) USING BTREE,
  KEY `videos_video_type_IDX` (`video_type`) USING BTREE,
  KEY `videos_video_name_IDX` (`video_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_collection`
--

DROP TABLE IF EXISTS `video_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_collection` (
  `video_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video_collection_type` int(11) DEFAULT NULL COMMENT 'video_collection_type_id, can only be one',
  `video_collection_name` varchar(100) NOT NULL,
  `video_collection_files` text NOT NULL,
  `video_collection_description` text DEFAULT NULL,
  `video_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `video_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `video_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `video_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `video_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `video_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`video_collection_id`),
  KEY `video_collections_video_collection_added_timestamp_IDX` (`video_collection_added_timestamp`) USING BTREE,
  KEY `video_collections_video_collection_type_IDX` (`video_collection_type`) USING BTREE,
  KEY `video_collections_video_collection_name_IDX` (`video_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_collection`
--

LOCK TABLES `video_collection` WRITE;
/*!40000 ALTER TABLE `video_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_collection_type`
--

DROP TABLE IF EXISTS `video_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_collection_type` (
  `video_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`video_collection_type_id`),
  FULLTEXT KEY `video_collection_types_video_coll_type_name_IDX` (`video_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Types of video collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_collection_type`
--

LOCK TABLES `video_collection_type` WRITE;
/*!40000 ALTER TABLE `video_collection_type` DISABLE KEYS */;
INSERT INTO `video_collection_type` VALUES (1,'FILM'),(2,'FILM SERIES'),(3,'TELEVISION SERIES'),(4,'PORTFOLIO');
/*!40000 ALTER TABLE `video_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_type`
--

DROP TABLE IF EXISTS `video_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_type` (
  `video_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`video_type_id`),
  FULLTEXT KEY `video_types_video_type_name_IDX` (`video_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='Types of video files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_type`
--

LOCK TABLES `video_type` WRITE;
/*!40000 ALTER TABLE `video_type` DISABLE KEYS */;
INSERT INTO `video_type` VALUES (1,'SCENE'),(2,'FILM'),(3,'EPISODE'),(4,'ADVERTISEMENT'),(5,'MUSIC VIDEO'),(6,'SPECIAL FEATURE');
/*!40000 ALTER TABLE `video_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-06 13:28:32
