ALTER TABLE `audio` ADD COLUMN IF NOT EXISTS `audio_duration` varchar(16) DEFAULT NULL COMMENT 'How long will the file play?';
ALTER TABLE `audio` ADD COLUMN IF NOT EXISTS `audio_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes';
ALTER TABLE `audio` ADD COLUMN IF NOT EXISTS `audio_file_type` varchar(100) DEFAULT NULL COMMENT 'The type of audio file (e.g. mp3, ogg, etc)';
ALTER TABLE `audio` ADD COLUMN IF NOT EXISTS `audio_rating` varchar(100) DEFAULT NULL COMMENT 'How nice is the file on a scale of 1 to 5?';
ALTER TABLE `audio` ADD COLUMN IF NOT EXISTS `audio_warning` tinyint(1) DEFAULT NULL COMMENT 'Does the file have explicit language?';

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL COMMENT 'The name of the country in English',
  `country_demonym` varchar(100) NOT NULL COMMENT 'The term for a person from this country',
  `country_exists` tinyint(1) NOT NULL COMMENT 'Does the country still exist or not?',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_country_name_IDX` (`country_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Places where people live and die';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','Afghani',1),(2,'Albania','Albanian',1),(3,'Algeria','Algerian',1),(4,'Andorra','Andorran',1),(5,'Angola','Angolan',1),(6,'Antigua and Barbuda','Antiguan and Barbudan',1),(7,'Argentina','Argentinian',1),(8,'Armenia','Armenian',1),(9,'Australia','Australian',1),(10,'Austria','Austrian',1),(11,'Azerbaijan','Azerbaijani',1),(12,'Bahamas','Bahamian',1),(13,'Bahrain','Bahraini',1),(14,'Bangladesh','Bangladeshi',1),(15,'Barbados','Barbadian',1),(16,'Belarus','Belarusian',1),(17,'Belgium','Belgian',1),(18,'Belize','Belizean',1),(19,'Benin','Beninese',1),(20,'Bhutan','Bhutanese',1),(21,'Bolivia','Bolivian',1),(22,'Bosnia and Herzegovina','Bosnian and Herzegovinian',1),(23,'Botswana','Motswana',1),(24,'Brazil','Brazilian',1),(25,'Brunei','Bruneian',1),(26,'Bulgaria','Bulgarian',1),(27,'Burkina Faso','Burkinabe',1),(28,'Burundi','Burundian',1),(29,'Cambodia','Cambodian',1),(30,'Cameroon','Cameroonian',1),(31,'Canada','Canadian',1),(32,'Cape Verde','Cape Verdean',1),(33,'Central African Republic','Central African',1),(34,'Chad','Chadian',1),(35,'Chile','Chilean',1),(36,'China','Chinese',1),(37,'Colombia','Colombian',1),(38,'Comoros','Comoran',1),(39,'Republic of the Congo','Congolese',1),(40,'Democratic Republic of the Congo','Congolese',1),(41,'Costa Rica','Costa Rican',1),(42,'Cote d\'Ivoire','Ivorian',1),(43,'Croatia','Croatian',1),(44,'Cuba','Cuban',1),(45,'Cyprus','Cypriot',1),(46,'Czechia','Czech',1),(47,'Denmark','Danish',1),(48,'Djibouti','Djibouti',1),(49,'Dominica','Dominican',1),(50,'Dominican Republic','Dominican',1),(51,'East Timor','East Timorese',1),(52,'Ecuador','Ecuadorean',1),(53,'Egypt','Egyptian',1),(54,'El Salvador','Salvadoran',1),(55,'Equatorial Guinea','Equatoguinean',1),(56,'Eritrea','Eritrean',1),(57,'Estonia','Estonian',1),(58,'Ethiopia','Ethiopian',1),(59,'Fiji','Fijian',1),(60,'Finland','Finnish',1),(61,'France','French',1),(62,'Gabon','Gabonese',1),(63,'Gambia','Gambian',1),(64,'Georgia','Georgian',1),(65,'Germany','German',1),(66,'Ghana','Ghanian',1),(67,'Greece','Greek',1),(68,'Grenada','Grenadan',1),(69,'Guatemala','Guatemalan',1),(70,'Guinea','Guinean',1),(71,'Guinea-Bissau','Guinea-Bissauan',1),(72,'Guyana','Guyanese',1),(73,'Haiti','Hiatian',1),(74,'Honduras','Honduran',1),(75,'Hungary','Hungarian',1),(76,'Iceland','Icelandic',1),(77,'India','Indian',1),(78,'Indonesia','Indonesian',1),(79,'Iran','Iranian',1),(80,'Iraq','Iraqi',1),(81,'Ireland','Irish',1),(82,'Israel','Israeli',1),(83,'Italy','Italian',1),(84,'Jamaica','Jamaican',1),(85,'Japan','Japanese',1),(86,'Jordan','Jordanian',1),(87,'Kazakhstan','Kazakh',1),(88,'Kenya','Kenyan',1),(89,'Kiribati','I-Kiribati',1),(90,'North Korea','North Korean',1),(91,'South Korea','South Korean',1),(92,'Kosovo','Kosovar',1),(93,'Kuwait','Kuwaiti',1),(94,'Kyrgyzstan','Kyrgyz',1),(95,'Laos','Laotian',1),(96,'Latvia','Latvian',1),(97,'Lebanon','Lebanese',1),(98,'Lesotho','Mosotho',1),(99,'Liberia','Liberian',1),(100,'Libya','Libyan',1),(101,'Liechtenstein','Liechtensteiner',1),(102,'Lithuania','Lithuanian',1),(103,'Luxembourg','Luxembourger',1),(104,'Macedonia','Macedonian',1),(105,'Madagascar','Malagasy',1),(106,'Malawi','Malawian',1),(107,'Malaysia','Malaysian',1),(108,'Maldives','Maldivian',1),(109,'Mali','Malian',1),(110,'Malta','Maltese',1),(111,'Marshall Islands','Marshallese',1),(112,'Mauritania','Mauritanian',1),(113,'Mauritius','Mauritian',1),(114,'Mexico','Mexican',1),(115,'Federated States of Micronesia','Micronesian',1),(116,'Moldova','Moldovan',1),(117,'Monaco','Monagasque',1),(118,'Mongolia','Mongolian',1),(119,'Montenegro','Montenegrin',1),(120,'Morocco','Moroccan',1),(121,'Mozambique','Mozambican',1),(122,'Myanmar','Burmese',1),(123,'Namibia','Namibian',1),(124,'Nauru','Nauruan',1),(125,'Nepal','Nepalese',1),(126,'Netherlands','Dutch',1),(127,'New Zealand','New Zealander',1),(128,'Nicaragua','Nicaraguan',1),(129,'Niger','Nigerien',1),(130,'Nigeria','Nigerian',1),(131,'Norway','Norwegian',1),(132,'Oman','Omani',1),(133,'Pakistan','Pakistani',1),(134,'Palau','Palauan',1),(135,'Panama','Panamanian',1),(136,'Papua New Guinea','Papua New Guinean',1),(137,'Paraguay','Paraguayan',1),(138,'Peru','Peruvian',1),(139,'Philippines','Filipino',1),(140,'Poland','Polish',1),(141,'Portugal','Portuguese',1),(142,'Qatar','Qatari',1),(143,'Romania','Romanian',1),(144,'Russia','Russian',1),(145,'Rwanda','Rwandan',1),(146,'Saint Kitts and Nevis','Kittian',1),(147,'Saint Lucia','Saint Lucian',1),(148,'Samoa','Samoan',1),(149,'San Marino','Sammarinese',1),(150,'Sao Tome and Principe','Sao Tomean',1),(151,'Saudi Arabia','Saudi Arabian',1),(152,'Senegal','Senegalese',1),(153,'Serbia','Serbian',1),(154,'Seychelles','Seychellois',1),(155,'Sierra Leone','Sierra Leonean',1),(156,'Singapore','Singaporean',1),(157,'Slovakia','Slovakian',1),(158,'Slovenia','Slovenian',1),(159,'Solomon Islands','Solomon Islander',1),(160,'Somalia','Somali',1),(161,'South Africa','South African',1),(162,'South Sudan','South Sudanese',1),(163,'Spain','Spanish',1),(164,'Sri Lanka','Sri Lankan',1),(165,'Sudan','Sudanese',1),(166,'Suriname','Surinamer',1),(167,'Eswatini','Liswati',1),(168,'Sweden','Swedish',1),(169,'Switzerland','Swiss',1),(170,'Syria','Syrian',1),(171,'Taiwan','Taiwanese',1),(172,'Tajikistan','Tajik',1),(173,'Tanzania','Tanzanian',1),(174,'Thailand','Thai',1),(175,'Togo','Togolese',1),(176,'Tonga','Tongan',1),(177,'Trinidad and Tobago','Trinidadian and Tobagonian',1),(178,'Tunisia','Tunisian',1),(179,'Türkiye','Turkish',1),(180,'Turkmenistan','Turkmen',1),(181,'Tuvalu','Tuvaluan',1),(182,'Uganda','Ugandan',1),(183,'Ukraine','Ukrainian',1),(184,'United Arab Emirates','Emirati',1),(185,'United Kingdom','British',1),(186,'United States','American',1),(187,'Uruguay','Uruguayan',1),(188,'Uzbekistan','Uzbek',1),(189,'Vanuatu','Ni-Vanuatu',1),(190,'Venezuela','Venzuelan',1),(191,'Vietnam','Vietnamese',1),(192,'Yemen','Yemeni',1),(193,'Zambia','Zambian',1),(194,'Zimbabwe','Zimbabwean',1);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_width` int(11) DEFAULT NULL;
ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_height` int(11) DEFAULT NULL;
ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_size` int(11) DEFAULT NULL COMMENT 'image size in kilobytes';
ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_file_type` varchar(100) DEFAULT NULL;
ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_rating` int(11) DEFAULT NULL COMMENT 'On a scale of one to five, how nice is it?';
ALTER TABLE `image` ADD COLUMN IF NOT EXISTS `image_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the image safe for people to view at work?';


ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_rating` int(11) DEFAULT NULL COMMENT 'The rating on a scale of 1 to 5';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_wikipedia` varchar(255) DEFAULT NULL COMMENT 'If there is a wikipedia page for the organization, it should be listed here';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_imdb` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s IMDB page';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_mastodon` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Mastodon account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_pixelfed` varchar(255) DEFAULT NULL COMMENT 'URL of the organization''s official PixelFed account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_youtube` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Youtube account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_instagram` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Instagram account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_tiktok` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official TikTok account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_twitter` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Twitter account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_facebook` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Facebook account';
ALTER TABLE `organization` ADD COLUMN IF NOT EXISTS `organization_goodreads` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official GoodReads account.';

ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_sort_name` varchar(100) NOT NULL AFTER COLUMN `person_name`;
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_pronouns` varchar(2) DEFAULT NULL AFTER COLUMN `person_aliases`;
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_rating` int(11) DEFAULT NULL COMMENT 'The rating on a scale of 1 to 5';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_wikipedia` varchar(255) DEFAULT NULL COMMENT 'URL for a Wikipedia article about the person';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_imdb` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s IMDB page';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_mastodon` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Mastodon account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_pixelfed` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Pixelfed account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_youtube` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s YouTube channel';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_instagram` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Instagram account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_tiktok` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s TikTok account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_twitter` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Twitter account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_facebook` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Facebook account';
ALTER TABLE `person` ADD COLUMN IF NOT EXISTS `person_goodreads` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s GoodReads author page';

--
-- Table structure for table `pronoun`
--

DROP TABLE IF EXISTS `pronoun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pronoun` (
  `pronoun_id` int(11) NOT NULL AUTO_INCREMENT,
  `pronoun_name` varchar(100) NOT NULL,
  PRIMARY KEY (`pronoun_id`),
  UNIQUE KEY `pronoun_pronoun_id_IDX` (`pronoun_id`) USING BTREE,
  UNIQUE KEY `pronoun_pronoun_name_IDX` (`pronoun_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pronoun`
--

LOCK TABLES `pronoun` WRITE;
/*!40000 ALTER TABLE `pronoun` DISABLE KEYS */;
INSERT INTO `pronoun` VALUES (1,'he/him.his'),(2,'she/her/hers'),(3,'they/them/their');
/*!40000 ALTER TABLE `pronoun` ENABLE KEYS */;
UNLOCK TABLES;

ALTER TABLE `text` ADD COLUMN IF NOT EXISTS `text_pages` int(11) DEFAULT NULL COMMENT 'The approximate number of pages in the document';
ALTER TABLE `text` ADD COLUMN IF NOT EXISTS `text_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes';
ALTER TABLE `text` ADD COLUMN IF NOT EXISTS `text_file_type` varchar(100) DEFAULT NULL COMMENT 'The file format of the document (e.g. pdf, epub, cbr, etc)';
ALTER TABLE `text` ADD COLUMN IF NOT EXISTS `text_rating` int(11) DEFAULT NULL COMMENT 'On a scale of 1 to 5, how good is the text?';
ALTER TABLE `text` ADD COLUMN IF NOT EXISTS `text_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the text safe for young minds?';

ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_width` int(11) DEFAULT NULL;
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_height` int(11) DEFAULT NULL;
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_duration` varchar(16) DEFAULT NULL;
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes';
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_file_type` varchar(100) DEFAULT NULL COMMENT 'The file extension (e.g. m4v, webm, etc)';
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_rating` int(11) DEFAULT NULL COMMENT 'On a scale of 1 to 5, how enjoyable is the video?';
ALTER TABLE `video` ADD COLUMN IF NOT EXISTS `video_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the video safe for young minds?';
