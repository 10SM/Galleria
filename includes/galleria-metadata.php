<?php
/*
 *
 * General Metadata for and about this particular Galleria installation
 *
 */

// Galleria version number
$version = "v20230614";

// Name of this site
$site_name = "Test Galleria";

// DB connection info
define ("DBNAME", '');
define ("DBHOST", 'localhost'); // don't change this unless you know what you're doing
define ("DBUSER", '');
define ("DBPASS", '');
?>
