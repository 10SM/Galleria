-- MySQL dump 10.19  Distrib 10.3.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: galleria
-- ------------------------------------------------------
-- Server version	10.3.34-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio-collection-types`
--

DROP TABLE IF EXISTS `audio-collection-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio-collection-types` (
  `audio-coll-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `audio-coll-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio-coll-type-id`),
  FULLTEXT KEY `audio_collection_types_audio_coll_type_name_IDX` (`audio-coll-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Audio collection types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio-collection-types`
--

LOCK TABLES `audio-collection-types` WRITE;
/*!40000 ALTER TABLE `audio-collection-types` DISABLE KEYS */;
INSERT INTO `audio-collection-types` VALUES (1,'ALBUM'),(2,'AUDIO DRAMA'),(3,'AUDIOBOOK'),(4,'PODCAST');
/*!40000 ALTER TABLE `audio-collection-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio-collections`
--

DROP TABLE IF EXISTS `audio-collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio-collections` (
  `audio-collection-id` int(11) NOT NULL AUTO_INCREMENT,
  `audio-collection-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio-collection-type` int(11) DEFAULT NULL COMMENT 'audio-collection-type-id, cna only be one',
  `audio-collection-name` varchar(100) NOT NULL,
  `audio-collection-audios` text NOT NULL COMMENT 'audio-id, must be at least one, comma-separated',
  `audio-collection-description` text DEFAULT NULL,
  `audio-collection-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `audio-collection-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `audio-collection-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `audio-collection-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `audio-collection-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `audio-collection-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`audio-collection-id`),
  KEY `audi_collections_audio_collection_added_timestamp_IDX` (`audio-collection-added-timestamp`) USING BTREE,
  KEY `audi_collections_audio_collection_type_IDX` (`audio-collection-type`) USING BTREE,
  KEY `audi_collections_audio_collection_name_IDX` (`audio-collection-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio-collections`
--

LOCK TABLES `audio-collections` WRITE;
/*!40000 ALTER TABLE `audio-collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio-collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio-types`
--

DROP TABLE IF EXISTS `audio-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio-types` (
  `audio-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `audio-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio-type-id`),
  FULLTEXT KEY `audio_types_audio_type_name_IDX` (`audio-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='Audio file types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio-types`
--

LOCK TABLES `audio-types` WRITE;
/*!40000 ALTER TABLE `audio-types` DISABLE KEYS */;
INSERT INTO `audio-types` VALUES (1,'CHAPTER'),(2,'EPISODE'),(3,'SAMPLE'),(4,'SKIT'),(5,'SONG'),(6,'ADVERTISEMENT');
/*!40000 ALTER TABLE `audio-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audios`
--

DROP TABLE IF EXISTS `audios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audios` (
  `audio-id` int(11) NOT NULL AUTO_INCREMENT,
  `audio-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio-type` int(11) DEFAULT NULL COMMENT 'audio-type-id, can only be one',
  `audio-name` varchar(100) NOT NULL,
  `audio-path` varchar(255) NOT NULL COMMENT 'relative path',
  `audio-description` text DEFAULT NULL,
  `audio-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `audio-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `audio-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `audio-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `audio-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`audio-id`),
  UNIQUE KEY `audios_audio_path_IDX` (`audio-path`) USING BTREE,
  KEY `audios_audio_added_timestamp_IDX` (`audio-added-timestamp`) USING BTREE,
  KEY `audios_audio_type_IDX` (`audio-type`) USING BTREE,
  KEY `audios_audio_name_IDX` (`audio-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audios`
--

LOCK TABLES `audios` WRITE;
/*!40000 ALTER TABLE `audios` DISABLE KEYS */;
/*!40000 ALTER TABLE `audios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `awards`
--

DROP TABLE IF EXISTS `awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awards` (
  `award-id` int(11) NOT NULL AUTO_INCREMENT,
  `award-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award-type` tinyint(1) DEFAULT NULL COMMENT 'competitive/non-competitive',
  `award-name` varchar(100) NOT NULL,
  `award-description` text DEFAULT NULL,
  `award-winner` varchar(100) NOT NULL COMMENT 'person-id, can be more than one',
  `award-nominees` varchar(100) DEFAULT NULL COMMENT 'person-id, can be more than one',
  `award-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `award-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`award-id`),
  UNIQUE KEY `awards_award_name_IDX` (`award-name`) USING BTREE,
  KEY `awards_award_added_timestamp_IDX` (`award-added-timestamp`) USING BTREE,
  KEY `awards_award_type_IDX` (`award-type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awards`
--

LOCK TABLES `awards` WRITE;
/*!40000 ALTER TABLE `awards` DISABLE KEYS */;
/*!40000 ALTER TABLE `awards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `awards-collections`
--

DROP TABLE IF EXISTS `awards-collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awards-collections` (
  `award-collection-id` int(11) NOT NULL AUTO_INCREMENT,
  `award-collection-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award-collection-formal-name` varchar(100) NOT NULL COMMENT 'e.g. The Academy Awards',
  `award-collection-nickname` varchar(100) DEFAULT NULL COMMENT 'e.g. The Oscars',
  `award-collection-description` text DEFAULT NULL,
  `award-collection-start-year` date DEFAULT NULL,
  `award-collection-end-year` date DEFAULT NULL,
  `award-collection-date` date DEFAULT NULL COMMENT 'the date of award ceremony',
  `award-collection-number` varchar(10) DEFAULT NULL COMMENT 'e.g. 8th, 50th, etc',
  `award-collection-frequency` varchar(100) DEFAULT NULL COMMENT 'not all awards are given annually',
  `award-collection-organization` int(11) NOT NULL COMMENT 'organization-id, must be included',
  `award-collection-parent` int(11) DEFAULT NULL COMMENT 'award-collection-id, can only be one',
  `award-collection-awards` text NOT NULL COMMENT 'award-id, comma separated, can be more than one',
  `award-collection-tags` text DEFAULT NULL,
  `award-collection-categories` text DEFAULT NULL,
  `award-collection-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id',
  `award-collection-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id',
  PRIMARY KEY (`award-collection-id`),
  UNIQUE KEY `awards_collections_award_collection_formal_name_IDX` (`award-collection-formal-name`) USING BTREE,
  UNIQUE KEY `awards_collections_award_collection_nickname_IDX` (`award-collection-nickname`) USING BTREE,
  KEY `awards_collections_award_collection_added_timestamp_IDX` (`award-collection-added-timestamp`) USING BTREE,
  KEY `awards_collections_award_collection_start_year_IDX` (`award-collection-start-year`) USING BTREE,
  KEY `awards_collections_award_collection_end_year_IDX` (`award-collection-end-year`) USING BTREE,
  KEY `awards_collections_award_collection_date_IDX` (`award-collection-date`) USING BTREE,
  KEY `awards_collections_award_collection_number_IDX` (`award-collection-number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Mainly for awards ceremonies';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awards-collections`
--

LOCK TABLES `awards-collections` WRITE;
/*!40000 ALTER TABLE `awards-collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `awards-collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category-id` int(11) NOT NULL AUTO_INCREMENT,
  `category-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `category-name` varchar(100) NOT NULL,
  `category-slug` varchar(100) NOT NULL,
  `category-parent` int(11) DEFAULT NULL COMMENT 'category-id, can only be one',
  `category-description` text DEFAULT NULL,
  `category-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `category-color` varchar(6) DEFAULT NULL COMMENT 'RGB hex code',
  PRIMARY KEY (`category-id`),
  UNIQUE KEY `categories_category_name_IDX` (`category-name`) USING BTREE,
  UNIQUE KEY `categories_category_slug_IDX` (`category-slug`) USING BTREE,
  KEY `categories_category_added_timestamp_IDX` (`category-added-timestamp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image-collection-types`
--

DROP TABLE IF EXISTS `image-collection-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image-collection-types` (
  `image-coll-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `image-coll-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`image-coll-type-id`),
  UNIQUE KEY `image_collection_types_image_coll_type_name_IDX` (`image-coll-type-name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='Types of image collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image-collection-types`
--

LOCK TABLES `image-collection-types` WRITE;
/*!40000 ALTER TABLE `image-collection-types` DISABLE KEYS */;
INSERT INTO `image-collection-types` VALUES (2,'ATLAS'),(1,'GALLERY'),(3,'PORTFOLIO');
/*!40000 ALTER TABLE `image-collection-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image-collections`
--

DROP TABLE IF EXISTS `image-collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image-collections` (
  `image-collection-id` int(11) NOT NULL AUTO_INCREMENT,
  `image-collection-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image-collection-type` int(11) DEFAULT NULL COMMENT 'image-collection-type-id, can only be one',
  `image-collection-name` varchar(100) NOT NULL,
  `image-collection-images` text NOT NULL COMMENT 'image-id, must be at least one, comma-separated',
  `image-collection-description` text DEFAULT NULL,
  `image-collection-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `image-collection-organizations` varchar(300) DEFAULT NULL,
  `image-collection-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `image-collection-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `image-collection-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`image-collection-id`),
  KEY `image_collections_image_collection_added_timestamp_IDX` (`image-collection-added-timestamp`) USING BTREE,
  KEY `image_collections_image_collection_type_IDX` (`image-collection-type`) USING BTREE,
  KEY `image_collections_image_collection_name_IDX` (`image-collection-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image-collections`
--

LOCK TABLES `image-collections` WRITE;
/*!40000 ALTER TABLE `image-collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `image-collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image-types`
--

DROP TABLE IF EXISTS `image-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image-types` (
  `image-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `image-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`image-type-id`),
  FULLTEXT KEY `image_types_image_type_name_IDX` (`image-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='Types of image files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image-types`
--

LOCK TABLES `image-types` WRITE;
/*!40000 ALTER TABLE `image-types` DISABLE KEYS */;
INSERT INTO `image-types` VALUES (1,'CHART'),(2,'ILLUSTRATION'),(3,'INFOGRAPHIC'),(4,'LOGO'),(5,'MAP'),(6,'PAINTING'),(7,'PHOTOGRAPH'),(8,'SKETCH'),(9,'POSTER'),(10,'ADVERTISEMENT');
/*!40000 ALTER TABLE `image-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `image-id` int(11) NOT NULL AUTO_INCREMENT,
  `image-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image-type` int(11) DEFAULT NULL COMMENT 'image-type-id, can only be one',
  `image-name` varchar(100) NOT NULL,
  `image-path` varchar(255) NOT NULL COMMENT 'relative path',
  `image-description` text DEFAULT NULL,
  `image-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `image-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `image-date` date DEFAULT NULL,
  `image-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `image-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  PRIMARY KEY (`image-id`),
  UNIQUE KEY `images_image_path_IDX` (`image-path`) USING BTREE,
  KEY `images_image_added_timestamp_IDX` (`image-added-timestamp`) USING BTREE,
  KEY `images_image_type_IDX` (`image-type`) USING BTREE,
  KEY `images_image_name_IDX` (`image-name`) USING BTREE,
  KEY `images_image_date_IDX` (`image-date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `organization-id` int(11) NOT NULL AUTO_INCREMENT,
  `organization-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `organization-name` varchar(100) NOT NULL,
  `organization-aliases` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `organization-start-date` date DEFAULT NULL,
  `organization-end-date` date DEFAULT NULL,
  `organization-description` text DEFAULT NULL,
  `organization-place` varchar(100) DEFAULT NULL COMMENT 'city, state, country',
  `organization-url` varchar(255) DEFAULT NULL COMMENT 'Offical website or social media',
  `organization-email` varchar(255) DEFAULT NULL COMMENT 'Official emal address',
  `organization-logo` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `organization-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one.',
  `organization-hero-image` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `organization-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `organization-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  PRIMARY KEY (`organization-id`),
  UNIQUE KEY `organizations_organization_name_IDX` (`organization-name`) USING BTREE,
  KEY `organizations_organization_added_timestamp_IDX` (`organization-added-timestamp`) USING BTREE,
  KEY `organizations_organization_start_date_IDX` (`organization-start-date`) USING BTREE,
  KEY `organizations_organization_end_date_IDX` (`organization-end-date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations`
--

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `person-id` int(11) NOT NULL AUTO_INCREMENT,
  `person-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `person-name` varchar(100) NOT NULL,
  `person-aliases` varchar(100) DEFAULT NULL,
  `person-birth-date` date DEFAULT NULL,
  `person-birth-place` varchar(100) DEFAULT NULL,
  `person-death-date` date DEFAULT NULL,
  `person-death-place` varchar(100) DEFAULT NULL,
  `person-death-cause` varchar(100) DEFAULT NULL,
  `person-description` text DEFAULT NULL,
  `person-nationalities` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `person-url` varchar(255) DEFAULT NULL,
  `person-email` varchar(255) DEFAULT NULL COMMENT 'professional email address',
  `person-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `person-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `person-hero-image` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `person-tags` varchar(300) DEFAULT NULL COMMENT 'can be more than one, tag-id, comma-separated',
  `person-categories` varchar(300) DEFAULT NULL COMMENT 'can be more than one, category-id, comma-separated',
  PRIMARY KEY (`person-id`),
  UNIQUE KEY `people_person_name_IDX` (`person-name`) USING BTREE,
  KEY `people_person_added_timestamp_IDX` (`person-added-timestamp`) USING BTREE,
  KEY `people_person_birth_date_IDX` (`person-birth-date`) USING BTREE,
  KEY `people_person_death_date_IDX` (`person-death-date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people`
--

LOCK TABLES `people` WRITE;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
/*!40000 ALTER TABLE `people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tag-id` int(11) NOT NULL AUTO_INCREMENT,
  `tag-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tag-name` varchar(100) NOT NULL,
  `tag-slug` varchar(100) NOT NULL,
  `tag-description` text DEFAULT NULL,
  `tag-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `tag-color` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`tag-id`),
  UNIQUE KEY `tags_tag_name_IDX` (`tag-name`) USING BTREE,
  UNIQUE KEY `tags_tag_slug_IDX` (`tag-slug`) USING BTREE,
  KEY `tags_tag_added_timestamp_IDX` (`tag-added-timestamp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text-collection-types`
--

DROP TABLE IF EXISTS `text-collection-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text-collection-types` (
  `text-coll-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `text-coll-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`text-coll-type-id`),
  FULLTEXT KEY `text_collection_types_text_coll_type_name_IDX` (`text-coll-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='Types of text collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text-collection-types`
--

LOCK TABLES `text-collection-types` WRITE;
/*!40000 ALTER TABLE `text-collection-types` DISABLE KEYS */;
INSERT INTO `text-collection-types` VALUES (1,'BOOK'),(2,'MAGAZINE'),(3,'JOURNAL'),(4,'NEWSPAPER'),(5,'BLOG'),(6,'FORUM'),(7,'PORTFOLIO');
/*!40000 ALTER TABLE `text-collection-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text-collections`
--

DROP TABLE IF EXISTS `text-collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text-collections` (
  `text-collection-id` int(11) NOT NULL AUTO_INCREMENT,
  `text-collection-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text-collection-type` int(11) DEFAULT NULL COMMENT 'text-collection-type-id, can only be one',
  `text-collection-name` varchar(100) NOT NULL,
  `text-collection-texts` text NOT NULL COMMENT 'text-id, must be at least one, comma-separated',
  `text-collection-description` text DEFAULT NULL,
  `text-collection-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `text-collection-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `text-collection-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `text-collection-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `text-collection-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `text-collection-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`text-collection-id`),
  KEY `text_collections_text_collection_added_timestamp_IDX` (`text-collection-added-timestamp`) USING BTREE,
  KEY `text_collections_text_collection_type_IDX` (`text-collection-type`) USING BTREE,
  KEY `text_collections_text_collection_name_IDX` (`text-collection-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text-collections`
--

LOCK TABLES `text-collections` WRITE;
/*!40000 ALTER TABLE `text-collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `text-collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text-types`
--

DROP TABLE IF EXISTS `text-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text-types` (
  `text-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `text-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`text-type-id`),
  FULLTEXT KEY `text_types_text_type_name_IDX` (`text-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='Types of text files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text-types`
--

LOCK TABLES `text-types` WRITE;
/*!40000 ALTER TABLE `text-types` DISABLE KEYS */;
INSERT INTO `text-types` VALUES (1,'BOOK CHAPTER'),(2,'JOURNAL ARTICLE'),(3,'MAGAZINE ARTICLE'),(4,'NEWSPAPER ARTICLE'),(5,'TRANSCRIPT'),(6,'THESIS'),(7,'DISSERTATION'),(8,'ESSAY'),(9,'BLOG POST'),(10,'BLOG PAGE'),(11,'FORUM POST'),(12,'COMMENT'),(13,'CORRESPONDENCE'),(14,'ADVERTISEMENT');
/*!40000 ALTER TABLE `text-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `texts`
--

DROP TABLE IF EXISTS `texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `texts` (
  `text-id` int(11) NOT NULL AUTO_INCREMENT,
  `text-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text-type` int(11) DEFAULT NULL COMMENT 'text-type-id, can only be one',
  `text-name` varchar(100) NOT NULL,
  `text-path` varchar(255) NOT NULL COMMENT 'relative path',
  `text-description` text DEFAULT NULL,
  `text-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `text-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `text-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `text-categories` varchar(300) DEFAULT NULL COMMENT 'category-id can be more than one, comma-separated',
  `text-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `text-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`text-id`),
  UNIQUE KEY `texts_text_path_IDX` (`text-path`) USING BTREE,
  KEY `texts_text_added_timestamp_IDX` (`text-added-timestamp`) USING BTREE,
  KEY `texts_text_type_IDX` (`text-type`) USING BTREE,
  KEY `texts_text_name_IDX` (`text-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texts`
--

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;
/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video-collection-types`
--

DROP TABLE IF EXISTS `video-collection-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video-collection-types` (
  `video-coll-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `video-coll-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`video-coll-type-id`),
  FULLTEXT KEY `video_collection_types_video_coll_type_name_IDX` (`video-coll-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Types of video collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video-collection-types`
--

LOCK TABLES `video-collection-types` WRITE;
/*!40000 ALTER TABLE `video-collection-types` DISABLE KEYS */;
INSERT INTO `video-collection-types` VALUES (1,'FILM'),(2,'FILM SERIES'),(3,'TELEVISION SERIES'),(4,'PORTFOLIO');
/*!40000 ALTER TABLE `video-collection-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video-collections`
--

DROP TABLE IF EXISTS `video-collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video-collections` (
  `video-collection-id` int(11) NOT NULL AUTO_INCREMENT,
  `video-collection-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video-collection-type` int(11) DEFAULT NULL COMMENT 'video-collection-type-id, can only be one',
  `video-collection-name` varchar(100) NOT NULL,
  `video-collection-videos` text NOT NULL,
  `video-collection-description` text DEFAULT NULL,
  `video-collection-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `video-collection-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `video-collection-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `video-collection-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `video-collection-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `video-collection-alt-thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`video-collection-id`),
  KEY `video_collections_video_collection_added_timestamp_IDX` (`video-collection-added-timestamp`) USING BTREE,
  KEY `video_collections_video_collection_type_IDX` (`video-collection-type`) USING BTREE,
  KEY `video_collections_video_collection_name_IDX` (`video-collection-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video-collections`
--

LOCK TABLES `video-collections` WRITE;
/*!40000 ALTER TABLE `video-collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `video-collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video-types`
--

DROP TABLE IF EXISTS `video-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video-types` (
  `video-type-id` int(11) NOT NULL AUTO_INCREMENT,
  `video-type-name` varchar(100) NOT NULL,
  PRIMARY KEY (`video-type-id`),
  FULLTEXT KEY `video_types_video_type_name_IDX` (`video-type-name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='Types of video files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video-types`
--

LOCK TABLES `video-types` WRITE;
/*!40000 ALTER TABLE `video-types` DISABLE KEYS */;
INSERT INTO `video-types` VALUES (1,'SCENE'),(2,'FILM'),(3,'EPISODE'),(4,'ADVERTISEMENT'),(5,'MUSIC VIDEO'),(6,'SPECIAL FEATURE');
/*!40000 ALTER TABLE `video-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `video-id` int(11) NOT NULL AUTO_INCREMENT,
  `video-added-timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video-type` int(11) DEFAULT NULL COMMENT 'video-type-id, can only be one',
  `video-name` varchar(100) NOT NULL,
  `video-path` varchar(255) NOT NULL COMMENT 'relative path',
  `video-description` text DEFAULT NULL,
  `video-people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `video-organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `video-tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `video-categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `video-poster` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `video-fanart` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  PRIMARY KEY (`video-id`),
  UNIQUE KEY `videos_video_path_IDX` (`video-path`) USING BTREE,
  KEY `videos_video_added_timestamp_IDX` (`video-added-timestamp`) USING BTREE,
  KEY `videos_video_type_IDX` (`video-type`) USING BTREE,
  KEY `videos_video_name_IDX` (`video-name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-26 12:35:13
