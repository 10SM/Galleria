-- MySQL dump 10.19  Distrib 10.3.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: galleria
-- ------------------------------------------------------
-- Server version	10.3.38-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio`
--

DROP TABLE IF EXISTS `audio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio` (
  `audio_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio_type` int(11) DEFAULT NULL COMMENT 'audio_type_id, can only be one',
  `audio_name` varchar(100) NOT NULL,
  `audio_path` varchar(255) NOT NULL COMMENT 'relative path',
  `audio_description` text DEFAULT NULL,
  `audio_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `audio_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `audio_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `audio_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `audio_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `audio_duration` varchar(16) DEFAULT NULL COMMENT 'How long will the file play?',
  `audio_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes',
  `audio_file_type` varchar(100) DEFAULT NULL COMMENT 'The type of audio file (e.g. mp3, ogg, etc)',
  `audio_rating` varchar(100) DEFAULT NULL COMMENT 'How nice is the file on a scale of 1 to 5?',
  `audio_warning` tinyint(1) DEFAULT NULL COMMENT 'Does the file have explicit language?',
  PRIMARY KEY (`audio_id`),
  UNIQUE KEY `audios_audio_path_IDX` (`audio_path`) USING BTREE,
  KEY `audios_audio_added_timestamp_IDX` (`audio_added_timestamp`) USING BTREE,
  KEY `audios_audio_type_IDX` (`audio_type`) USING BTREE,
  KEY `audios_audio_name_IDX` (`audio_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio`
--

LOCK TABLES `audio` WRITE;
/*!40000 ALTER TABLE `audio` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_collection`
--

DROP TABLE IF EXISTS `audio_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_collection` (
  `audio_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio_collection_type` int(11) DEFAULT NULL COMMENT 'audio_collection_type_id, can only be one',
  `audio_collection_name` varchar(100) NOT NULL,
  `audio_collection_files` text NOT NULL COMMENT 'audio_id, must be at least one, comma-separated',
  `audio_collection_description` text DEFAULT NULL,
  `audio_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `audio_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `audio_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `audio_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `audio_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `audio_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`audio_collection_id`),
  KEY `audi_collections_audio_collection_added_timestamp_IDX` (`audio_collection_added_timestamp`) USING BTREE,
  KEY `audi_collections_audio_collection_type_IDX` (`audio_collection_type`) USING BTREE,
  KEY `audi_collections_audio_collection_name_IDX` (`audio_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_collection`
--

LOCK TABLES `audio_collection` WRITE;
/*!40000 ALTER TABLE `audio_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_collection_type`
--

DROP TABLE IF EXISTS `audio_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_collection_type` (
  `audio_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio_collection_type_id`),
  FULLTEXT KEY `audio_collection_types_audio_coll_type_name_IDX` (`audio_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Audio collection types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_collection_type`
--

LOCK TABLES `audio_collection_type` WRITE;
/*!40000 ALTER TABLE `audio_collection_type` DISABLE KEYS */;
INSERT INTO `audio_collection_type` VALUES (1,'ALBUM'),(2,'AUDIO DRAMA'),(3,'AUDIOBOOK'),(4,'PODCAST');
/*!40000 ALTER TABLE `audio_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audio_type`
--

DROP TABLE IF EXISTS `audio_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_type` (
  `audio_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`audio_type_id`),
  FULLTEXT KEY `audio_types_audio_type_name_IDX` (`audio_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Audio file types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_type`
--

LOCK TABLES `audio_type` WRITE;
/*!40000 ALTER TABLE `audio_type` DISABLE KEYS */;
INSERT INTO `audio_type` VALUES (1,'CHAPTER'),(2,'EPISODE'),(3,'SAMPLE'),(4,'SKIT'),(5,'SONG'),(6,'ADVERTISEMENT');
/*!40000 ALTER TABLE `audio_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `award`
--

DROP TABLE IF EXISTS `award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award` (
  `award_id` int(11) NOT NULL AUTO_INCREMENT,
  `award_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award_type` tinyint(1) DEFAULT NULL COMMENT 'competitive/non-competitive',
  `award_name` varchar(100) NOT NULL,
  `award_description` text DEFAULT NULL,
  `award_winner` varchar(100) NOT NULL COMMENT 'person_id, can be more than one',
  `award_nominees` varchar(100) DEFAULT NULL COMMENT 'person_id, can be more than one',
  `award_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `award_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`award_id`),
  UNIQUE KEY `awards_award_name_IDX` (`award_name`) USING BTREE,
  KEY `awards_award_added_timestamp_IDX` (`award_added_timestamp`) USING BTREE,
  KEY `awards_award_type_IDX` (`award_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award`
--

LOCK TABLES `award` WRITE;
/*!40000 ALTER TABLE `award` DISABLE KEYS */;
/*!40000 ALTER TABLE `award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `award_collection`
--

DROP TABLE IF EXISTS `award_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award_collection` (
  `award_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `award_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `award_collection_formal_name` varchar(100) NOT NULL COMMENT 'e.g. The Academy Awards',
  `award_collection_nickname` varchar(100) DEFAULT NULL COMMENT 'e.g. The Oscars',
  `award_collection_description` text DEFAULT NULL,
  `award_collection_start_year` date DEFAULT NULL,
  `award_collection_end_year` date DEFAULT NULL,
  `award_collection_date` date DEFAULT NULL COMMENT 'the date of award ceremony',
  `award_collection_number` varchar(10) DEFAULT NULL COMMENT 'e.g. 8th, 50th, etc',
  `award_collection_frequency` varchar(100) DEFAULT NULL COMMENT 'not all awards are given annually',
  `award_collection_organization` int(11) NOT NULL COMMENT 'organization_id, must be included',
  `award_collection_parent` int(11) DEFAULT NULL COMMENT 'award_collection_id, can only be one',
  `award_collection_awards` text NOT NULL COMMENT 'award_id, comma separated, can be more than one',
  `award_collection_tags` text DEFAULT NULL,
  `award_collection_categories` text DEFAULT NULL,
  `award_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id',
  `award_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id',
  PRIMARY KEY (`award_collection_id`),
  UNIQUE KEY `awards_collections_award_collection_formal_name_IDX` (`award_collection_formal_name`) USING BTREE,
  UNIQUE KEY `awards_collections_award_collection_nickname_IDX` (`award_collection_nickname`) USING BTREE,
  KEY `awards_collections_award_collection_added_timestamp_IDX` (`award_collection_added_timestamp`) USING BTREE,
  KEY `awards_collections_award_collection_start_year_IDX` (`award_collection_start_year`) USING BTREE,
  KEY `awards_collections_award_collection_end_year_IDX` (`award_collection_end_year`) USING BTREE,
  KEY `awards_collections_award_collection_date_IDX` (`award_collection_date`) USING BTREE,
  KEY `awards_collections_award_collection_number_IDX` (`award_collection_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Mainly for awards ceremonies';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award_collection`
--

LOCK TABLES `award_collection` WRITE;
/*!40000 ALTER TABLE `award_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `award_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `category_name` varchar(100) NOT NULL,
  `category_slug` varchar(100) NOT NULL,
  `category_parent` int(11) DEFAULT NULL COMMENT 'category_id, can only be one',
  `category_description` text DEFAULT NULL,
  `category_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `category_color` varchar(6) DEFAULT NULL COMMENT 'RGB hex code',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `categories_category_name_IDX` (`category_name`) USING BTREE,
  UNIQUE KEY `categories_category_slug_IDX` (`category_slug`) USING BTREE,
  KEY `categories_category_added_timestamp_IDX` (`category_added_timestamp`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL COMMENT 'The name of the country in English',
  `country_demonym` varchar(100) NOT NULL COMMENT 'The term for a person from this country',
  `country_exists` tinyint(1) NOT NULL COMMENT 'Does the country still exist or not?',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_country_name_IDX` (`country_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Places where people live and die';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','Afghani',1),(2,'Albania','Albanian',1),(3,'Algeria','Algerian',1),(4,'Andorra','Andorran',1),(5,'Angola','Angolan',1),(6,'Antigua and Barbuda','Antiguan and Barbudan',1),(7,'Argentina','Argentinian',1),(8,'Armenia','Armenian',1),(9,'Australia','Australian',1),(10,'Austria','Austrian',1),(11,'Azerbaijan','Azerbaijani',1),(12,'Bahamas','Bahamian',1),(13,'Bahrain','Bahraini',1),(14,'Bangladesh','Bangladeshi',1),(15,'Barbados','Barbadian',1),(16,'Belarus','Belarusian',1),(17,'Belgium','Belgian',1),(18,'Belize','Belizean',1),(19,'Benin','Beninese',1),(20,'Bhutan','Bhutanese',1),(21,'Bolivia','Bolivian',1),(22,'Bosnia and Herzegovina','Bosnian and Herzegovinian',1),(23,'Botswana','Motswana',1),(24,'Brazil','Brazilian',1),(25,'Brunei','Bruneian',1),(26,'Bulgaria','Bulgarian',1),(27,'Burkina Faso','Burkinabe',1),(28,'Burundi','Burundian',1),(29,'Cambodia','Cambodian',1),(30,'Cameroon','Cameroonian',1),(31,'Canada','Canadian',1),(32,'Cape Verde','Cape Verdean',1),(33,'Central African Republic','Central African',1),(34,'Chad','Chadian',1),(35,'Chile','Chilean',1),(36,'China','Chinese',1),(37,'Colombia','Colombian',1),(38,'Comoros','Comoran',1),(39,'Republic of the Congo','Congolese',1),(40,'Democratic Republic of the Congo','Congolese',1),(41,'Costa Rica','Costa Rican',1),(42,'Cote d\'Ivoire','Ivorian',1),(43,'Croatia','Croatian',1),(44,'Cuba','Cuban',1),(45,'Cyprus','Cypriot',1),(46,'Czechia','Czech',1),(47,'Denmark','Danish',1),(48,'Djibouti','Djibouti',1),(49,'Dominica','Dominican',1),(50,'Dominican Republic','Dominican',1),(51,'East Timor','East Timorese',1),(52,'Ecuador','Ecuadorean',1),(53,'Egypt','Egyptian',1),(54,'El Salvador','Salvadoran',1),(55,'Equatorial Guinea','Equatoguinean',1),(56,'Eritrea','Eritrean',1),(57,'Estonia','Estonian',1),(58,'Ethiopia','Ethiopian',1),(59,'Fiji','Fijian',1),(60,'Finland','Finnish',1),(61,'France','French',1),(62,'Gabon','Gabonese',1),(63,'Gambia','Gambian',1),(64,'Georgia','Georgian',1),(65,'Germany','German',1),(66,'Ghana','Ghanian',1),(67,'Greece','Greek',1),(68,'Grenada','Grenadan',1),(69,'Guatemala','Guatemalan',1),(70,'Guinea','Guinean',1),(71,'Guinea-Bissau','Guinea-Bissauan',1),(72,'Guyana','Guyanese',1),(73,'Haiti','Hiatian',1),(74,'Honduras','Honduran',1),(75,'Hungary','Hungarian',1),(76,'Iceland','Icelandic',1),(77,'India','Indian',1),(78,'Indonesia','Indonesian',1),(79,'Iran','Iranian',1),(80,'Iraq','Iraqi',1),(81,'Ireland','Irish',1),(82,'Israel','Israeli',1),(83,'Italy','Italian',1),(84,'Jamaica','Jamaican',1),(85,'Japan','Japanese',1),(86,'Jordan','Jordanian',1),(87,'Kazakhstan','Kazakh',1),(88,'Kenya','Kenyan',1),(89,'Kiribati','I-Kiribati',1),(90,'North Korea','North Korean',1),(91,'South Korea','South Korean',1),(92,'Kosovo','Kosovar',1),(93,'Kuwait','Kuwaiti',1),(94,'Kyrgyzstan','Kyrgyz',1),(95,'Laos','Laotian',1),(96,'Latvia','Latvian',1),(97,'Lebanon','Lebanese',1),(98,'Lesotho','Mosotho',1),(99,'Liberia','Liberian',1),(100,'Libya','Libyan',1),(101,'Liechtenstein','Liechtensteiner',1),(102,'Lithuania','Lithuanian',1),(103,'Luxembourg','Luxembourger',1),(104,'Macedonia','Macedonian',1),(105,'Madagascar','Malagasy',1),(106,'Malawi','Malawian',1),(107,'Malaysia','Malaysian',1),(108,'Maldives','Maldivian',1),(109,'Mali','Malian',1),(110,'Malta','Maltese',1),(111,'Marshall Islands','Marshallese',1),(112,'Mauritania','Mauritanian',1),(113,'Mauritius','Mauritian',1),(114,'Mexico','Mexican',1),(115,'Federated States of Micronesia','Micronesian',1),(116,'Moldova','Moldovan',1),(117,'Monaco','Monagasque',1),(118,'Mongolia','Mongolian',1),(119,'Montenegro','Montenegrin',1),(120,'Morocco','Moroccan',1),(121,'Mozambique','Mozambican',1),(122,'Myanmar','Burmese',1),(123,'Namibia','Namibian',1),(124,'Nauru','Nauruan',1),(125,'Nepal','Nepalese',1),(126,'Netherlands','Dutch',1),(127,'New Zealand','New Zealander',1),(128,'Nicaragua','Nicaraguan',1),(129,'Niger','Nigerien',1),(130,'Nigeria','Nigerian',1),(131,'Norway','Norwegian',1),(132,'Oman','Omani',1),(133,'Pakistan','Pakistani',1),(134,'Palau','Palauan',1),(135,'Panama','Panamanian',1),(136,'Papua New Guinea','Papua New Guinean',1),(137,'Paraguay','Paraguayan',1),(138,'Peru','Peruvian',1),(139,'Philippines','Filipino',1),(140,'Poland','Polish',1),(141,'Portugal','Portuguese',1),(142,'Qatar','Qatari',1),(143,'Romania','Romanian',1),(144,'Russia','Russian',1),(145,'Rwanda','Rwandan',1),(146,'Saint Kitts and Nevis','Kittian',1),(147,'Saint Lucia','Saint Lucian',1),(148,'Samoa','Samoan',1),(149,'San Marino','Sammarinese',1),(150,'Sao Tome and Principe','Sao Tomean',1),(151,'Saudi Arabia','Saudi Arabian',1),(152,'Senegal','Senegalese',1),(153,'Serbia','Serbian',1),(154,'Seychelles','Seychellois',1),(155,'Sierra Leone','Sierra Leonean',1),(156,'Singapore','Singaporean',1),(157,'Slovakia','Slovakian',1),(158,'Slovenia','Slovenian',1),(159,'Solomon Islands','Solomon Islander',1),(160,'Somalia','Somali',1),(161,'South Africa','South African',1),(162,'South Sudan','South Sudanese',1),(163,'Spain','Spanish',1),(164,'Sri Lanka','Sri Lankan',1),(165,'Sudan','Sudanese',1),(166,'Suriname','Surinamer',1),(167,'Eswatini','Liswati',1),(168,'Sweden','Swedish',1),(169,'Switzerland','Swiss',1),(170,'Syria','Syrian',1),(171,'Taiwan','Taiwanese',1),(172,'Tajikistan','Tajik',1),(173,'Tanzania','Tanzanian',1),(174,'Thailand','Thai',1),(175,'Togo','Togolese',1),(176,'Tonga','Tongan',1),(177,'Trinidad and Tobago','Trinidadian and Tobagonian',1),(178,'Tunisia','Tunisian',1),(179,'Türkiye','Turkish',1),(180,'Turkmenistan','Turkmen',1),(181,'Tuvalu','Tuvaluan',1),(182,'Uganda','Ugandan',1),(183,'Ukraine','Ukrainian',1),(184,'United Arab Emirates','Emirati',1),(185,'United Kingdom','British',1),(186,'United States','American',1),(187,'Uruguay','Uruguayan',1),(188,'Uzbekistan','Uzbek',1),(189,'Vanuatu','Ni-Vanuatu',1),(190,'Venezuela','Venzuelan',1),(191,'Vietnam','Vietnamese',1),(192,'Yemen','Yemeni',1),(193,'Zambia','Zambian',1),(194,'Zimbabwe','Zimbabwean',1);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image_type` int(11) DEFAULT NULL COMMENT 'image_type_id, can only be one',
  `image_name` varchar(100) NOT NULL,
  `image_path` varchar(255) NOT NULL COMMENT 'relative path',
  `image_description` text DEFAULT NULL,
  `image_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `image_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `image_date` date DEFAULT NULL,
  `image_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `image_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `image_size` int(11) DEFAULT NULL COMMENT 'image size in kilobytes',
  `image_file_type` varchar(100) DEFAULT NULL,
  `image_rating` int(11) DEFAULT NULL COMMENT 'On a scale of one to five, how nice is it?',
  `image_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the image safe for people to view at work?',
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `images_image_path_IDX` (`image_path`) USING BTREE,
  KEY `images_image_added_timestamp_IDX` (`image_added_timestamp`) USING BTREE,
  KEY `images_image_type_IDX` (`image_type`) USING BTREE,
  KEY `images_image_name_IDX` (`image_name`) USING BTREE,
  KEY `images_image_date_IDX` (`image_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=871 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_collection`
--

DROP TABLE IF EXISTS `image_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_collection` (
  `image_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image_collection_type` int(11) DEFAULT NULL COMMENT 'image_collection_type_id, can only be one',
  `image_collection_name` varchar(100) NOT NULL,
  `image_collection_files` text NOT NULL COMMENT 'image_id, must be at least one, comma-separated',
  `image_collection_description` text DEFAULT NULL,
  `image_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `image_collection_organizations` varchar(300) DEFAULT NULL,
  `image_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `image_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `image_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`image_collection_id`),
  KEY `image_collections_image_collection_added_timestamp_IDX` (`image_collection_added_timestamp`) USING BTREE,
  KEY `image_collections_image_collection_type_IDX` (`image_collection_type`) USING BTREE,
  KEY `image_collections_image_collection_name_IDX` (`image_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_collection`
--

LOCK TABLES `image_collection` WRITE;
/*!40000 ALTER TABLE `image_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `image_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_collection_type`
--

DROP TABLE IF EXISTS `image_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_collection_type` (
  `image_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`image_collection_type_id`),
  UNIQUE KEY `image_collection_types_image_coll_type_name_IDX` (`image_collection_type_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of image collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_collection_type`
--

LOCK TABLES `image_collection_type` WRITE;
/*!40000 ALTER TABLE `image_collection_type` DISABLE KEYS */;
INSERT INTO `image_collection_type` VALUES (2,'ATLAS'),(1,'GALLERY'),(3,'PORTFOLIO');
/*!40000 ALTER TABLE `image_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_type`
--

DROP TABLE IF EXISTS `image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_type` (
  `image_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`image_type_id`),
  FULLTEXT KEY `image_types_image_type_name_IDX` (`image_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of image files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_type`
--

LOCK TABLES `image_type` WRITE;
/*!40000 ALTER TABLE `image_type` DISABLE KEYS */;
INSERT INTO `image_type` VALUES (1,'CHART'),(2,'ILLUSTRATION'),(3,'INFOGRAPHIC'),(4,'LOGO'),(5,'MAP'),(6,'PAINTING'),(7,'PHOTOGRAPH'),(8,'SKETCH'),(9,'POSTER'),(10,'ADVERTISEMENT'),(11,'SCREENSHOT');
/*!40000 ALTER TABLE `image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `organization_name` varchar(100) NOT NULL,
  `organization_aliases` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `organization_start_date` date DEFAULT NULL,
  `organization_end_date` date DEFAULT NULL,
  `organization_description` text DEFAULT NULL,
  `organization_place` varchar(100) DEFAULT NULL COMMENT 'city, state, country',
  `organization_url` varchar(255) DEFAULT NULL COMMENT 'Offical website or social media',
  `organization_email` varchar(255) DEFAULT NULL COMMENT 'Official emal address',
  `organization_logo` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `organization_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one.',
  `organization_hero_image` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `organization_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `organization_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `organization_rating` int(11) DEFAULT NULL COMMENT 'The rating on a scale of 1 to 5',
  `organization_wikipedia` varchar(255) DEFAULT NULL COMMENT 'If there is a wikipedia page for the organization, it should be listed here',
  `organization_imdb` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s IMDB page',
  `organization_mastodon` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Mastodon account',
  `organization_pixelfed` varchar(255) DEFAULT NULL COMMENT 'URL of the organization''s official PixelFed account',
  `organization_youtube` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Youtube account',
  `organization_instagram` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Instagram account',
  `organization_tiktok` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official TikTok account',
  `organization_twitter` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Twitter account',
  `organization_facebook` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official Facebook account',
  `organization_goodreads` varchar(255) DEFAULT NULL COMMENT 'URL for the organization''s official GoodReads account.',
  PRIMARY KEY (`organization_id`),
  UNIQUE KEY `organizations_organization_name_IDX` (`organization_name`) USING BTREE,
  KEY `organizations_organization_added_timestamp_IDX` (`organization_added_timestamp`) USING BTREE,
  KEY `organizations_organization_start_date_IDX` (`organization_start_date`) USING BTREE,
  KEY `organizations_organization_end_date_IDX` (`organization_end_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `person_name` varchar(100) NOT NULL,
  `person_sort_name` varchar(100) NOT NULL,
  `person_aliases` varchar(100) DEFAULT NULL,
  `person_pronouns` varchar(2) DEFAULT NULL,
  `person_birth_date` date DEFAULT NULL,
  `person_birth_place` varchar(100) DEFAULT NULL,
  `person_death_date` date NOT NULL DEFAULT '0000-00-00',
  `person_death_place` varchar(100) DEFAULT NULL,
  `person_death_cause` varchar(100) DEFAULT NULL,
  `person_description` text DEFAULT NULL,
  `person_nationalities` varchar(100) DEFAULT NULL COMMENT 'can be more than one',
  `person_url` varchar(255) DEFAULT NULL COMMENT 'URL for official website',
  `person_email` varchar(255) DEFAULT NULL COMMENT 'professional email address',
  `person_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_hero_image` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `person_tags` varchar(300) DEFAULT NULL COMMENT 'can be more than one, tag-id, comma-separated',
  `person_categories` varchar(300) DEFAULT NULL COMMENT 'can be more than one, category-id, comma-separated',
  `person_rating` int(11) DEFAULT NULL COMMENT 'The rating on a scale of 1 to 5',
  `person_wikipedia` varchar(255) DEFAULT NULL COMMENT 'URL for a Wikipedia article about the person',
  `person_imdb` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s IMDB page',
  `person_mastodon` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Mastodon account',
  `person_pixelfed` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Pixelfed account',
  `person_youtube` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s YouTube channel',
  `person_instagram` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Instagram account',
  `person_tiktok` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s TikTok account',
  `person_twitter` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Twitter account',
  `person_facebook` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s Facebook account',
  `person_goodreads` varchar(255) DEFAULT NULL COMMENT 'URL for the person''s GoodReads author page',
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `people_person_name_IDX` (`person_name`) USING BTREE,
  KEY `people_person_added_timestamp_IDX` (`person_added_timestamp`) USING BTREE,
  KEY `people_person_birth_date_IDX` (`person_birth_date`) USING BTREE,
  KEY `people_person_death_date_IDX` (`person_death_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pronoun`
--

DROP TABLE IF EXISTS `pronoun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pronoun` (
  `pronoun_id` int(11) NOT NULL AUTO_INCREMENT,
  `pronoun_name` varchar(100) NOT NULL,
  PRIMARY KEY (`pronoun_id`),
  UNIQUE KEY `pronoun_pronoun_id_IDX` (`pronoun_id`) USING BTREE,
  UNIQUE KEY `pronoun_pronoun_name_IDX` (`pronoun_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pronoun`
--

LOCK TABLES `pronoun` WRITE;
/*!40000 ALTER TABLE `pronoun` DISABLE KEYS */;
INSERT INTO `pronoun` VALUES (1,'he/him.his'),(2,'she/her/hers'),(3,'they/them/their');
/*!40000 ALTER TABLE `pronoun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tag_name` varchar(100) NOT NULL,
  `tag_slug` varchar(100) NOT NULL,
  `tag_description` text DEFAULT NULL,
  `tag_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `tag_color` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tags_tag_name_IDX` (`tag_name`) USING BTREE,
  UNIQUE KEY `tags_tag_slug_IDX` (`tag_slug`) USING BTREE,
  KEY `tags_tag_added_timestamp_IDX` (`tag_added_timestamp`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text`
--

DROP TABLE IF EXISTS `text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text` (
  `text_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text_type` int(11) DEFAULT NULL COMMENT 'text-type-id, can only be one',
  `text_name` varchar(100) NOT NULL,
  `text_path` varchar(255) NOT NULL COMMENT 'relative path',
  `text_description` text DEFAULT NULL,
  `text_people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `text_organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `text_tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `text_categories` varchar(300) DEFAULT NULL COMMENT 'category-id can be more than one, comma-separated',
  `text_thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `text_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `text_pages` int(11) DEFAULT NULL COMMENT 'The approximate number of pages in the document',
  `text_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes',
  `text_file_type` varchar(100) DEFAULT NULL COMMENT 'The file format of the document (e.g. pdf, epub, cbr, etc)',
  `text_rating` int(11) DEFAULT NULL COMMENT 'On a scale of 1 to 5, how good is the text?',
  `text_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the text safe for young minds?',
  PRIMARY KEY (`text_id`),
  UNIQUE KEY `texts_text_path_IDX` (`text_path`) USING BTREE,
  KEY `texts_text_added_timestamp_IDX` (`text_added_timestamp`) USING BTREE,
  KEY `texts_text_type_IDX` (`text_type`) USING BTREE,
  KEY `texts_text_name_IDX` (`text_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text`
--

LOCK TABLES `text` WRITE;
/*!40000 ALTER TABLE `text` DISABLE KEYS */;
/*!40000 ALTER TABLE `text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_collection`
--

DROP TABLE IF EXISTS `text_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_collection` (
  `text_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text_collection_type` int(11) DEFAULT NULL COMMENT 'text_collection_type_id, can only be one',
  `text_collection_name` varchar(100) NOT NULL,
  `text_collection_files` text NOT NULL COMMENT 'text_id, must be at least one, comma-separated',
  `text_collection_description` text DEFAULT NULL,
  `text_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `text_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `text_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `text_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `text_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `text_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`text_collection_id`),
  KEY `text_collections_text_collection_added_timestamp_IDX` (`text_collection_added_timestamp`) USING BTREE,
  KEY `text_collections_text_collection_type_IDX` (`text_collection_type`) USING BTREE,
  KEY `text_collections_text_collection_name_IDX` (`text_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_collection`
--

LOCK TABLES `text_collection` WRITE;
/*!40000 ALTER TABLE `text_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `text_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_collection_type`
--

DROP TABLE IF EXISTS `text_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_collection_type` (
  `text_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`text_collection_type_id`),
  FULLTEXT KEY `text_collection_types_text_coll_type_name_IDX` (`text_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of text collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_collection_type`
--

LOCK TABLES `text_collection_type` WRITE;
/*!40000 ALTER TABLE `text_collection_type` DISABLE KEYS */;
INSERT INTO `text_collection_type` VALUES (1,'BOOK'),(2,'MAGAZINE'),(3,'JOURNAL'),(4,'NEWSPAPER'),(5,'BLOG'),(6,'FORUM'),(7,'PORTFOLIO');
/*!40000 ALTER TABLE `text_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_type`
--

DROP TABLE IF EXISTS `text_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_type` (
  `text_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`text_type_id`),
  FULLTEXT KEY `text_types_text_type_name_IDX` (`text_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of text files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_type`
--

LOCK TABLES `text_type` WRITE;
/*!40000 ALTER TABLE `text_type` DISABLE KEYS */;
INSERT INTO `text_type` VALUES (1,'BOOK CHAPTER'),(2,'JOURNAL ARTICLE'),(3,'MAGAZINE ARTICLE'),(4,'NEWSPAPER ARTICLE'),(5,'TRANSCRIPT'),(6,'THESIS'),(7,'DISSERTATION'),(8,'ESSAY'),(9,'BLOG POST'),(10,'BLOG PAGE'),(11,'FORUM POST'),(12,'COMMENT'),(13,'CORRESPONDENCE'),(14,'ADVERTISEMENT');
/*!40000 ALTER TABLE `text_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video_type` int(11) DEFAULT NULL COMMENT 'video-type-id, can only be one',
  `video_name` varchar(100) NOT NULL,
  `video_path` varchar(255) NOT NULL COMMENT 'relative path',
  `video_description` text DEFAULT NULL,
  `video_people` varchar(300) DEFAULT NULL COMMENT 'person-id, can be more than one, comma-separated',
  `video_organizations` varchar(300) DEFAULT NULL COMMENT 'organization-id, can be more than one, comma-separated',
  `video_poster` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `video_fanart` int(11) DEFAULT NULL COMMENT 'image-id, can only be one',
  `video_tags` varchar(300) DEFAULT NULL COMMENT 'tag-id, can be more than one, comma-separated',
  `video_categories` varchar(300) DEFAULT NULL COMMENT 'category-id, can be more than one, comma-separated',
  `video_width` int(11) DEFAULT NULL,
  `video_height` int(11) DEFAULT NULL,
  `video_duration` varchar(16) DEFAULT NULL,
  `video_size` int(11) DEFAULT NULL COMMENT 'The size of the file in kilobytes',
  `video_file_type` varchar(100) DEFAULT NULL COMMENT 'The file extension (e.g. m4v, webm, etc)',
  `video_rating` int(11) DEFAULT NULL COMMENT 'On a scale of 1 to 5, how enjoyable is the video?',
  `video_warning` tinyint(1) DEFAULT NULL COMMENT 'Is the video safe for young minds?',
  PRIMARY KEY (`video_id`),
  UNIQUE KEY `videos_video_path_IDX` (`video_path`) USING BTREE,
  KEY `videos_video_added_timestamp_IDX` (`video_added_timestamp`) USING BTREE,
  KEY `videos_video_type_IDX` (`video_type`) USING BTREE,
  KEY `videos_video_name_IDX` (`video_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_collection`
--

DROP TABLE IF EXISTS `video_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_collection` (
  `video_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_collection_added_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `video_collection_type` int(11) DEFAULT NULL COMMENT 'video_collection_type_id, can only be one',
  `video_collection_name` varchar(100) NOT NULL,
  `video_collection_files` text NOT NULL,
  `video_collection_description` text DEFAULT NULL,
  `video_collection_people` varchar(300) DEFAULT NULL COMMENT 'person_id, can be more than one, comma-separated',
  `video_collection_organizations` varchar(300) DEFAULT NULL COMMENT 'organization_id, can be more than one, comma-separated',
  `video_collection_tags` varchar(300) DEFAULT NULL COMMENT 'tag_id, can be more than one, comma-separated',
  `video_collection_categories` varchar(300) DEFAULT NULL COMMENT 'category_id, can be more than one, comma-separated',
  `video_collection_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  `video_collection_alternate_thumbnail` int(11) DEFAULT NULL COMMENT 'image_id, can only be one',
  PRIMARY KEY (`video_collection_id`),
  KEY `video_collections_video_collection_added_timestamp_IDX` (`video_collection_added_timestamp`) USING BTREE,
  KEY `video_collections_video_collection_type_IDX` (`video_collection_type`) USING BTREE,
  KEY `video_collections_video_collection_name_IDX` (`video_collection_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_collection`
--

LOCK TABLES `video_collection` WRITE;
/*!40000 ALTER TABLE `video_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_collection_type`
--

DROP TABLE IF EXISTS `video_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_collection_type` (
  `video_collection_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_collection_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`video_collection_type_id`),
  FULLTEXT KEY `video_collection_types_video_coll_type_name_IDX` (`video_collection_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of video collections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_collection_type`
--

LOCK TABLES `video_collection_type` WRITE;
/*!40000 ALTER TABLE `video_collection_type` DISABLE KEYS */;
INSERT INTO `video_collection_type` VALUES (1,'FILM'),(2,'FILM SERIES'),(3,'TELEVISION SERIES'),(4,'PORTFOLIO');
/*!40000 ALTER TABLE `video_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_type`
--

DROP TABLE IF EXISTS `video_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_type` (
  `video_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`video_type_id`),
  FULLTEXT KEY `video_types_video_type_name_IDX` (`video_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Types of video files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_type`
--

LOCK TABLES `video_type` WRITE;
/*!40000 ALTER TABLE `video_type` DISABLE KEYS */;
INSERT INTO `video_type` VALUES (1,'SCENE'),(2,'FILM'),(3,'EPISODE'),(4,'ADVERTISEMENT'),(5,'MUSIC VIDEO'),(6,'SPECIAL FEATURE');
/*!40000 ALTER TABLE `video_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'galleria'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-28  7:08:43
