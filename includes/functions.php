<?php
/**
 * includes/functions.php
 *
 * A file which should be used to contain functions for Galleria
 */

include "galleria-metadata.php";

$dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
mysqli_set_charset($dbconn, "utf8mb4");


/**
 * Function nicetext($text) sanitizes text inputs from forms
 */
function nicetext($text) {
    // get rid of whitespace characters at start or end of text
    $text = trim($text);
    // removes \ backslash escape characters
    $text = stripslashes($text);
    // converts special characters (i.e. < > &, etc) into their html entities
    #$text = htmlspecialchars($text,ENT_QUOTES,'UTF-8',true);
    $text = preg_replace('/\'/i', '&apos;', $text);
    return $text;
}

/**
 * Function makeslug($text) turns a title into a url-friendly slug
 */
function makeslug($text) {
    //taken from https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes

    // make all letters lowercase
    $text = strtolower($text);
    // make it alphanumeric and turn other characters into hyphens
    $text = preg_replace("/[^a-z0-9_\s-]/", "", $text);
    // clean up multiple hyphens and whitespaces
    $text = preg_replace("/[\s-]+/", " ", $text);
    // convert whitespaces and underscores to hyphens
    $text = preg_replace("/[\s_]/", "-", $text);
    return $text;
}

/**
 * Function iscolor($text) validates whether a string is an RGB hex code.
 */
function iscolor($text) {
    // adapted from https://php-mysql-articles.blogspot.com/2009/06/how-to-validate-hex-color-in-php.html

    // this version doesn't need #
    if(preg_match('/^[a-f0-9]{6}$/i', $text)) {
        return $text;
    } else {
        return NULL;
    }
}

/**
 * Function redirect($location) will redirect from one webpage to another. It basically simplifies a builtin PHP function/
 */
function redirect($location) {
    return header("Location: $location");
}
?>
