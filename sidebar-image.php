<?php
/**
 * Displays image information
 */

/** Get the name of the image type */
$imagetypeq = "SELECT * FROM image_type";
$imagetypequery = mysqli_query($dbconn,$imagetypeq);
while ($imagetypeopt = mysqli_fetch_assoc($imagetypequery)) {
    $imagetypeid    = $imagetypeopt['image_type_id'];
    $imagetypename  = $imagetypeopt['image_type_name'];

    if ($getimagetype == $imagetypeid) {
        $goodimagetypename = $imagetypename;
        $goodimagetypeid    = $imagetypeid;
    }
}

 /** Fix the date format */
 $gooddate = date("F j, Y",strtotime($getimagedate));
?>
                    <div class="vert-block">                <!-- Future versions will show some basic metadata here -->
                        <h2 class="block-title">Metadata</h2>
                         <table>
                            <tr>
                                <td>Width</td>
                                <td class="num">1920</td>
                            </tr>
                            <tr>
                                <td>Height</td>
                                <td class="num">1080</td>
                            </tr>
                            <tr>
                                <td>Size</td>
                                <td class="num">123 kb</td>
                            </tr>
                            <tr>
                                <td>File type</td>
                                <td class="num">PNG</td>
                            </tr>
                            <tr>
                                <td>Image type</td>
                                <td class="num"><?php echo $goodimagetypename; ?></td>
                            </tr>
<?php
if ($getimagedate !== '' && $getimagedate !== NULL) {
    echo "\t\t\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t\t\t<td>Image date</td>\n";
    echo "\t\t\t\t\t\t\t\t<td class=\"num\">".$gooddate."</td>\n";
    echo "\t\t\t\t\t\t\t</tr>\n";
}
?>
                            <tr>
                                <td>Rating</td>
                                <td class="num">&starf;&starf;&starf;&starf;&star;</td>
                            </tr>
                        </table>
                        <p><a href="https://codeberg.org/10SM/Galleria">Galleria</a> <?php echo $version; ?></p>
                    </div> <!-- end div .vert-block -->
