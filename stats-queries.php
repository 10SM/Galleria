<?php
/**
 * Queries the database on behalf of sidebar-stats.php
 */

/**
 * Get the number of images in the database
 */
$imgstatsq = "SELECT * FROM image";
$imgstatsquery = mysqli_query($dbconn,$imgstatsq);
$imgstatsnum = mysqli_num_rows($imgstatsquery);

/**
 * Get the number of videos in the database
 */
$vidstatsq = "SELECT * FROM video";
$vidstatsquery = mysqli_query($dbconn,$vidstatsq);
$vidstatsnum = mysqli_num_rows($vidstatsquery);

/**
 * Get the number of audio files in the database
 */
$audstatsq = "SELECT * FROM audio";
$audstatsquery = mysqli_query($dbconn,$audstatsq);
$audstatsnum = mysqli_num_rows($audstatsquery);

/**
 * Get the number of texts in the database
 */
$txtstatsq = "SELECT * FROM text";
$txtstatsquery = mysqli_query($dbconn,$txtstatsq);
$txtstatsnum = mysqli_num_rows($txtstatsquery);

/**
 * Get the number of awards in the database
 */
$awastatsq = "SELECT * FROM award";
$awastatsquery = mysqli_query($dbconn,$awastatsq);
$awastatsnum = mysqli_num_rows($awastatsquery);

/**
 * Get the number of people in the database
 */
$pplstatsq = "SELECT * FROM person";
$pplstatsquery = mysqli_query($dbconn,$pplstatsq);
$pplstatsnum = mysqli_num_rows($pplstatsquery);

/**
 * Get the number of organizations in the database
 */
$orgstatsq = "SELECT * FROM organization";
$orgstatsquery = mysqli_query($dbconn,$orgstatsq);
$orgstatsnum = mysqli_num_rows($orgstatsquery);

/**
 * Get the number of tags in the database
 */
$tagstatsq = "SELECT * FROM tag";
$tagstatsquery = mysqli_query($dbconn,$tagstatsq);
$tagstatsnum = mysqli_num_rows($tagstatsquery);

/**
 * Get the number of categories in the database
 */
$catstatsq = "SELECT * FROM category";
$catstatsquery = mysqli_query($dbconn,$catstatsq);
$catstatsnum = mysqli_num_rows($catstatsquery);
?>
