# Galleria

*Galleria* is an aspiration at the moment. It's intended to be a program written in PHP but run on local computers for the purpose of playing audio and video files, as well as displaying images and ebooks.

The name *Galleria* comes from a common name for malls in the United States, and this software is intended to be something like a digital equivalent of a mall with a music store, a bookstore, a movie theater, and a poster store.

But it's also more than that. It's also a bit like a library because it can have reference materials on the people and the companies that created the images, music, films, or books, but it's also possible to export those details to a citation manager such as Zotero. That's the plan, at least.

Aspirations.
---
The initial goal is to make *Galleria* work with images, videos, audio files, as well as having tags, categories, people, organizations, and awards. Working with document files (e.g. PDF, EPUB, CBR, CBZ, etc.) will take a little longer. When it gets to the point of working with document files, work will also be started on setting up *Galleria* for localization, WIA-ARIA standards, themability, and pluginability.
