<?php
require "includes/galleria-metadata.php";
?>
<html>
    <head>
        <title><?php echo $page_name; ?> | Galleria</title>
        <link rel="stylesheet" href="themes/dark.css">
        <link rel="icon" sizes="any" type="image/svg+xml" href="includes/favicon.svg">
        <link rel="alternate icon" type="image/x-icon" href="includes/favicon.ico">
        <link rel="icon" sizes="192x192" type="image/png" href="includes/galleria-icon-192.png">
        <link rel="apple-touch-icon" sizes="180x180" type="image/png" href="includes/galleria-icon-180.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <header>
            <a href="/">&#8962;</a> | <a href="audio-list.php">AUDIO</a> | <a href="image-list.php">IMAGES</a> | <a href="video-list.php">VIDEO</a> | <a href="text-list.php">TEXTS</a> | <a href="award-list.php">AWARDS</a> | <a href="person-list.php">PEOPLE</a> | <a href="organization-list.php">ORGANIZATIONS</a> | <a href="tag-list.php" >TAGS</a> | <a href="category-list.php" >CATEGORIES</a> | <a href="admin/index.php">ADMIN</a>
        </header>
<!-- -------------------------------------------------------------------------- END HEADER.PHP -->
