<?php
/**
 * This file displays a list of all items with a certain tag.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';


/** get the ID for this tag */
if (isset($_GET["tagid"])) {
    $get_id = $_GET["tagid"];
} else {
    $get_id = "";
}

/** If we have an ID, lets get the data from the 'tag' table */
if ($get_id != '') {

    /** let's create the query */
    $tagpageq = "SELECT * FROM tag WHERE tag_id=".$get_id."";
    $tagpagequery = mysqli_query($dbconn,$tagpageq);

    while ($tagpageopt = mysqli_fetch_assoc($tagpagequery)) {
        $tagpageid      = $tagpageopt['tag_id'];
        $tagpagename    = $tagpageopt['tag_name'];
        $tagpagedesc    = $tagpageopt['tag_description'];
        $tagpagecolor   = $tagpageopt['tag_color'];
    }
}


$page_name = $tagpagename;
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START TAG.PHP -->
        <main>
	        <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>
                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
				            <h1><span style="height:30px;width:30px;border-radius:50%;background-color:#<?php echo $tagpagecolor; ?>;display:inline-block;"></span>&nbsp;<?php echo $tagpagename; ?></h1>

				            <p><?php echo $tagpagedesc; ?></p>
				            <p>Future versions will include sections or tabs for people, organizations, files, collections, etc that have this tag.</p>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END TAG.PHP -->
<?php require 'footer.php'; ?>
