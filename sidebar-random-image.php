<?php
/**
 * Gets a random image from the `image` table and displays it
 */

?>
                    <div class="vert-block">                <!-- picture-of-the-day: a function randomly chooses a picture to display each day -->
                        <h2 class="block-title">Random image</h2>
                        <figure class="potd">
<?php
/** Get a random image ID */
$randimgq = "SELECT * FROM image ORDER BY RAND() LIMIT 1";
$randimgquery = mysqli_query($dbconn,$randimgq);
while ($randimgopt = mysqli_fetch_assoc($randimgquery)) {
    $randimgid      = $randimgopt['image_id'];
    $randimgname    = $randimgopt['image_name'];
    $randimgdesc    = $randimgopt['image_description'];

    echo "\t\t\t\t\t\t\t\t<img src=\"thumb.php?imageid=".$randimgid."\" alt=\"".$randimgdesc."\" title=\"".$randimgname."\">\n";
    echo "\t\t\t\t\t\t\t\t<figcaption>".$randimgname."</figcaption>\n";
}
?>
                        </figure>
                    </div> <!-- end div .vert-block -->
