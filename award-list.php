<?php
/**
 * This file displays a list of all awards in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listawardq = "SELECT * FROM award ORDER BY award_name ASC";
$listawardquery = mysqli_query($dbconn,$listawardq);

$page_name = "All awards";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START AWARD-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/award-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listawardquery) > 0) {
    while ($listawardopt = mysqli_fetch_assoc($listawardquery)) {
        $awardid      = $listawardopt['award_id'];
        $awardname    = $listawardopt['award_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"award.php?awardid=".$awardid."\">".$awardname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"admin/award-edit.php?awardid=".$awardid."\">Edit</a> | <a href=\"admin/award-delete.php?awardid=".$awardid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listawardquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no awards in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END AWARD-LIST.PHP -->
<?php require 'footer.php'; ?>
