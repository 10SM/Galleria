<?php
/**
 * This file displays a list of all tags in Galleria.
 */

/** Files required to go further */
require 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';

/** Here is our query */
$listtagq = "SELECT * FROM tag ORDER BY tag_name ASC";
$listtagquery = mysqli_query($dbconn,$listtagq);

$page_name = "All tags";
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START TAG-LIST.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-random-image.php';
require 'sidebar-stats.php';
?>                </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <p class="add-new-span"><a href="admin/tag-add.php">Add new</a></p>
                        <table class="item-table">
<?php

if(mysqli_num_rows($listtagquery) > 0) {
    while ($listtagopt = mysqli_fetch_assoc($listtagquery)) {
        $tagid      = $listtagopt['tag_id'];
        $tagname    = $listtagopt['tag_name'];

        echo "\t\t\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"tag.php?tagid=".$tagid."\">".$tagname."</td>\n";
        echo "\t\t\t\t\t\t\t\t<td><a href=\"admin/tag-edit.php?tagid=".$tagid."\">Edit</a> | <a href=\"admin/tag-delete.php?tagid=".$tagid."\">Delete</a></td>\n";
        echo "\t\t\t\t\t\t\t</td>\n";
    }
} else if(mysqli_num_rows($listtagquery) == 0) {
    echo "\t\t\t\t\t\t\t<tr><td>There are no tags in the database</td></tr>\n";
}


?>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END TAG-LIST.PHP -->
<?php require 'footer.php'; ?>
