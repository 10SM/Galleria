<?php
/**
 * This file displays an image in Galleria.
 */

/** Files required to go further */
require_once 'includes/galleria-metadata.php';
require 'includes/functions.php';
require 'stats-queries.php';


/** get the ID for this image */
if (isset($_GET["imageid"])) {
    $get_id = $_GET["imageid"];
} else {
    $get_id = "";
}

/** Here is our main query */
$getimageq = "SELECT * FROM image WHERE image_id='".$get_id."'";
$getimagequery = mysqli_query($dbconn,$getimageq);
while ($getimageopt = mysqli_fetch_assoc($getimagequery)) {

    $getimageid          = $getimageopt['image_id'];
    $getimageadded       = $getimageopt['image_added_timestamp'];
    $getimagetype        = $getimageopt['image_type'];
    $getimagename        = $getimageopt['image_name'];
    $getimagepath        = $getimageopt['image_path'];
    $getimagedesc        = $getimageopt['image_description'];
    $getimageppl         = $getimageopt['image_people'];
    $getimageorgs        = $getimageopt['image_organizations'];
    $getimagedate        = $getimageopt['image_date'];
    $getimagetags        = $getimageopt['image_tags'];
    $getimagecats        = $getimageopt['image_categories'];
}

$page_name = $getimagename;
require 'header.php';
?>
<!-- -------------------------------------------------------------------------- START IMAGE.PHP -->
        <main>
            <div class="container">                         <!-- covers pretty much everything between the header and the footer -->
                <div class="column-one">                    <!-- a vertically oriented section that has a "picture of the day" section on top and a stats section underneath -->
<?php
require 'sidebar-image.php';
?>
              </div> <!-- end div .column-one -->
                <div class="column-two">                <!-- a horizontally-oriented section that contains blocks for different types of media and information -->
                    <div class="list-block">
                        <h1><?php echo $page_name; ?></h1>
                        <img src="thumb.php?imageid=<?php echo $getimageid; ?>" alt="<?php echo $getimagedesc; ?>" title="<?php echo $getimagename; ?>" class="image-main">
                        <!-- table for more detailed metadata. we don't want the sidebar to look too busy/crowded -->
                        <table>
                            <tr>
                                <td>Description</td>
                                <td><?php echo $getimagedesc; ?></td>
                            </tr>
                            <tr>
                                <td>Path</td>
                                <td><?php echo $getimagepath; ?></td>
                            </tr>
                            <tr>
                                <td>People</td>
                                <td><?php echo $getimageppl; ?></td>
                            </tr>
                            <tr>
                                <td>Organizations</td>
                                <td><?php echo $getimageorgs; ?></td>
                            </tr>
                            <tr>
                                <td>Tags</td>
                                <td><?php echo $getimagetags; ?></td>
                            </tr>
                            <tr>
                                <td>Categories</td>
                                <td><?php echo $getimagecats; ?></td>
                            </tr>
                            <tr>
                                <td><a href="admin/image-meta-edit.php?imageid=<?php echo $getimageid; ?>">Edit</a> | <a href="admin/image-delete.php?imageid=<?php echo $getimageid; ?>">Delete</a></td>
                            </tr>
                        </table>
                    </div> <!-- end div .horiz-block -->
                </div> <!-- end div .column-two -->
            </div> <!-- end div .container -->
        </main>
<!-- -------------------------------------------------------------------------- END IMAGE.PHP -->
<?php require 'footer.php'; ?>
